<?php

use App\Console\Commands\ChangeHasGrownTodayStatus;
use App\Console\Commands\HandleStatus;
use App\Console\Commands\HandleSubscriptionStatus;
use App\Console\Commands\RunAutowatering;
use App\Console\Commands\SendHasGrownToday;
use App\Console\Commands\SendNewPlants;

return [
    [
        'command' => new HandleStatus(),
        'modeInterval' => true,
        'modeIntervalMinutes' => 10
    ],
    [
        'command' => new HandleSubscriptionStatus(),
        'modeInterval' => true,
        'modeIntervalMinutes' => 10
    ],
    [
        'command' => new RunAutowatering(),
        'modeInterval' => true,
        'modeIntervalMinutes' => 60
    ],
    [
        'command' => new SendNewPlants(),
        'type' => 'SendNewPlants',
        'modeInterval' => true,
        'modeIntervalMinutes' => 60
    ],
    [
        'command' => new SendHasGrownToday(),
        'type' => 'SendHasGrownToday',
        'modeInterval' => true,
        'modeIntervalMinutes' => 60
    ],
    [
        'command' => new ChangeHasGrownTodayStatus(),
        'type' => 'ChangeHasGrownTodayStatus',
        'modeInterval' => true,
        'modeIntervalMinutes' => 60
    ]
];