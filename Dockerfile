FROM show4me/alpine-php-base:php-7.2.24

WORKDIR /var/www/service

COPY . .
COPY /nginx.conf /etc/nginx/nginx.conf
COPY /php.ini /usr/local/etc/php/php.ini
CMD "./run.sh"
