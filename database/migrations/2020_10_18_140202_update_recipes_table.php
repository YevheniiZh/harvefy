<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recipes', function (Blueprint $table) {
            $table->dropColumn('plant_type_id');
            $table->jsonb('plant_type_ids')->after('title');
        });

        Schema::create('recipe_plant_types', function (Blueprint $table) {
            $table->id();
            $table->integer('recipe_id');
            $table->integer('plant_type_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('recipes', function (Blueprint $table) {
             $table->integer('plant_type_id')->after('title');
             $table->dropColumn('plant_type_ids');
         });
        Schema::dropIfExists('recipe_plant_types');
    }
}
