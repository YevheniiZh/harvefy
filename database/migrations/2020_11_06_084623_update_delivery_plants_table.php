<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateDeliveryPlantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('ALTER TABLE `delivery_plants` MODIFY `plant_id` int(11)');
        DB::unprepared('DROP INDEX delivery_plants_plant_id_unique ON delivery_plants');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('CREATE UNIQUE INDEX delivery_plants_plant_id_unique
ON delivery_plants(plant_id);
');
    }
}
