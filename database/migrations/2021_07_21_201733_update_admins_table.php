<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admins', function (Blueprint $table) {
            $table->string('surname');
            $table->string('phone')->nullable();
            $table->string('promocode')->nullable();
            $table->boolean('is_partner')->default(false);
            $table->string('reset_pass_token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admins', function (Blueprint $table) {
            $table->dropColumn('surname');
            $table->dropColumn('phone');
            $table->dropColumn('promocode');
            $table->dropColumn('is_partner');
            $table->dropColumn('reset_pass_token');
        });
    }
}
