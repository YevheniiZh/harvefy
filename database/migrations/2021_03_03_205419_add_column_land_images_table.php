<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddColumnLandImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('land_images', function (Blueprint $table) {
            $table->unsignedInteger('land_type_id');
            $table->unsignedInteger('product_type_id')->nullable();
        });

        DB::unprepared('update `land_images` set `land_type_id` = 1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('land_images', function (Blueprint $table) {
            $table->dropColumn('land_type_id');
            $table->dropColumn('product_type_id');
        });
    }
}
