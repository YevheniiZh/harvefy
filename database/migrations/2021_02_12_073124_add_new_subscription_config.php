<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddNewSubscriptionConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [
                'name' => 'subscription_2_plants_price',
                'description' => '2 plants land',
                'value' => 270
            ],
            [
                'name' => 'subscription_angus_price',
                'description' => 'meat angus',
                'value' => 600
            ],
            [
                'name' => 'subscription_simental_price',
                'description' => 'meat simental',
                'value' => 500
            ]
        ];

        DB::table('configs')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::raw("DELETE FROM configs WHERE name in ('subscription_2_plants_price', 'subscription_angus_price', 'subscription_simental_price')");
    }
}
