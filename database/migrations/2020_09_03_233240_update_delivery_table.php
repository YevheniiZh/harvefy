<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDeliveryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery', function (Blueprint $table) {
            $table->dropColumn('plant_id');
            $table->jsonb('plant_ids')->after('user_id');
        });

        Schema::create('delivery_plants', function (Blueprint $table) {
            $table->id();
            $table->integer('delivery_id');
            $table->integer('plant_id')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery', function (Blueprint $table) {
            $table->integer('plant_id')->after('user_id');
            $table->dropColumn('plant_ids');
        });
        Schema::dropIfExists('delivery_plants');
    }
}
