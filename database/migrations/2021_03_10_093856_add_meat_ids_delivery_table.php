<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddMeatIdsDeliveryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery', function (Blueprint $table) {
            $table->jsonb('meat_ids')->after('plant_ids');
        });
        DB::unprepared('update `delivery` set meat_ids = \'[]\'');

        Schema::table('delivery_plants', function (Blueprint $table) {
            $table->unsignedInteger('product_type_id')->after('plant_id');
        });
        DB::unprepared('ALTER TABLE `delivery_plants` CHANGE `plant_id` `product_id` int(11)');
        DB::unprepared('update `delivery_plants` set `product_type_id` = 1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery', function (Blueprint $table) {
            $table->dropColumn('meat_ids');
        });

        Schema::table('delivery_plants', function (Blueprint $table) {
            $table->dropColumn('product_type_id');
        });
        DB::unprepared('ALTER TABLE `delivery_plants` CHANGE `product_id` `plant_id` int(11)');
    }
}
