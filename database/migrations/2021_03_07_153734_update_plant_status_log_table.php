<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class UpdatePlantStatusLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('ALTER TABLE `plant_status_log` CHANGE `plant_id` `product_id` int(11)');
        Schema::table('plant_status_log', function (Blueprint $table) {
            $table->unsignedInteger('product_type_id')->after('product_id');
        });
        DB::unprepared('ALTER TABLE `meat_types` MODIFY `price` decimal(8,2) default NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('ALTER TABLE `plant_status_log` CHANGE `product_id` `plant_id` int(11)');
        Schema::table('plant_status_log', function (Blueprint $table) {
            $table->dropColumn('product_type_id');
        });
        DB::unprepared('ALTER TABLE `meat_types` MODIFY `price` decimal(8,2)');
    }
}
