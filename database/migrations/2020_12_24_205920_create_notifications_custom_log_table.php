<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsCustomLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications_custom_log', function (Blueprint $table) {
            $table->id();
            $table->integer('to_all_users');
            $table->string('email')->nullable();
            $table->string('title')->default('');
            $table->string('body')->default('');
            $table->boolean('status');
            $table->text('response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications_custom_log');
    }
}
