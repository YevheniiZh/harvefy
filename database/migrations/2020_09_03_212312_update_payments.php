<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdatePayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('ALTER TABLE `payments` CHANGE `land_id` `item_id` int(11)');

        Schema::table('payments', function (Blueprint $table) {
            $table->string('item_type')->after('item_id')->default('');
        });
        Schema::table('delivery', function (Blueprint $table) {
            $table->boolean('is_payed')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('ALTER TABLE `payments` CHANGE `item_id` `land_id` int(11)');

        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('item_type');
        });
        Schema::table('delivery', function (Blueprint $table) {
            $table->dropColumn('is_payed');
        });
    }
}
