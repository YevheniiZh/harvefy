<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddToConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [
                'name' => 'minutes_before_die_without_watering',
                'description' => '48 hours',
                'value' => 48 * 60
            ],
            [
                'name' => 'minutes_before_die_after_grown',
                'description' => '72 hours',
                'value' => 72 * 60
            ]
        ];

        DB::table('configs')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::raw("DELETE FROM configs WHERE name in ('minutes_before_die_without_watering', 'minutes_before_die_after_grown')");
    }
}
