<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddValueToConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [
                'name' => 'minutes_before_watering_required',
                'description' => '24 hours',
                'value' => 24 * 60
            ]
        ];

        DB::table('configs')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::raw("DELETE FROM configs WHERE name = 'minutes_before_watering_required'");
    }
}
