<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meat', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('land_id');
            $table->unsignedInteger('meat_type_id');
            $table->integer('harvest_option_id')->default(null)->nullable();
            $table->integer('status')->default(1);
            $table->timestamp('last_watering')->default(null)->nullable();
            $table->jsonb('watering_log')->default(null)->nullable();
            $table->boolean('autowatering')->default(false);
            $table->timestamp('status_updated_at')->nullable();
            $table->timestamp('status_watering_required_at')->nullable();
            $table->timestamps();

            $table->index('meat_type_id', 'fk_meat_meat_type_id_idx');

            $table->foreign('meat_type_id')
                ->references('id')
                ->on('meat_types')
                ->onUpdate('CASCADE')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meat');
    }
}
