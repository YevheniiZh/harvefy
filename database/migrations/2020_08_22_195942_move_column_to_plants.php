<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MoveColumnToPlants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plants', function (Blueprint $table) {
            $table->boolean('autowatering')->default(false);
            $table->timestamp('status_updated_at')->nullable();
        });

        Schema::table('lands', function (Blueprint $table) {
            $table->dropColumn('autowatering');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plants', function (Blueprint $table) {
            $table->dropColumn('autowatering');
            $table->dropColumn('status_updated_at');
        });

        Schema::table('lands', function (Blueprint $table) {
            $table->boolean('autowatering')->default(false);
        });
    }
}
