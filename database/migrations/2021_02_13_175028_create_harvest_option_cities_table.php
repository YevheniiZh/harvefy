<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHarvestOptionCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('harvest_option_cities', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('harvest_option_id');
            $table->integer('city_id');
            $table->timestamps();

            $table->index('harvest_option_id', 'fk_harvest_option_cities_harvest_options_idx');

            $table->foreign('harvest_option_id')
                ->references('id')
                ->on('harvest_options')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('harvest_option_cities');
    }
}
