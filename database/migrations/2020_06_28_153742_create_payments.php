<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('land_id');
            $table->decimal('amount');
            $table->string('currency')->default('');
            $table->string('type')->default('');
            $table->string('payment_system')->default('');
            $table->string('card_type')->default('');
            $table->decimal('fee')->default(0);
            $table->string('status')->default('');
            $table->string('transaction_id')->default('');
            $table->text('ps_response')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
