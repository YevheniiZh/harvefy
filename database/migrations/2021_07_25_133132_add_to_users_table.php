<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('promocode', 100);
            $table->string('address');
            $table->integer('partner_id')->nullable();

            $table->index('partner_id', 'partner_id_users_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('promocode');
            $table->dropColumn('address');
            $table->dropColumn('partner_id');
        });
    }
}
