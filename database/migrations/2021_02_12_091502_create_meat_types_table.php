<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeatTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meat_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('image')->nullable();
            $table->decimal('price');
            $table->string('short_desc');
            $table->text('full_desc');
            $table->integer('days_to_grow');
            $table->boolean('out_of_stock')->default(false);
            $table->boolean('teaser')->default(false);
            $table->text('about_culture');
            $table->integer('wither_in_days')->nullable();
            $table->string('video')->default(null)->nullable();
            $table->integer('harvests_per_month');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meat_types');
    }
}
