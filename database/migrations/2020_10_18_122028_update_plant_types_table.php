<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdatePlantTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('ALTER TABLE `plant_types` MODIFY `wither_in_days` int(11) default NULL');
        DB::unprepared('ALTER TABLE `plant_types` MODIFY `price` decimal(8,2) default NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('ALTER TABLE `plant_types` MODIFY `wither_in_days` int(11)');
        DB::unprepared('ALTER TABLE `plant_types` MODIFY `price` decimal(8,2)');
    }
}
