<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->string('name');
            $table->integer('value');
            $table->string('description')->default('');
            $table->jsonb('additional')->default(null)->nullable();
            $table->timestamps();
        });

        $data = [
            [
                'name' => 'subscription_price',
                'value' => 400
            ]
        ];

        DB::table('configs')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
