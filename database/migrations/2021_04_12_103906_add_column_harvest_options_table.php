<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnHarvestOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('harvest_options', function (Blueprint $table) {
            $table->string('image')->after('name')->nullable();
            $table->boolean('active')->after('meat_type_id')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('harvest_options', function (Blueprint $table) {
            $table->dropColumn('image');
            $table->dropColumn('active');
        });
    }
}
