<?php

use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('city_id')->after('name');

            $table->index('city_id', 'city_id_users_idx');
        });

        DB::unprepared('update `users` set `city_id` = 1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $output = new ConsoleOutput();
        $output->writeln('<error>You need to set city name for users</error>');

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('city_id');
        });
    }
}
