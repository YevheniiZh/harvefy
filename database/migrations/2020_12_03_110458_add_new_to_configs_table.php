<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddNewToConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [
                'name' => 'new_plants_to_email',
                'description' => '',
                'value' => 1,
                'additional' => json_encode(['harvefyplants@gmail.com'])
            ],
            [
                'name' => 'delivery_days',
                'description' => '',
                'value' => 1,
                'additional' => json_encode([
                    'Понедельник',
                    'Среда',
                    'Пятница',
                    'Суббота',
                ])
            ]
        ];

        DB::table('configs')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DELETE FROM configs WHERE name in ('delivery_days', 'new_plants_to_email')");
    }
}
