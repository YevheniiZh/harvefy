<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', static function () {
    return view('landing');
})->name('landing');
Route::get('/beef', static function () {return view('landing');});
Route::get('/microgreen', static function () {return view('landing');});
Route::get('/legal', static function () {return view('landing');});
Route::get('/recipes', static function () {return view('landing');});
Route::get('/contact', static function () {return view('landing');});
Route::get('/privacy', static function () {
    return redirect('/legal');
});
Route::get('/terms', static function () {
    return redirect('/legal');
});
Route::post('admin/password/email', 'Admin\PasswordController@sendRecoverPassword');
Route::get('password/recover', 'Admin\PasswordController@showRecoverPassword')->name('password_recover');
Route::post('password/recover', 'Admin\PasswordController@setNewPassword');

Auth::routes();

Route::middleware('auth')->group(static function () {
    Route::get('/home', 'Admin\HomeController@index')->name('home');

    Route::get('password/change', 'Admin\AdminController@showChangePassword');
    Route::patch('password/change', 'Admin\AdminController@changePassword');

    Route::prefix('admin')->namespace('Admin')->group(static function () {

        Route::middleware('can:is_admin')->group(static function () {
            Route::get('admins', 'AdminController@index');
            Route::get('partners', 'PartnersController@index');
            Route::post('partners', 'PartnersController@add');
            Route::post('partners/id/{id?}', 'PartnersController@update');
            Route::get('plant_types', 'PlantTypeController@index');
            Route::post('plant_types', 'PlantTypeController@add');
            Route::post('plant_types/id/{id?}', 'PlantTypeController@update');
            Route::delete('plant_types/id/{id?}', 'PlantTypeController@delete');
            Route::get('meat_types', 'MeatTypeController@index');
            Route::post('meat_types', 'MeatTypeController@add');
            Route::post('meat_types/id/{id?}', 'MeatTypeController@update');
            Route::delete('meat_types/id/{id?}', 'MeatTypeController@delete');
            Route::get('meat', 'MeatController@index');
            Route::post('meat/get_by_date', 'MeatController@getByDate');
            Route::post('meat/pour_on/id/{id?}', 'MeatController@pourOn');
            Route::get('about', 'AboutController@index');
            Route::post('about', 'AboutController@add');
            Route::post('about/id/{id?}', 'AboutController@update');
            Route::delete('about/id/{id?}', 'AboutController@delete');
            Route::get('recipes', 'RecipeController@index');
            Route::post('recipes', 'RecipeController@add');
            Route::post('recipes/id/{id?}', 'RecipeController@update');
            Route::delete('recipes/id/{id?}', 'RecipeController@delete');
            Route::get('images', 'LandImageController@index');
            Route::post('images', 'LandImageController@add');
            Route::post('images/type', 'LandImageController@addType');
            Route::delete('images/id/{id?}', 'LandImageController@delete');
            Route::get('cities', 'CitiesController@index');
            Route::post('cities', 'CitiesController@add');
            Route::post('cities/id/{id?}', 'CitiesController@update');
            Route::delete('cities/id/{id?}', 'CitiesController@delete');
            Route::get('streets', 'StreetsController@index');
            Route::post('streets', 'StreetsController@add');
            Route::post('streets/id/{id?}', 'StreetsController@update');
            Route::delete('streets/id/{id?}', 'StreetsController@delete');
            Route::get('harvest_options', 'HarvestOptionController@index');
            Route::post('harvest_options', 'HarvestOptionController@add');
            Route::post('harvest_options/id/{id?}', 'HarvestOptionController@update');
            Route::delete('harvest_options/id/{id?}', 'HarvestOptionController@delete');
            Route::get('contact_us', 'ContactUsController@index');
            Route::patch('contact_us/id/{id?}', 'ContactUsController@markAsHandled');
            Route::delete('contact_us/id/{id?}', 'ContactUsController@delete');
        });

        Route::get('users', 'UserController@users');
        Route::get('lands', 'LandController@index');
        Route::get('plant', 'PlantController@index');
        Route::post('plant/get_by_date', 'PlantController@getByDate');
        Route::post('plant/pour_on/id/{id?}', 'PlantController@pourOn');
        Route::get('payments', 'PaymentController@index');
        Route::get('payments/transactions', 'PaymentController@transactions');
        Route::get('delivery', 'DeliveryController@index');
        Route::patch('delivery/id/{id?}', 'DeliveryController@update');
        Route::patch('delivery/config/id/{id?}', 'DeliveryController@updateConfig');
        Route::get('subscriptions', 'SubscriptionsController@index');
        Route::get('notifications_log', 'NotificationsController@index');
        Route::get('notifications_log/get_data', 'NotificationsController@getData');
        Route::post('notifications/send', 'NotificationsController@send');
    });
});