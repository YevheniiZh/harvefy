<?php

use App\Services\MeatService;
use App\Services\PlantService;
use App\Services\SubscriptionService;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('handleStatus', function () {
    $result['plant'] = PlantService::checkStatusForUpdate();
    $result['meat'] = MeatService::checkStatusForUpdate();
    $result = json_encode($result);
    echo date('d-m-Y H:i:s') . " handled {$result}\r\n" ;
});

Artisan::command('handleSubscriptionStatus', function () {
    $result = SubscriptionService::checkStatusForUpdate();
    $result = json_encode($result);
    echo date('d-m-Y H:i:s') . " handled {$result}\r\n" ;
});

Artisan::command('runAutowatering', function () {
    $result = PlantService::runAutowatering();
    $result = json_encode($result);
    echo date('d-m-Y H:i:s') . " handled {$result}\r\n" ;
});

Artisan::command('sendNewPlants', function () {
    $result = PlantService::sendNewPlants();
    $result = json_encode($result);
    echo date('d-m-Y H:i:s') . " handled {$result}\r\n" ;
});
