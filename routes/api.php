<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', static function (Request $request) {
    return $request->user();
});*/

Route::get('cities', 'UserController@getCities');
Route::get('streets', 'UserController@getStreets');
Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::post('forgot', 'AuthController@forgot');

Route::middleware('auth:api')->group(static function () {
    Route::patch('token/refresh', 'AuthController@tokenRefresh');
    Route::get('token/info', 'AuthController@tokenInfo');
    Route::get('logout', 'AuthController@logout');

    Route::get('profile', 'UserController@profile');
    Route::delete('profile', 'UserController@delete');
    //Route::get('users/id/{id?}', 'UserController@singleUser');
    //Route::get('users', 'UserController@allUsers');

    Route::prefix('partner')->group(static function () {
        Route::get('/', 'PartnerController@index');
        Route::get('/id/{id?}', 'PartnerController@show');
    });

    Route::prefix('meat_type')->group(static function () {
        Route::get('/', 'MeatTypeController@index');
        Route::get('/id/{id?}', 'MeatTypeController@show');
    });

    Route::prefix('plant_type')->group(static function () {
        Route::get('/', 'PlantTypeController@index');
        Route::get('/id/{id?}', 'PlantTypeController@show');
    });

    Route::prefix('plant_status')->group(static function () {
        Route::get('/', 'PlantStatusController@index');
        Route::get('/id/{id?}', 'PlantStatusController@show');
    });

    Route::prefix('lands')->group(static function () {
        Route::get('/land_type', 'LandController@landTypes');
        Route::get('/', 'LandController@index');
        Route::post('/', 'LandController@store');
        Route::patch('/id/{id?}', 'LandController@update');
        Route::get('/id/{id?}', 'LandController@show');
        Route::post('/add_plant/id/{id?}', 'LandController@addPlant');
        Route::post('/add_meat/id/{id?}', 'LandController@addMeat');
        //Route::delete('/', 'LandController@delete');
    });

    Route::prefix('plant')->group(static function () {
        Route::get('/', 'PlantController@index');
        Route::get('/id/{id?}', 'PlantController@show');
        Route::patch('/id/{id?}', 'PlantController@update');
        Route::post('/harvest/id/{id?}', 'PlantController@harvest');
        Route::post('/pour_on/id/{id?}', 'PlantController@pourOn');
    });

    Route::prefix('meat')->group(static function () {
        Route::get('/', 'MeatController@index');
        Route::get('/id/{id?}', 'MeatController@show');
        Route::patch('/id/{id?}', 'MeatController@update');
        Route::post('/harvest/id/{id?}', 'MeatController@harvest');
        Route::get('/harvest/options', 'HarvestOptionController@index');
        Route::post('/pour_on/id/{id?}', 'MeatController@pourOn');
    });

    Route::prefix('about')->group(static function () {
        Route::get('/', 'AboutController@index');
        Route::get('/id/{id?}', 'AboutController@show');
    });

    Route::prefix('recipes')->group(static function () {
        Route::get('/', 'RecipeController@index');
        Route::get('/id/{id?}', 'RecipeController@show');
    });

    Route::prefix('payments')->group(static function () {
        Route::post('', 'PaymentController@create');
        Route::post('/delivery', 'PaymentController@createDelivery');
        Route::get('/id/{id?}', 'PaymentController@show');
    });

    Route::prefix('subscription')->group(static function () {
        Route::get('/', 'SubscriptionController@index');
        Route::get('/id/{id?}', 'SubscriptionController@show');
        Route::post('unsubscribe/id/{id?}', 'SubscriptionController@unsubscribe');
    });

    Route::prefix('delivery')->group(static function () {
        Route::get('/', 'DeliveryController@index');
        Route::get('/days', 'DeliveryController@days');
        Route::get('/last', 'DeliveryController@last');
        Route::get('/id/{id?}', 'DeliveryController@show');
        Route::delete('/id/{id?}', 'DeliveryController@delete');
        Route::post('/', 'DeliveryController@store');
    });

    Route::prefix('contact_us')->group(static function () {
        Route::get('/', 'ContactUsController@index');
        Route::get('/id/{id?}', 'ContactUsController@show');
        Route::post('/', 'ContactUsController@store');
    });

    Route::prefix('notification')->group(static function () {
        Route::get('/', 'NotificationController@index');
        Route::get('/id/{id?}', 'NotificationController@show');
        Route::post('/', 'NotificationController@store');
    });
});

Route::prefix('payments')->group(static function () {
    Route::get('/return', 'PaymentController@after');
    Route::post('/return', 'PaymentController@after');
    Route::post('/service', 'PaymentController@service');
    Route::post('/payment', 'PaymentController@getLiqpayPaymentPage');
    Route::get('/payment', 'PaymentController@getLiqpayPaymentPage');
    Route::get('/payment/id/{id?}', 'PaymentController@getLiqpayPaymentPageById');
});
Route::prefix('config')->group(static function () {
    Route::get('/', 'ConfigController@getConfig');
    Route::patch('/', 'ConfigController@updateConfig');
    Route::get('/get_current_api_version', 'ConfigController@getCurrentApiVersion');
});
Route::get('/cron/execute', 'CronController@execute');
