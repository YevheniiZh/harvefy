@extends('adminlte::auth.auth-page', ['auth_type' => 'login'])
@php( $login_url = View::getSection('login_url') ?? config('adminlte.login_url', 'login') )

@php( $password_email_url = View::getSection('password_email_url') ?? config('adminlte.password_email_url', 'password/email') )

@if (config('adminlte.use_route_url', false))
    @php( $password_email_url = $password_email_url ? route($password_email_url) : '' )
@else
    @php( $password_email_url = $password_email_url ? url($password_email_url) : '' )
@endif

@section('auth_header', __('adminlte::adminlte.password_reset_message'))

@section('auth_body')

    @if(session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <form id="emailForm" action="{{ $password_email_url }}" method="post">
        <p id="form-errors" class="text-center error invalid-feedback" style="display: inherit;"></p>
        {{ csrf_field() }}

        {{-- Email field --}}
        <div class="input-group mb-3">
            <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                   value="{{ old('email') }}" placeholder="{{ __('adminlte::adminlte.email') }}" autofocus>
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-envelope {{ config('adminlte.classes_auth_icon', '') }}"></span>
                </div>
            </div>
            @if($errors->has('email'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </div>
            @endif
        </div>

        {{-- Send reset link button --}}
        <button type="submit" class="btn btn-block {{ config('adminlte.classes_auth_btn', 'btn-flat btn-primary') }}">
            <span class="fas fa-share-square"></span>
            {{ __('adminlte::adminlte.send_password_reset_link') }}
        </button>

    </form>

@stop

@section('auth_footer')
    @if($login_url)
        <p class="my-0">
            <a href="{{ $login_url }}">
                {{ __('adminlte::adminlte.i_already_have_a_membership') }}
            </a>
        </p>
    @endif
@stop

<script src="../vendor/sweetalert2/sweetalert2.min.js"></script>
<link rel="stylesheet" href="../vendor/sweetalert2/sweetalert2.css">

<script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        $("form#emailForm").on('submit', function(e){
            e.preventDefault();
            let data = new FormData(this);
            $('#form-errors').text('');
            let item = $(this);
            let id = item.val();
            $.ajax({
                data: data,
                url: '/admin/password/email',
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                method: 'POST',
                type: 'POST',
                success: function(result) {
                    Toast.fire({
                        icon: 'success',
                        title: result.data
                    });
                    $('[name="email"]').val('');
                },
                error: function (jqXHR, exception) {
                    $.each(jqXHR.responseJSON.errors, function (key, item) {
                        $('#form-errors').append(item.message + '<br>');
                    });
                },
            });
        });
    });

</script>