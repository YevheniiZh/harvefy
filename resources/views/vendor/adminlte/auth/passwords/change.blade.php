@extends('adminlte::page')

@section('title', 'Admins')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-body login-card-body">
                    <p class="login-box-msg"></p>

                    <form action="/password/change" method="patch" enctype="multipart/form-data">
                        <p id="form-errors" class="text-center error invalid-feedback" style="display: inherit;"></p>
                        @csrf
                        <div class="input-group mb-3">
                            <input id="password" type="password" class="form-control" placeholder="Password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input id="password_confirmation" type="password" class="form-control" placeholder="Confirm Password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <button id="submit" class="btn btn-primary btn-block">Change password</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                </div>
                <!-- /.login-card-body -->
            </div>            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

@stop

<script src="../vendor/sweetalert2/sweetalert2.min.js"></script>
<link rel="stylesheet" href="../vendor/sweetalert2/sweetalert2.css">

<script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        $('#submit').on('click', function(e){
            e.preventDefault();
            $('#form-errors').text('');
            let item = $(this);
            let id = item.val();
            $.ajax({
                data: {
                    "_token": "{{ csrf_token() }}",
                    'password': $('#password').val(),
                    'password_confirmation': $('#password_confirmation').val()
                },
                url: '/password/change',
                method: 'PATCH',
                type: 'PATCH',
                success: function(result) {
                    Toast.fire({
                        icon: 'success',
                        title: result.data
                    });
                    $('#password').val('');
                    $('#password_confirmation').val('');
                },
                error: function (jqXHR, exception) {
                    $.each(jqXHR.responseJSON.errors, function (key, item) {
                        $('#form-errors').append(item.message + '<br>');
                    });
                }
            });
        });
    });

</script>