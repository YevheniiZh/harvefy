@extends('adminlte::page')

@section('title', 'Delivery')

@section('content')
    @if($isAdmin)
        <div class="row">
        <div class="col-12">
            <button type="button" class="btn btn-success float-right" id="setDeliveryDays" data-toggle="modal" data-target="#deliveryDays" data-whatever="@mdo">Delivery days</button>
        </div>
    </div>
    <hr>
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Delivery list</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>User id</th>
                            <th>Plants</th>
                            <th>Meat</th>
                            <th>City</th>
                            <th>Address</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Day of week</th>
                            <th>Delivered</th>
                            <th>Delivery date</th>
                            <th>Paid</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                            @if($isAdmin)
                                <th>Manager</th>
                            @endif
                            <th>Manage</th>
                        </tr>
                        </thead>
                        <tbody id="ajaxPosts">
                        @foreach( $delivery as $index => $item)
                            <tr @if(in_array($selectedItem, $item['plant_ids'], true)) style="background: rgba(150,192,138,0.59)" @endif>
                                <td>{{ $item['id'] }}</td>
                                <td>{{ $item['user_id'] }}</td>
                                <td>
                                    @foreach($item['plants'] as $plant_type_id){{ $loop->first ? '' : ', ' }}
                                    {{ $plant_types[$plant_type_id['plant_type_id']]['name'] }}@endforeach
                                </td>
                                <td>
                                    @foreach($item['meat'] as $meat_type_id){{ $loop->first ? '' : ', ' }}
                                    {{ $meat_types[$meat_type_id['meat_type_id']]['name'] }}@endforeach
                                </td>
                                <td>{{ $item['city'] }}</td>
                                <td>{{ $item['address'] }}</td>
                                <td>{{ $item['name'] }}</td>
                                <td>{{ $item['phone'] }}</td>
                                <td>{{ $item['day'] }}</td>
                                <td class="is_delivered">
                                    @if($item['delivered'])
                                        <i class="fas fa-check" style="color:#82c91e;"></i>
                                    @else
                                        <i class="fas fa-minus" style="color:#b1b1b1;"></i>
                                    @endif
                                </td>
                                <td>{{ $item['delivery_date'] }}</td>
                                <td>
                                    @if($item['is_payed'])
                                        <i class="fas fa-check" style="color:#82c91e;"></i>
                                    @else
                                        <i class="fas fa-minus" style="color:#b1b1b1;"></i>
                                    @endif
                                </td>
                                <td>{{ $item['created_at'] }}</td>
                                <td>{{ $item['updated_at'] }}</td>
                                @if($isAdmin)
                                    <td data-id="{{ $item['user']['partner_id'] }}">
                                        @if($partners[$item['user']['partner_id']]??false)
                                            {{ $partners[$item['user']['partner_id']]['name'] }} {{ $partners[$item['user']['partner_id']]['surname'] }}
                                        @else
                                            Наши
                                        @endif
                                    </td>
                                @endif
                                <td>
                                    <div class="btn-group">
                                        @if(!$item['delivered'] && $item['is_payed'])
                                        <button type="button" value="{{ $item['id'] }}" class="btn btn-success set_delivered" title="Delivered"><i class="fas fa-truck"></i></button>
                                        <button type="button" value="{{ $item['id'] }}" class="btn btn-default" title="Invoice"
                                                data-toggle="modal"
                                                data-target="#modal-lg"
                                                data-name="Invoice Id:{{ $item['id'] }}"
                                                data-user-address="{{ $item['address'] }}"
                                                data-user-name="{{ $item['name'] }}"
                                                data-user-phone="{{ $item['phone'] }}"
                                                data-plants="{{ $item['plant_params'] }}"
                                        ><i class="fas fa-file"></i></button>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->


        </div>
        <!-- /.col -->
    </div>

    <div class="modal fade" id="deliveryDays" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Day</th>
                            <th>Active</th>
                        </tr>
                        </thead>
                        <tbody id="ajaxPosts">
                            @foreach( $days as $index => $item)
                                <tr>
                                    <td>{{ $item['name'] ?? '' }}</td>
                                    <td>
                                        @if(isset($item['active']))
                                            <i class="fas fa-check updateActiveStatus" data-id="{{ $index }}" style="color:#82c91e;"></i>
                                        @else
                                            <i class="fas fa-minus updateActiveStatus" data-id="{{ $index }}" style="color:#b1b1b1;"></i>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-lg" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="invoice">
                    <p><b>Адрес:</b> <span data-address></span></p>
                    <p><b>Имя:</b> <span data-name></span></p>
                    <p><b>Телефон:</b> <span data-phone></span></p>
                    <table class="table table-bordered table-hover" border="1" cellpadding="3" width="50%">
                        <thead>
                        <tr>
                            <th>Растение</th>
                            <th>Дата посадки</th>
                            <th>Кол-во</th>
                        </tr>
                        </thead>
                        <tbody id="plants">

                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="printDiv()">Print</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop
<script src="../vendor/sweetalert2/sweetalert2.min.js"></script>
<link rel="stylesheet" href="../vendor/sweetalert2/sweetalert2.css">

<script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>
<!-- DataTables -->
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="../vendor/datatables/js/dataTables.bootstrap4.min.js"></script>

<script>
var plant_types = {!! json_encode($plant_types) !!};
var partners = {!! json_encode($partners) !!};
var isAdmin = {{$isAdmin}};

$(document).ready(function() {

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    $("#example2").DataTable({
        order: [0, 'desc'],
        pageLength : 15,
        lengthMenu: [[10, 15, 50, 100, -1], [10, 15, 50, 100, 'All']],
        initComplete: function () {
            if (!isAdmin) {
                return;
            }
            this.api().columns([14]).every( function () {
                var column = this;
                var select = $('<select><option value="">Все</option></select>')
                    .appendTo( $(column.header()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column.search( val ? '^'+val+'$' : '', true, false ).draw();
                    } );

                select.append( '<option value="Наши">Наши</option>' )
                $.each(partners, function ( i, item ) {
                    let name = item.name + (item.surname ? ' '+ item.surname : '');
                    select.append( '<option value="'+name+'">'+name + '</option>' )
                } );
            } );
        }
    });

    $('.set_delivered').on('click', function(e){
        let item = $(this);
        let id = item.val();
        $.ajax({
            data: {
                "_token": "{{ csrf_token() }}"
            },
            url: '/admin/delivery/id/' + id,
            type: 'PATCH',
            success: function(result) {
                let icon = '<i class="fas fa-check" style="color:#82c91e;"></i>';
                item.parents('tr').find('.is_delivered').html(icon);
                item.remove();
            },
            error: function (jqXHR, exception) {
                Toast.fire({
                    icon: 'error',
                    title: jqXHR.responseJSON.message
                });
            }
        });
    });

    $('.updateActiveStatus').on('click', function(e){
        let item = $(this);
        let id = parseInt(item.data('id'));
        let isActive = item.hasClass('fa-check') ? 0 : 1;   // reverted value
        $.ajax({
            data: {
                "_token": "{{ csrf_token() }}",
                id: id,
                isActive: isActive
            },
            url: '/admin/delivery/config/id/' + id,
            type: 'PATCH',
            success: function(result) {
                if (isActive) {
                    item.removeClass('fa-minus').addClass('fa-check').css('color', '#82c91e');
                } else {
                    item.removeClass('fa-check').addClass('fa-minus').css('color', '#b1b1b1');
                }
            },
            error: function (jqXHR, exception) {
                Toast.fire({
                    icon: 'error',
                    title: jqXHR.responseJSON.message
                });
            }
        });
    });

    $('#modal-lg').on('show.bs.modal', function (event) {
        const button = $(event.relatedTarget);
        const name = button.data('name');
        const plants = button.data('plants');
        let plantTypes = {};
        $.each(plants, function(key, value) {
            let name = plant_types[value['plant_type_id']].name;
            plantTypes[name] = plantTypes[name] ? plantTypes[name] : {};
            plantTypes[name]['date'] = plantTypes[name]['date'] && (plantTypes[name]['date'] !== value['created_at']) ? plantTypes[name]['date'] + ', ' + value['created_at'] : value['created_at'];
            plantTypes[name]['count'] = plantTypes[name]['count'] ? plantTypes[name]['count'] + 1 : 1;
        });
        $('#plants').html('');
        $.each(plantTypes, function(key, value) {
            $('#plants').append(' <tr>\n' +
                '<th>' + key + '</th>\n' +
                '<th>' + value['date'] + '</th>\n' +
                '<th>' + value['count'] + '</th>\n' +
                '</tr>');
        });
        const modal = $(this);
        modal.find('.modal-title').text(name);
        modal.find('[data-address]').text(button.data('user-address'));
        modal.find('[data-name]').text(button.data('user-name'));
        modal.find('[data-phone]').text(button.data('user-phone'));
    });

});

function printDiv() {
    var divToPrint = document.getElementById('invoice');
    newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
}

</script>