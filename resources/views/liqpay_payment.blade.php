<form method="POST" action="{{ $url }}"
      accept-charset="utf-8">
    <input type="hidden" name="data" value="{{ $data }}"/>
    <input type="hidden" name="signature" value="{{ $signature }}"/>
    <input type="image"
           src="//static.liqpay.ua/buttons/p1ru.radius.png"/>
</form>
<script>
    document.addEventListener('DOMContentLoaded', function(){
        document.forms[0].submit();
    });
</script>