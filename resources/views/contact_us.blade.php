@extends('adminlte::page')

@section('title', 'Contact us')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Contact us requests</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>User id</th>
                            <th>Title</th>
                            <th>Details</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                            <th>Manage</th>
                        </tr>
                        </thead>
                        <tbody id="ajaxPosts">
                        @foreach( $items as $index => $item)
                            <tr class="item-row">
                                <td>{{ $item['id'] }}</td>
                                <td>{{ $item['user_id'] }}</td>
                                <td>{{ $item['title'] }}</td>
                                <td>{{ $item['details'] }}</td>
                                <td>{{ $item['created_at'] }}</td>
                                <td>{{ $item['updated_at'] }}</td>
                                <td>
                                    <div class="btn-group">
                                        @if(!$item['status'])
                                        <button type="button" value="{{ $item['id'] }}" class="btn btn-default mark_as_handled" title="Mark as handled"><i class="fas fa-check"></i></button>
                                        @else
                                            <span class="btn"><i class="fas fa-check" style="color: #00b44e" title="Handled"></i></span>
                                        @endif
                                            <button type="button" value="{{ $item['id'] }}" class="btn btn-default delete_item" title="Remove"><i class='fas fa-trash-alt' style="color: #5f5f5f"></i></button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->


        </div>
        <!-- /.col -->
    </div>


@stop

<script src="../vendor/sweetalert2/sweetalert2.min.js"></script>
<link rel="stylesheet" href="../vendor/sweetalert2/sweetalert2.css">

<script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        $('.mark_as_handled').on('click', function(e){
            let item = $(this);
            let id = item.val();
            $.ajax({
                data: {
                    "_token": "{{ csrf_token() }}"
                },
                url: '/admin/contact_us/id/' + id,
                type: 'PATCH',
                success: function(result) {
                    let icon = '<span class="btn"><i class="fas fa-check" style="color: #00b44e" title="Handled"></i></span>';
                    item.replaceWith(icon);
                },
                error: function (jqXHR, exception) {
                    console.log(jqXHR.responseJSON.message)
                }
            });
        });

        $('.delete_item').on('click', function(e){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    let item = $(this);
                    let id = item.val();
                    $.ajax({
                        data: {
                            "_token": "{{ csrf_token() }}"
                        },
                        url: '/admin/contact_us/id/' + id,
                        type: 'DELETE',
                        success: function(result) {
                            Toast.fire({
                                icon: 'success',
                                title: result.data
                            });
                            item.parents('.item-row').remove();
                        },
                        error: function (jqXHR, exception) {
                            Toast.fire({
                                icon: 'error',
                                title: jqXHR.responseJSON.message
                            });
                        }
                    });
                }
            })
        });
    });

</script>