@extends('adminlte::page')

@section('title', 'Push notifications')

@section('content_header')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('/vendor/datatables/css/dataTables.bootstrap4.min.css') }}">
@stop

@section('content')
    @if($isAdmin)
    <div class="row">
        <div class="card">
            <div class="card-header d-flex p-0">
                <ul class="nav nav-pills p-2">
                    <li class="nav-item"><a class="nav-link active"
                                            href="{{ $links['notifications'] }}">Notifications</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ $links['notificationsCustom'] }}">Custom
                            notifications</a></li>
                </ul>
            </div><!-- /.card-header -->
        </div><!-- /.card-header -->
    </div>
    @endif
    <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Push notifications</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="notification" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>User id</th>
                            <th>Plant id</th>
                            <th>Title</th>
                            <th>Status</th>
{{--                            <th>Response</th>--}}
                            <th>Created at</th>
                            <th>Updated at</th>
                        </tr>
                        </thead>
                        <tbody id="ajaxPosts">
                        @foreach( $items as $index => $item)
                            <tr>
                                <td>{{ $item['id'] }}</td>
                                <td>{{ $item['user_id'] }}</td>
                                <td>{{ $item['plant_id'] }}</td>
                                <td>{{ $item['title'] }}</td>
                                <td>
                                    @if($item['status'])
                                        <i class="fas fa-check" style="color:#82c91e;"></i>
                                    @else
                                        <i class="fas fa-minus" style="color:#b1b1b1;"></i>
                                    @endif
                                </td>
                                {{--<td>
                                    @if($item['response'])
                                        <p title="{{ $item['response'] }}">response</p>
                                    @endif
                                </td>--}}
                                <td>{{ $item['created_at'] }}</td>
                                <td>{{ $item['updated_at'] }}</td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
@stop

<script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="../vendor/datatables/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        // Setup - add a text input to each footer cell
        $('#notification thead tr').clone(true).appendTo( '#notification thead' );
        $('#notification thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Filter"  style="width: 100%;"/>' );

            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );

        var table = $("#notification").DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            order: [0, 'desc'],
            pageLength: 15,
            lengthMenu: [[10, 15, 50, 100, -1], [10, 15, 50, 100, 'All']],
            processing: true,
            serverSide: true,
            ajax: '/admin/notifications_log/get_data',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'user_id', name: 'user_id' },
                { data: 'plant_id', name: 'plant_id' },
                { data: 'title', name: 'title' },
                { data: 'status', name: 'status',
                    render: function (data) {
                        return data ? '<i class="fas fa-check" style="color:#82c91e;"></i>' : '<i class="fas fa-minus" style="color:#b1b1b1;"></i>';
                    },
                },
                { data: 'created_at', name: 'created_at' },
                { data: 'updated_at', name: 'updated_at' }
            ]
        });

        /*var table = $("#notification").DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            order: [0, 'desc'],
            pageLength: 15,
            lengthMenu: [[10, 15, 50, 100, -1], [10, 15, 50, 100, 'All']],
        });*/
    });
</script>