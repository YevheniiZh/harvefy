@extends('adminlte::page')

@section('title', 'Meat')

@section('content_header')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('/vendor/datatables/css/dataTables.bootstrap4.min.css') }}">
    <!-- daterange picker -->
    <link rel="stylesheet" href="../vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <button type="button" class="btn btn-success float-right" id="addMeatBtn" data-toggle="modal" data-target="#newMeat" data-whatever="@mdo">New meat</button>
            <button type="button" class="btn btn-default float-left" id="downloadMeatBtn" data-toggle="modal" data-target="#downloadMeat" data-whatever="@mdo">Download meat</button>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Meat</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="meatsTable" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>User id</th>
                            <th>Land id</th>
                            <th>Meat name</th>
                            <th>Status</th>
                            <th>Created at</th>
                            <th>Autowatering</th>
                            <th>Last watering</th>
                            <th>Updated at</th>
                            @if($isAdmin)
                                <th>Manager</th>
                            @endif
                            <th>Manage</th>
                            {{--<th></th>--}}
                        </tr>
                        </thead>
                        <tbody id="ajaxPosts">
                        @foreach( $items as $index => $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->user_id }}</td>
                                <td>{{ $item->land_id }}</td>
                                <td>{{ $meat_types[$item->meat_type_id]['name'] ?? '' }}</td>
                                <td class="status">{{ $item->status }}
                                    @if($item->status === 2)
                                        <i class="fas fa-exclamation-circle" style="color:red;"
                                           title="{{ $statuses[$item->status] }}"></i>
                                    @elseif($item->status === 4 || $item->status === 8)
                                        <i class="fas fa-times-circle" style="color:red;"
                                           title="{{ $statuses[$item->status] }}"></i>
                                    @elseif($item->status === 5)
                                        <i class="fas fa-shopping-basket" style="color:orange;"
                                           title="{{ $statuses[$item->status] }}"></i>
                                    @elseif($item->status === 6)
                                        <i class="fas fa-truck" style="color:orange;"
                                           title="{{ $statuses[$item->status] }}"></i>
                                    @elseif($item->status === 7)
                                        <i class="fas fa-check" style="color:#82c91e;"
                                           title="{{ $statuses[$item->status] }}"></i>
                                    @elseif($item->status === 3)
                                        <i class="fas fa-seedling" style="color:#00bd00;"
                                           title="{{ $statuses[$item->status] }}"></i>
                                    @else
                                        <i class="fas fa-seedling" style="color:#a9a9a9;"
                                           title="{{ $statuses[$item->status] }}"></i>
                                    @endif
                                </td>
                                <td>{{ $item->created_at }}</td>
                                <td>
                                    @if($item->autowatering)
                                        <i class="fas fa-check" style="color:#82c91e;"></i>
                                    @else
                                        <i class="fas fa-minus" style="color:#b1b1b1;"></i>
                                    @endif
                                </td>
                                <td class="last_watering">{{ $item->last_watering }}</td>
                                <td class="updated_at">{{ $item->updated_at }}</td>
                                @if($isAdmin)
                                    <td data-id="{{ $item['user']['partner_id'] }}">
                                        @if($partners[$item['user']['partner_id']]??false)
                                            {{ $partners[$item['user']['partner_id']]['name'] }} {{ $partners[$item['user']['partner_id']]['surname'] }}
                                        @else
                                            Наши
                                        @endif
                                    </td>
                                @endif
{{--                                <th><a href="{{ Request::url() }}/{{ $item->id }}">подробнее</a></th>--}}
                                <td>
                                    <div class="btn-group">
                                        {{--@if($item->status == 4)
                                        <button type="button" value="{{ $item->id }}" class="btn btn-danger delete_post" title="Delete" disabled><i class="fas fa-trash"></i></button>
                                        @endif--}}
                                        @if($item->status === 2)
                                            <button type="button" value="{{ $item->id }}" class="btn btn-default pour_on" title="Water the meat">
                                                <i class="fas fa-shower" style="color:green;"></i>
                                            </button>
                                        @elseif($item->status == 6)
                                            <a href="{{ $deliveryLink }}?id={{ $item->id }}" class="btn btn-success" title="Delivery"><i class="fas fa-truck"></i></a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->


        </div>
        <!-- /.col -->
    </div>

    <div class="modal fade" id="newMeat" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">New meat {{ $lastItemsPeriod }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="invoice">
                    <div class="card-body">
                        <table class="table table-bordered table-hover" border="1" cellpadding="3">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Meat type</th>
                                <th>Quantity</th>
                                <th>Meated</th>
                            </tr>
                            </thead>
                            <tbody id="ajaxPosts">
                            @foreach( $lastItems as $index => $item)
                                <tr>
                                    <td>{{ $item->meat_type_id }}</td>
                                    <td>{{ $meat_types[$item->meat_type_id]['name'] ?? '' }}</td>
                                    <td>{{ $item->count }}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="printDiv()">Print</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="downloadMeat" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Download meat</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="invoice">
                    <div class="card-body">
                        <!-- Date and time range -->
                        <div class="form-group">
                            <div class="input-group">
                                <button type="button" class="btn btn-default float-right" id="daterange-btn">
                                    <i class="far fa-calendar-alt"></i> <span>Choose date range</span>
                                    <i class="fas fa-caret-down"></i>
                                </button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-switch custom-switch-on-success">
                                <input type="checkbox" class="custom-control-input" name="is_payed" id="is_payed" checked>
                                <label class="custom-control-label" for="is_payed">Show only payed</label>
                            </div>
                        </div>

                        <div id="tableWrapper" style="display: none">
                            <table id="downloadMeatTable" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>User id</th>
                                    <th>Land id</th>
                                    <th>Meat type</th>
                                    <th>Autowatering</th>
                                    <th>Paid</th>
                                    <th>Created at</th>
                                </tr>
                                </thead>
                                <tbody id="ajaxPosts">
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop

<script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="../vendor/datatables/js/dataTables.bootstrap4.min.js"></script>

<!-- Bootstrap 4 -->
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="../vendor/select2/js/select2.full.min.js"></script>
<script src="../vendor/moment/moment.min.js"></script>
<!-- date-range-picker -->
<script src="../vendor/daterangepicker/daterangepicker.js"></script>
<script src="../vendor/sweetalert2/sweetalert2.min.js"></script>
<link rel="stylesheet" href="../vendor/sweetalert2/sweetalert2.css">

<script type="text/javascript" src="../vendor/datatables-plugins/buttons/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="../vendor/datatables-plugins/buttons/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="../vendor/datatables-plugins/jszip/jszip.min.js"></script>
<script type="text/javascript" src="../vendor/datatables-plugins/pdfmake/pdfmake.min.js"></script>
<script type="text/javascript" src="../vendor/datatables-plugins/pdfmake/vfs_fonts.js"></script>
<script type="text/javascript" src="../vendor/datatables-plugins/buttons/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="../vendor/datatables-plugins/buttons/js/buttons.print.min.js"></script>
<script>
var partners = {!! json_encode($partners) !!};
var isAdmin = {{$isAdmin}};

$(document).ready(function() {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    $("#meatsTable").DataTable({
        order: [0, 'desc'],
        pageLength : 15,
        lengthMenu: [[10, 15, 50, 100, -1], [10, 15, 50, 100, 'All']],
        initComplete: function () {
            if (!isAdmin) {
                return;
            }
            this.api().columns([9]).every( function () {
                var column = this;
                var select = $('<select><option value="">Все</option></select>')
                    .appendTo( $(column.header()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column.search( val ? '^'+val+'$' : '', true, false ).draw();
                    } );

                select.append( '<option value="Наши">Наши</option>' )
                $.each(partners, function ( i, item ) {
                    let name = item.name + (item.surname ? ' '+ item.surname : '');
                    select.append( '<option value="'+name+'">'+name + '</option>' )
                } );
            } );
        }
    });

    let table = $("#downloadMeatTable").DataTable({
        order: [0, 'desc'],
        searching: false,
        paging: false,
        info: false,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        columns: [
            {data: "id"},
            {data: "user_id"},
            {data: "land_id"},
            {data: "meat_type.name"},
            {data: "autowatering"},
            {data: "land.is_payed"},
            {data: "created_at"}
        ]
    });

    let startDate;
    let endDate;

    var start = moment();
    var end = moment();

    function cb(start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#tableWrapper').show();
        startDate = start;
        endDate = end;
    }

    let daterangepicker = $('#daterange-btn').daterangepicker(
        {
            ranges   : {
                'Today'       : [moment(), moment()],
                'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: start,
            endDate: end,
        },
        cb
    );

    cb(start, end);
    updateTable();

    daterangepicker.on('apply.daterangepicker', function() {
        updateTable();
    });

    $('#is_payed').change(function() {
        updateTable();
    });

    function updateTable() {
        if (!startDate || !endDate) {
            return;
        }
        let start = startDate.format('YYYY-MM-DD');
        let end = endDate.format('YYYY-MM-DD');
        let only_payed = $('#is_payed').is(":checked");

        $.ajax({
            data: {
                "_token": "{{ csrf_token() }}",
                'start': start,
                'end': end,
                'only_payed': only_payed ? 1 : 0,
            },
            url: '/admin/meat/get_by_date',
            type: 'POST',
            success: function(result) {
                let items = result.data;
                table
                    .clear()
                    .rows.add( items)
                    .draw();

            }
        });
    }

    $(document).on('click', '.pour_on', function(e){
        e.preventDefault();
        let item = $(this);
        var id = item.val();
        $.ajax({
            data: {
                "_token": "{{ csrf_token() }}"
            },
            url: '/admin/meat/pour_on/id/' + id,
            type: 'POST',
            success: function(result) {
                let meat = result.data;
                let icon = '<i class="fas fa-seedling" style="color:#a9a9a9;"></i>';
                let tr = item.parents('tr');
                tr.find('.status').html(meat.status + icon);
                tr.find('.last_watering').text(meat.last_watering);
                tr.find('.updated_at').text(meat.updated_at);
                item.remove();
            },
            error: function (jqXHR, exception) {
                Toast.fire({
                    icon: 'error',
                    title: jqXHR.responseJSON.message
                });
            }
        });
    });
});

function printDiv() {
    let divToPrint = document.getElementById('invoice');
    newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
}
</script>