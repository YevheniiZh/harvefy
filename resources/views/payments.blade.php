@extends('adminlte::page')

@section('title', 'Payments')

@section('content_header')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('/vendor/datatables/css/dataTables.bootstrap4.min.css') }}">
@stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Payments</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="payments" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>User id</th>
                            <th>Email</th>
                            <th>Item id</th>
                            <th>Item type</th>
                            <th>Amount</th>
                            <th>Currency</th>
                            <th>Status</th>
                            <th>ps_response</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                            @if($isAdmin)
                                <th>Manager</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody id="ajaxPosts">
                        @foreach( $transactions as $index => $item)
                            <tr>
                                <td>{{ $item['id'] }}</td>
                                <td>{{ $item['user_id'] }}</td>
                                <td>{{ $item['user']['email'] }}</td>
                                <td>{{ $item['item_id'] }}</td>
                                <td>{{ $item['item_type'] }}</td>
                                <td>{{ $item['amount'] }}</td>
                                <td>{{ $item['currency'] }}</td>
                                <td>{{ $item['status'] }}</td>
                                <td>
                                    @if($item['ps_response'])
                                        <p title="{{ $item['ps_response'] }}">response</p>
                                    @endif
                                </td>
                                <td>{{ $item['created_at'] }}</td>
                                <td>{{ $item['updated_at'] }}</td>
                                @if($isAdmin)
                                    <td data-id="{{ $item['user']['partner_id'] }}">
                                        @if($partners[$item['user']['partner_id']]??false)
                                            {{ $partners[$item['user']['partner_id']]['name'] }} {{ $partners[$item['user']['partner_id']]['surname'] }}
                                        @else
                                            Наши
                                        @endif
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="10" style="text-align:right">TOTAL:</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->


        </div>
        <!-- /.col -->
    </div>


@stop

<script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="../vendor/datatables/js/dataTables.bootstrap4.min.js"></script>
<script>
var partners = {!! json_encode($partners) !!};
var isAdmin = {{$isAdmin}};

$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#payments thead tr').clone(true).appendTo( '#payments thead' );
    $('#payments thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Filter"  style="width: 100%;"/>' );

        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    var table = $("#payments").DataTable({
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
            let amountColumnNumber = 5;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return i * 1;
            };
            // Total over all pages
            /*total = api
                .column( amountColumnNumber )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );*/

            // Total over this page
            pageTotal = api
                .column( amountColumnNumber, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            $( api.column( 10 ).footer() ).html(
                pageTotal + ' UAH'
            );
        },
        orderCellsTop: true,
        fixedHeader: true,
        order: [0, 'desc'],
        pageLength: 15,
        lengthMenu: [[10, 15, 50, 100, -1], [10, 15, 50, 100, 'All']],
        initComplete: function () {
            if (!isAdmin) {
                return;
            }
            this.api().columns([11]).every( function () {
                var column = this;
                var select = $('<select><option value="">Все</option></select>')
                    .appendTo( $(column.header()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column.search( val ? '^'+val+'$' : '', true, false ).draw();
                    } );

                select.append( '<option value="Наши">Наши</option>' )
                $.each(partners, function ( i, item ) {
                    let name = item.name + (item.surname ? ' '+ item.surname : '');
                    select.append( '<option value="'+name+'">'+name + '</option>' )
                } );
            } );
        }
    });
});
</script>