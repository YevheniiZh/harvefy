@extends('adminlte::page')

@section('title', 'Subscriptions')

@section('content_header')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('/vendor/datatables/css/dataTables.bootstrap4.min.css') }}">
@stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Subscriptions</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="subscriptions" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>User id</th>
                            <th>Email</th>
                            <th>Land id</th>
                            <th>Status</th>
{{--                            <th>Auto renewal</th>--}}
                            <th>Next payment</th>
                            <th>Canceled</th>
                            <th>Cancel date</th>
{{--                            <th>end date</th>--}}
                            <th>Renew</th>
                            <th>Created at</th>
                            @if($isAdmin)
                                <th>Manager</th>
                            @endif
{{--                            <th>Updated at</th>--}}
                        </tr>
                        </thead>
                        <tbody id="ajaxPosts">
                        @foreach( $items as $index => $item)
                            @if(!$item['payment_id'])
                                @continue
                            @endif
                            <tr>
                                <td>{{ $item['id'] }}</td>
                                <td>{{ $item['user_id'] }}</td>
                                <td>{{ $item['user']['email'] }}</td>
                                <td>{{ $item['land_id'] }}</td>
                                <td title="{{ $statuses[$item['status']] }}">{{ $item['status'] }}</td>
                                {{--<td>
                                    @if($item['auto_renewal'])
                                        <i class="fas fa-check" style="color:#82c91e;"></i>
                                    @else
                                        <i class="fas fa-minus" style="color:#b1b1b1;"></i>
                                    @endif
                                </td>--}}
                                <td>{{ $item['next_payment'] }}</td>
                                <td>
                                    @if($item['canceled'])
                                        <i class="fas fa-check" style="color:black;"></i>
                                    @else
                                        <i class="fas fa-minus" style="color:#b1b1b1;"></i>
                                    @endif
                                </td>
                                <td>{{ $item['cancel_date'] }}</td>
{{--                                <td>{{ $item['end_date'] }}</td>--}}
                                <td>{{ $item['renew'] }}</td>
                                <td>{{ $item['created_at'] }}</td>
                                @if($isAdmin)
                                    <td data-id="{{ $item['user']['partner_id'] }}">
                                        @if($partners[$item['user']['partner_id']]??false)
                                            {{ $partners[$item['user']['partner_id']]['name'] }} {{ $partners[$item['user']['partner_id']]['surname'] }}
                                        @else
                                            Наши
                                        @endif
                                    </td>
                                @endif
{{--                                <td>{{ $item['updated_at'] }}</td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->


        </div>
        <!-- /.col -->
    </div>
@stop

<script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="../vendor/datatables/js/dataTables.bootstrap4.min.js"></script>
<script>
var partners = {!! json_encode($partners) !!};
var isAdmin = {{$isAdmin}};

$(document).ready(function() {
        // Setup - add a text input to each footer cell
        $('#subscriptions thead tr').clone(true).appendTo( '#subscriptions thead' );
        $('#subscriptions thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Filter"  style="width: 100%;"/>' );

            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );

        var table = $("#subscriptions").DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            order: [0, 'desc'],
            pageLength: 15,
            lengthMenu: [[10, 15, 50, 100, -1], [10, 15, 50, 100, 'All']],
            initComplete: function () {
                if (!isAdmin) {
                    return;
                }
                this.api().columns([10]).every( function () {
                    var column = this;
                    var select = $('<select><option value="">Все</option></select>')
                        .appendTo( $(column.header()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
                            column.search( val ? '^'+val+'$' : '', true, false ).draw();
                        } );

                    select.append( '<option value="Наши">Наши</option>' )
                    $.each(partners, function ( i, item ) {
                        let name = item.name + (item.surname ? ' '+ item.surname : '');
                        select.append( '<option value="'+name+'">'+name + '</option>' )
                    } );
                } );
            }
        });
    });
</script>