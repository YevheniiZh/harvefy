@extends('adminlte::page')

@section('title', 'Recipes')

@section('content')
    <div class="row">
        <div class="col-12">
            <button type="button" class="btn btn-success float-right" id="addRecipeBtn" data-toggle="modal" data-target="#addRecipe">Add recipe</button>
        </div>
    </div>
    <hr>

    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Recipes list</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Image</th>
                            <th>Text</th>
                            <th>Plant types</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                            <th>Manage</th>
                        </tr>
                        </thead>
                        <tbody id="ajaxPosts">
                        @foreach( $items as $index => $item)
                            <tr class="item-row">
                                <td>{{ $item->id }}</td>
                                <td><img src="{{ $item->image }}" style="
    max-width: 50px;
    max-height: 50px;
"></td>
                                <td>{!! \Illuminate\Support\Str::limit(strip_tags($item->recipe), 20, '...') !!}
                                    <a href="#" data-toggle="modal" data-target="#modal-lg" data-name="{{ $item->id }}" data-full_desc="{{ $item->recipe }}">View full recipe</a>
                                </td>
                                <td>
                                    @foreach($item->plant_type_ids as $plant_type_id){{ $loop->first ? '' : ', ' }}
                                    {{ $plant_types[$plant_type_id]['name'] ?? '' }}@endforeach
                                </td>
                                <td>{{ $item->created_at }}</td>
                                <td>{{ $item->updated_at }}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" value="{{ $item->id }}" class="btn btn-default edit_post" title="Edit" data-toggle="modal" data-target="#addRecipe" di><i class='fas fa-edit'></i></button>
                                        <button type="button" value="{{ $item->id }}" class="btn btn-danger delete_item" title="Remove"><i class='fas fa-trash-alt'></i></button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>

    <div class="modal fade" id="modal-lg" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addRecipe" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabel">New plant</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <form role="form" id="myForm" method="post" action="/admin/recipes"  enctype="multipart/form-data">
                <div class="modal-body">
                    <p id="form-errors" class="text-center error invalid-feedback" style="display: inherit;"></p>
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <textarea class="form-control textarea" rows="3" id="recipe" name="recipe" placeholder="Enter recipe"></textarea>
                            </div>
                            <div class="form-group">
                                <select class="select2" id="plant_type_ids" multiple="multiple" name="plant_type_ids[]" data-placeholder="Select plant type" style="width: 100%;">
                                    @foreach( $plant_types as $index => $item)
                                        <option value="{{ $index }}">{{ $item['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <img id='img-upload' style="max-width: 38px; max-height: 38px"/>
                                    <input type="text" class="form-control" readonly>
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                            Choose image file<input id="imageFile" type="file" class="custom-file-input" name="file">
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submitModal" data-mode='save' class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@stop
<script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>

<!-- Summernote -->
<script src="../vendor/summernote/summernote.js" defer></script>
<link rel="stylesheet" href="../vendor/summernote/summernote.css" />
<!-- sweetalert2 -->
<script src="../vendor/sweetalert2/sweetalert2.min.js"></script>
<link rel="stylesheet" href="../vendor/sweetalert2/sweetalert2.css">
<!-- Select2 -->
<link rel="stylesheet" href="../vendor/select2/css/select2.min.css">
<script src="../vendor/select2/js/select2.full.js" defer></script>
<!-- jquery-validation -->
<script src="../vendor/jquery-validation/jquery.validate.min.js" defer></script>
<script src="../vendor/jquery-validation/additional-methods.min.js" defer></script>
<style>
    .select2-results__option[aria-selected=true] {
        display: none;
    }
</style>
<script>
    var items = {!! json_encode($itemsJs) !!};

    $(document).ready(function() {
        $('#plant_type_ids').select2({placeholder: "Select plant type"});
        //$('.textarea').summernote();

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        $('.edit_post').on('click', function(e){
            let id = $(this).val();
            $('#modalLabel').text('Edit recipe (id: ' + id + ')');
            $('#submitModal').text('Update').attr('data-mode', 'edit').attr('data-id', id);
            populate('#myForm', items[id]);
        });

        function populate(frm, data) {
            $.each(data, function(key, value) {
                var ctrl = $('[id='+key+']', frm);
                switch(ctrl.prop("type")) {
                    case "radio": case "checkbox":
                        ctrl.each(function() {
                            if($(this).attr('value') == value) $(this).attr("checked",value);
                        });
                        break;
                    default:
                        ctrl.val(value);
                }
                if (key === 'plant_type_ids') {
                    ctrl.val(value).trigger('change');
                } else if (key === 'image') {
                    //$('.btn-file :file').trigger('fileselect', [value.split('/').pop()]);
                    $('#img-upload').attr('src', value);
                }
            });
        }

        $('.delete_item').on('click', function(e){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    let item = $(this);
                    let id = item.val();
                    $.ajax({
                        data: {
                            "_token": "{{ csrf_token() }}"
                        },
                        url: '/admin/recipes/id/' + id,
                        type: 'DELETE',
                        success: function(result) {
                            Toast.fire({
                                icon: 'success',
                                title: result.data
                            });
                            item.parents('.item-row').remove();
                        }
                    });
                }
            })
        });

        $("form#myForm").on('submit', function(e){
            e.preventDefault();
            let data = new FormData(this);
            $('#form-errors').text('');
            let method, url;
            if ($('#submitModal').data('mode') === 'edit') {
                let id = $('#submitModal').data('id');
                data.append('fileLink', items[id]['image']);
                url = '/admin/recipes/id/' + id;
            } else {
                url = '/admin/recipes';
            }
            $.ajax({
                url: url,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
                success: function(result){
                    Toast.fire({
                        icon: 'success',
                        title: result.data
                    });
                    $('#addRecipe').modal('hide');
                    location.reload();
                },
                error: function (jqXHR, exception) {
                    Toast.fire({
                        icon: 'error',
                        title: jqXHR.responseJSON.message
                    });
                    $.each(jqXHR.responseJSON.errors, function (key, item) {
                        $('#form-errors').append(item.message + '<br>');
                    });
                },
            });
        });

        // File input preview
        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }

        });
        function readURL(input) {
            $('#img-upload').removeAttr('src');
            if (input.files && input.files[0] && input.files[0].type.includes('image')) {
                console.log(input.files[0])
                var reader = new FileReader();
                reader.onload = function (e) {
                    if (e.target.result) {
                        $('#img-upload').attr('src', e.target.result);
                    }
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imageFile").change(function(){
            readURL(this);
        });

        $('#modal-lg').on('show.bs.modal', function (event) {
            const button = $(event.relatedTarget);
            //const name = button.data('name');
            const full_desc = button.data('full_desc');
            const modal = $(this);
            //modal.find('.modal-title').text(name);
            modal.find('.modal-body').html(full_desc);
        });

        $('#addRecipeBtn').on('click', function (event) {
            $('#modalLabel').text('New plant');
            $('#submitModal').text('Save').attr('data-mode', 'save');
        });

        $('#addRecipe').on('hidden.bs.modal', function(){
            $(this).find('form')[0].reset();
            $('#plant_type_ids').val('').trigger('change');
            $('#img-upload').removeAttr('src');
            $('#form-errors').text('');
        });
    });
</script>