@php
    $land_type_id = 0
@endphp

@extends('adminlte::page')

@section('title', 'Land images')

@section('content')
    <div class="row">
        <div class="col-12">
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#addType" data-whatever="@mdo">Add type</button>
            <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#addPlant" data-whatever="@mdo">Add image</button>
        </div>
    </div>
    <hr>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                @foreach( $items as $index => $item)
                                    @if( $land_type_id !== $item->land_type_id)
                                        </div>
                                        <hr><br>
                                        <h3>{{ $image_types[$item->land_type_id] ?? '' }}</h3>
                                        <div class="row">
                                            @if($land_type_id = $item->land_type_id) @endif
                                    @endif
                                <div class="col-sm-2 item-row">
                                    <a href="{{ $item->image }}" data-toggle="lightbox" data-title="" data-gallery="gallery">
                                        <img src="{{ $item->image }}" class="img-fluid mb-2" alt="white sample"/>
                                    </a>
                                    {{-- If not microgreen --}}
                                    @if($item->land_type_id == 2)
                                        <p>{{ $meat_types[$item->product_type_id]['name'] ?? '' }}</p>
                                    @endif
                                    <button type="button" value="{{ $item->id }}" class="btn btn-danger delete_item" title="Remove" style="
    bottom: 38px;
    position: relative;
    float: right;
"><i class='fas fa-trash-alt'></i></button>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="modal fade" id="addPlant" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">New image</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <form role="form" id="myForm" method="post" action="/admin/recipes"  enctype="multipart/form-data">
                <div class="modal-body">
                    <p id="form-errors" class="text-center error invalid-feedback" style="display: inherit;"></p>
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <div class="input-group">
                                    <img id='img-upload' style="max-width: 38px; max-height: 38px"/>
                                    <input type="text" class="form-control" readonly>
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                            Choose image file<input id="imageFile" type="file" class="custom-file-input" name="file">
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <select class="select2" id="land_type_id" name="land_type_id" data-placeholder="Select land type" style="width: 100%;" onchange="landTypeSelect()">
                                    @foreach( $image_types as $index => $item)
                                        <option value="{{ $index }}">{{ $item }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="select2" id="product_type_id" name="product_type_id" data-placeholder="Select product type" style="width: 100%;">
                                    @foreach( $meat_types as $index => $item)
                                        <option value="{{ $item['id'] }}">{{ $item['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="file_name" name="file_name" placeholder="File name">
                            </div>
                        </div>
                        <!-- /.card-body -->

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">
                        <span id="btn-spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true" style="display: none"></span>
                        Save
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addType" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">New image type</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <form role="form" id="addTypeForm" method="post" action="/admin/image/type"  enctype="multipart/form-data">
                <div class="modal-body">
                    <p id="form-errors" class="text-center error invalid-feedback" style="display: inherit;"></p>
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Type name">
                            </div>
                        </div>
                        <!-- /.card-body -->

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">
                        <span id="btn-spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true" style="display: none"></span>
                        Save
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
@stop
<script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>

<!-- sweetalert2 -->
<script src="../vendor/sweetalert2/sweetalert2.min.js"></script>
<link rel="stylesheet" href="../vendor/sweetalert2/sweetalert2.css">
<!-- Select2 -->
<link rel="stylesheet" href="../vendor/select2/css/select2.min.css">
<script src="../vendor/select2/js/select2.full.js" defer></script>
<!-- jquery-validation -->
<script src="../vendor/jquery-validation/jquery.validate.min.js" defer></script>
<script src="../vendor/jquery-validation/additional-methods.min.js" defer></script>
<!-- Ekko Lightbox -->
<link rel="stylesheet" href="../vendor/ekko-lightbox/ekko-lightbox.css">
<script src="../vendor/ekko-lightbox/ekko-lightbox.min.js" defer></script>

<script>
    $(document).ready(function() {
        $('#land_type_id').select2({placeholder: "Select land type"});
        $('#product_type_id').select2({placeholder: "Select product type"});
        landTypeSelect();

        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox({
                alwaysShowClose: true
            });
        });

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        $("form#myForm").on('submit', function(e){
            e.preventDefault();
            let data = new FormData(this);
            $('#form-errors').text('');
            $('#btn-spinner').show();
            $.ajax({
                url: '/admin/images',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
                success: function(data){
                    Toast.fire({
                        icon: 'success',
                        title: data.data
                    });
                    $('#addPlant').modal('hide');
                    location.reload();
                },
                error: function (jqXHR, exception) {
                    Toast.fire({
                        icon: 'error',
                        title: jqXHR.responseJSON.message
                    });
                    $.each(jqXHR.responseJSON.errors, function (key, item) {
                        $('#form-errors').append(item.message + '<br>');
                    });
                },
                always: function() {
                    alert( "complete" );
                }
            }).always(function() {
                $('#btn-spinner').hide();
            });
        });

        $("form#addTypeForm").on('submit', function(e){
            e.preventDefault();
            let data = new FormData(this);
            $('#form-errors').text('');
            $('#btn-spinner').show();
            $.ajax({
                url: '/admin/images/type',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
                success: function(data){
                    Toast.fire({
                        icon: 'success',
                        title: data.data
                    });
                    $('#addType').modal('hide');
                    location.reload();
                },
                error: function (jqXHR, exception) {
                    Toast.fire({
                        icon: 'error',
                        title: jqXHR.responseJSON.message
                    });
                    $.each(jqXHR.responseJSON.errors, function (key, item) {
                        $('#form-errors').append(item.message + '<br>');
                    });
                },
                always: function() {
                    alert( "complete" );
                }
            }).always(function() {
                $('#btn-spinner').hide();
            });
        });

        $('.delete_item').on('click', function(e){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    let item = $(this);
                    let id = item.val();
                    $.ajax({
                        data: {
                            "_token": "{{ csrf_token() }}"
                        },
                        url: '/admin/images/id/' + id,
                        type: 'DELETE',
                        success: function(result) {
                            Toast.fire({
                                icon: 'success',
                                title: result.data
                            });
                            item.parents('.item-row').remove();
                        }
                    });
                }
            })
        });

        // File input preview
        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }

        });
        function readURL(input) {
            $('#img-upload').removeAttr('src');
            if (input.files && input.files[0] && input.files[0].type.includes('image')) {
                console.log(input.files[0])
                var reader = new FileReader();
                reader.onload = function (e) {
                    if (e.target.result) {
                        $('#img-upload').attr('src', e.target.result);
                    }
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imageFile").change(function(){
            readURL(this);
        });
    });

    function landTypeSelect() {
        let id = parseInt($('#land_type_id').val());

        if (id !== 1 && id !== 2) {
            $('#file_name').parent('.form-group').show();
        } else {
            $('#file_name').parent('.form-group').hide();
        }

        if (id !== 2) {
            $('#product_type_id').parent('.form-group').hide();
        } else {
            $('#product_type_id').parent('.form-group').show()
        }
    }

</script>