<table class="table table-bordered table-hover" border="1" cellpadding="3">
    <thead>
    <tr>
        <th>Id</th>
        <th>Plant type</th>
        <th>Quantity</th>
        <th>Planted</th>
    </tr>
    </thead>
    <tbody id="ajaxPosts">
    @foreach( $lastItems as $index => $item)
        <tr>
            <td>{{ $item->plant_type_id }}</td>
            <td>{{ $product_types[$item->plant_type_id]['name'] }}</td>
            <td>{{ $item->count }}</td>
            <td></td>
        </tr>
    @endforeach
    </tfoot>
</table>