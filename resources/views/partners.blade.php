@extends('adminlte::page')

@section('title', 'Partners')

@section('content')
    <div class="row">
        <div class="col-12">
            <button type="button" class="btn btn-success float-right" id="addPlantBtn" data-toggle="modal" data-target="#addPartner" data-whatever="@mdo">Add partner</button>
        </div>
    </div>
    <hr>

    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Partners</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Surname</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Promo code</th>
                            <th>Registration date</th>
                            <th>Edit date</th>
                            <th>Manage</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $partners as $index => $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->surname }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone }}</td>
                                <td>{{ $user->promocode }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>{{ $user->updated_at }}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" value="{{ $user->id }}" class="btn btn-default edit_post" title="Edit" data-toggle="modal" data-target="#addPartner" di><i class='fas fa-edit'></i></button>
                                    </div>
                                </td>
                            </tr>                                {{--<th><a href="{{ Request::url() }}/{{ $user->id }}">подробнее</a></th>--}}

                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

    <div class="modal fade" id="addPartner" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabel">New partner</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <form role="form" id="myForm" method="post" action="/admin/cities"  enctype="multipart/form-data">
                    <div class="modal-body">
                        <p id="form-errors" class="text-center error invalid-feedback" style="display: inherit;"></p>
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" name="name">
                            </div>
                            <div class="form-group">
                                <label>Surname</label>
                                <input type="text" class="form-control" name="surname">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" name="email">
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="text" class="form-control" name="phone">
                            </div>
                            <div class="form-group">
                                <label>Promocode</label>
                                <input type="text" class="form-control" name="promocode">
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" id="submitModal" data-mode='save' class="btn btn-primary">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<script src="../vendor/sweetalert2/sweetalert2.min.js"></script>
<link rel="stylesheet" href="../vendor/sweetalert2/sweetalert2.css">
<script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>

<script>
    var items = {!! json_encode($itemsJs) !!};

    $(document).ready(function() {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        $('.edit_post').on('click', function(e){
            let id = $(this).val();
            $('#modalLabel').text('Edit partner (id: ' + id + ')');
            $('#submitModal').text('Update').attr('data-mode', 'edit').attr('data-id', id);
            $('[name="email"]').parent('.form-group').hide();
            //$('#myForm input').text('Update').attr('data-mode', 'edit').attr('data-id', id);
            populate('#myForm', items[id]);
        });

        function populate(frm, data) {
            $.each(data, function(key, value) {
                var ctrl = $('[name='+key+']', frm);
                switch(ctrl.prop("type")) {
                    case "radio": case "checkbox":
                        ctrl.each(function() {
                            if($(this).attr('value') == value) $(this).attr("checked",value);
                        });
                        ctrl.attr("checked",value);
                        break;
                    default:
                        ctrl.val(value);
                }
            });
        }

        $("form#myForm").on('submit', function(e){
            e.preventDefault();
            let data = new FormData(this);
            $('#form-errors').text('');
            let method, url;
            if ($('#submitModal').data('mode') === 'edit') {
                let id = $('#submitModal').data('id');
                data.append('fileLink', items[id]['image']);
                url = '/admin/partners/id/' + id;
            } else {
                url = '/admin/partners';
            }
            $.ajax({
                url: url,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
                success: function(data){
                    Toast.fire({
                        icon: 'success',
                        title: data.data
                    });
                    $('#addPartner').modal('hide');
                    location.reload();
                },
                error: function (jqXHR, exception) {
                    Toast.fire({
                        icon: 'error',
                        title: jqXHR.responseJSON.message
                    });
                    $.each(jqXHR.responseJSON.errors, function (key, item) {
                        $('#form-errors').append(item.message + '<br>');
                    });
                    $("#addPartner").scrollTop(0);
                },
            });
        });

        $('#addPartner').on('hidden.bs.modal', function(){
            $(this).find('form')[0].reset();
            $('#img-upload').removeAttr('src');
            $('#form-errors').text('');
        });

        $('.delete_item').on('click', function(e){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    let item = $(this);
                    let id = item.val();
                    $.ajax({
                        data: {
                            "_token": "{{ csrf_token() }}"
                        },
                        url: '/admin/cities/id/' + id,
                        type: 'DELETE',
                        success: function(result) {
                            Toast.fire({
                                icon: 'success',
                                title: result.data
                            });
                            item.parents('.item-row').remove();
                        },
                        error: function (jqXHR, exception) {
                            Toast.fire({
                                icon: 'error',
                                title: jqXHR.responseJSON.message
                            });
                            $.each(jqXHR.responseJSON.errors, function (key, item) {
                                $('#form-errors').append(item.message + '<br>');
                            });
                        }
                    });
                }
            })
        });

        $('#addPlantBtn').on('click', function (event) {
            $('#modalLabel').text('New partner');
            $('#submitModal').text('Save').attr('data-mode', 'save');
            $('[name="email"]').parent('.form-group').show();
        });

        $('#modal-lg').on('show.bs.modal', function (event) {
            const button = $(event.relatedTarget);
            const name = button.data('name');
            const full_desc = button.data('full_desc');
            const modal = $(this);
            modal.find('.modal-title').text(name);
            modal.find('.modal-body').html(full_desc);
        });
    });
</script>
@stop