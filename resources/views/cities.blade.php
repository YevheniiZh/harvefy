@extends('adminlte::page')

@section('title', 'Cities')

@section('content')
    <div class="row">
        <div class="col-12">
            <button type="button" class="btn btn-success float-right" id="addPlantBtn" data-toggle="modal" data-target="#addPlant" data-whatever="@mdo">Add city</button>
        </div>
    </div>
<hr>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Cities list</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Plants available</th>
                            <th>Meat available</th>
                            <th>Manage</th>
                        </tr>
                        </thead>
                        <tbody id="ajaxPosts">
                        @foreach( $items as $index => $item)
                            <tr class="item-row">
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->name }}</td>
                                <td>
                                    @if($item->plants_available)
                                        <i class="fas fa-check" style="color:#82c91e;"></i>
                                    @else
                                        <i class="fas fa-minus" style="color:#b1b1b1;"></i>
                                    @endif
                                </td>
                                <td>
                                    @if($item->meat_available)
                                        <i class="fas fa-check" style="color:#82c91e;"></i>
                                    @else
                                        <i class="fas fa-minus" style="color:#b1b1b1;"></i>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" value="{{ $item->id }}" class="btn btn-default edit_post" title="Edit" data-toggle="modal" data-target="#addPlant"><i class='fas fa-edit'></i></button>
                                        <button type="button" value="{{ $item->id }}" class="btn btn-danger delete_item" title="Remove"><i class='fas fa-trash-alt'></i></button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>

    <div class="modal fade" id="modal-lg" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addPlant" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabel">New city</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <form role="form" id="myForm" method="post" action="/admin/cities"  enctype="multipart/form-data">
                <div class="modal-body">
                    <p id="form-errors" class="text-center error invalid-feedback" style="display: inherit;"></p>
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" name="name">
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-switch custom-switch-on-success">
                                    <input type="checkbox" class="custom-control-input" name="plants_available" id="plants_available">
                                    <label class="custom-control-label" for="plants_available">Plants available</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-switch custom-switch-on-success">
                                    <input type="checkbox" class="custom-control-input" name="meat_available" id="meat_available">
                                    <label class="custom-control-label" for="meat_available">Meat available</label>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submitModal" data-mode='save' class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>

<script src="../vendor/sweetalert2/sweetalert2.min.js"></script>
<link rel="stylesheet" href="../vendor/sweetalert2/sweetalert2.css">
<script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>

<script>
    var items = {!! json_encode($itemsJs) !!};

    $(document).ready(function() {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        $('.edit_post').on('click', function(e){
            let id = $(this).val();
            $('#modalLabel').text('Edit city (id: ' + id + ')');
            $('#submitModal').text('Update').attr('data-mode', 'edit').attr('data-id', id);
            populate('#myForm', items[id]);
        });

        function populate(frm, data) {
            $.each(data, function(key, value) {
                var ctrl = $('[name='+key+']', frm);
                switch(ctrl.prop("type")) {
                    case "radio": case "checkbox":
                        ctrl.each(function() {
                            if($(this).attr('value') == value) $(this).attr("checked",value);
                        });
                        ctrl.attr("checked",value);
                        break;
                    default:
                        ctrl.val(value);
                }
            });
        }

        $("form#myForm").on('submit', function(e){
            e.preventDefault();
            let data = new FormData(this);
            $('#form-errors').text('');
            let method, url;
            if ($('#submitModal').data('mode') === 'edit') {
                let id = $('#submitModal').data('id');
                data.append('fileLink', items[id]['image']);
                url = '/admin/cities/id/' + id;
            } else {
                url = '/admin/cities';
            }
            $.ajax({
                url: url,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
                success: function(data){
                    Toast.fire({
                        icon: 'success',
                        title: data.data
                    });
                    $('#addPlant').modal('hide');
                    location.reload();
                },
                error: function (jqXHR, exception) {
                    Toast.fire({
                        icon: 'error',
                        title: jqXHR.responseJSON.message
                    });
                    $.each(jqXHR.responseJSON.errors, function (key, item) {
                        $('#form-errors').append(item.message + '<br>');
                    });
                    $("#addPlant").scrollTop(0);
                },
            });
        });

        $('#addPlant').on('hidden.bs.modal', function(){
            $(this).find('form')[0].reset();
            $('#img-upload').removeAttr('src');
            $('#form-errors').text('');
        });

        $('.delete_item').on('click', function(e){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    let item = $(this);
                    let id = item.val();
                    $.ajax({
                        data: {
                            "_token": "{{ csrf_token() }}"
                        },
                        url: '/admin/cities/id/' + id,
                        type: 'DELETE',
                        success: function(result) {
                            Toast.fire({
                                icon: 'success',
                                title: result.data
                            });
                            item.parents('.item-row').remove();
                        },
                        error: function (jqXHR, exception) {
                            Toast.fire({
                                icon: 'error',
                                title: jqXHR.responseJSON.message
                            });
                            $.each(jqXHR.responseJSON.errors, function (key, item) {
                                $('#form-errors').append(item.message + '<br>');
                            });
                        }
                    });
                }
            })
        });

        $('#addPlantBtn').on('click', function (event) {
            $('#modalLabel').text('New city');
            $('#submitModal').text('Save').attr('data-mode', 'save');
        });

        $('#modal-lg').on('show.bs.modal', function (event) {
            const button = $(event.relatedTarget);
            const name = button.data('name');
            const full_desc = button.data('full_desc');
            const modal = $(this);
            modal.find('.modal-title').text(name);
            modal.find('.modal-body').html(full_desc);
        });
    });
</script>
@stop

