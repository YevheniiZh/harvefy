@extends('adminlte::page')

@section('title', 'Harvest options')

@section('content')
    <div class="row">
        <div class="col-12">
            <button type="button" class="btn btn-success float-right" id="addItemBtn" data-toggle="modal" data-target="#addItem" data-whatever="@mdo">Add harvest option</button>
        </div>
    </div>
<hr>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Harvest options list</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Description</th>
                            <th>Available in city</th>
                            <th>Meat type</th>
                            <th>Active</th>
                            <th>Manage</th>
                        </tr>
                        </thead>
                        <tbody id="ajaxPosts">
                        @foreach( $items as $index => $item)
                            <tr class="item-row">
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->name }}</td>
                                <td><img src="{{ $item->image }}" style="
    max-width: 50px;
    max-height: 50px;
"></td>
                                <td>{{ $item->description }}</td>
                                <td>
                                    @foreach($item->city_ids as $city_id){{ $loop->first ? '' : ', ' }}
                                    {{ $cities[$city_id]['name'] ?? '' }}@endforeach
                                </td>
                                <td>{{ $meat_types[$item->meat_type_id]['name'] }}</td>
                                <td>
                                    @if($item->active)
                                        <i class="fas fa-check" style="color:#82c91e;"></i>
                                    @else
                                        <i class="fas fa-minus" style="color:#b1b1b1;"></i>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" value="{{ $item->id }}" class="btn btn-default edit_post" title="Edit" data-toggle="modal" data-target="#addItem"><i class='fas fa-edit'></i></button>
                                        <button type="button" value="{{ $item->id }}" class="btn btn-danger delete_item" title="Remove"><i class='fas fa-trash-alt'></i></button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>

    <div class="modal fade" id="modal-lg" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addItem" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabel">New harvest option</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <form role="form" id="myForm" method="post" action="/admin/harvest_options"  enctype="multipart/form-data">
                <div class="modal-body">
                    <p id="form-errors" class="text-center error invalid-feedback" style="display: inherit;"></p>
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" class="form-control" id="description" name="description">
                            </div>
                            <div class="form-group">
                                <label>Available in city</label>
                                <select class="select2" id="city_ids" name="city_ids[]" multiple="multiple" data-placeholder="Select city" style="width: 100%;">
                                    @foreach( $cities as $index => $item)
                                        <option value="{{ $index }}">{{ $item['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Meat type</label>
                                <select class="select2" id="meat_type_id" name="meat_type_id" data-placeholder="Select meat type" style="width: 100%;">
                                    <option></option>
                                    @foreach( $meat_types as $index => $item)
                                        <option value="{{ $index }}">{{ $item['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <img id='img-upload' style="max-width: 38px; max-height: 38px"/>
                                    <input type="text" class="form-control" readonly>
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                            Choose image file<input id="imageFile" type="file" class="custom-file-input" name="file">
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-switch custom-switch-on-success">
                                    <input type="checkbox" class="custom-control-input" name="active" id="active">
                                    <label class="custom-control-label" for="active">Active</label>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submitModal" data-mode='save' class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>


<script src="../vendor/sweetalert2/sweetalert2.min.js"></script>
<link rel="stylesheet" href="../vendor/sweetalert2/sweetalert2.css">
<script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>
<!-- Select2 -->
<link rel="stylesheet" href="../vendor/select2/css/select2.min.css">
<script src="../vendor/select2/js/select2.full.js" defer></script>
<style>
    .select2-results__option[aria-selected=true] {
        display: none;
    }
    .select2-selection {
        height: 38px !important;
    }
</style>
<script>
    var items = {!! json_encode($itemsJs) !!};

    $(document).ready(function() {
        $('#city_ids').select2({placeholder: "Select city"});
        $('#meat_type_id').select2({minimumResultsForSearch: -1});

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        $('.edit_post').on('click', function(e){
            let id = $(this).val();
            $('#modalLabel').text('Edit harvest option (id: ' + id + ')');
            $('#submitModal').text('Update').attr('data-mode', 'edit').attr('data-id', id);
            populate('#myForm', items[id]);
        });

        function populate(frm, data) {
            $.each(data, function(key, value) {
                var ctrl = $('[id='+key+']', frm);
                switch(ctrl.prop("type")) {
                    case "radio": case "checkbox":
                        ctrl.each(function() {
                            $(this).attr("checked", !!value);
                        });
                        break;
                    default:
                        ctrl.val(value);
                }
                if (key === 'city_ids') {
                    ctrl.val(value).trigger('change');
                } else if (key === 'meat_type_id') {
                    ctrl.val(value).trigger('change');
                } else if (key === 'image') {
                    //$('.btn-file :file').trigger('fileselect', [value.split('/').pop()]);
                    $('#img-upload').attr('src', value);
                }
            });
        }

        $("form#myForm").on('submit', function(e){
            e.preventDefault();
            let data = new FormData(this);
            $('#form-errors').text('');
            let method, url;
            if ($('#submitModal').data('mode') === 'edit') {
                let id = $('#submitModal').data('id');
                data.append('fileLink', items[id]['image']);
                url = '/admin/harvest_options/id/' + id;
            } else {
                url = '/admin/harvest_options';
            }
            $.ajax({
                url: url,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
                success: function(data){
                    Toast.fire({
                        icon: 'success',
                        title: data.data
                    });
                    $('#addItem').modal('hide');
                    location.reload();
                },
                error: function (jqXHR, exception) {
                    Toast.fire({
                        icon: 'error',
                        title: jqXHR.responseJSON.message
                    });
                    $.each(jqXHR.responseJSON.errors, function (key, item) {
                        $('#form-errors').append(item.message + '<br>');
                    });
                    $("#addItem").scrollTop(0);
                },
            });
        });

        $('#addItem').on('hidden.bs.modal', function(){
            $(this).find('form')[0].reset();
            $('#img-upload').removeAttr('src');
            $('#form-errors').text('');
        });

        $('.delete_item').on('click', function(e){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    let item = $(this);
                    let id = item.val();
                    $.ajax({
                        data: {
                            "_token": "{{ csrf_token() }}"
                        },
                        url: '/admin/harvest_options/id/' + id,
                        type: 'DELETE',
                        success: function(result) {
                            Toast.fire({
                                icon: 'success',
                                title: result.data
                            });
                            item.parents('.item-row').remove();
                        },
                        error: function (jqXHR, exception) {
                            Toast.fire({
                                icon: 'error',
                                title: jqXHR.responseJSON.message
                            });
                            $.each(jqXHR.responseJSON.errors, function (key, item) {
                                $('#form-errors').append(item.message + '<br>');
                            });
                        }
                    });
                }
            })
        });

        $('#addItemBtn').on('click', function (event) {
            $('#modalLabel').text('New harvest option');
            $('#submitModal').text('Save').attr('data-mode', 'save');
        });

        $('#addItem').on('hidden.bs.modal', function(){
            $(this).find('form')[0].reset();
            $('#city_ids').val('').trigger('change');
            $('#meat_type_id').val('').trigger('change');
            $('#form-errors').text('');
        });

        // File input preview
        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }

        });
        function readURL(input) {
            $('#img-upload').removeAttr('src');
            if (input.files && input.files[0] && input.files[0].type.includes('image')) {
                console.log(input.files[0])
                var reader = new FileReader();
                reader.onload = function (e) {
                    if (e.target.result) {
                        $('#img-upload').attr('src', e.target.result);
                    }
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imageFile").change(function(){
            readURL(this);
        });

        $('#modal-lg').on('show.bs.modal', function (event) {
            const button = $(event.relatedTarget);
            const name = button.data('name');
            const full_desc = button.data('full_desc');
            const modal = $(this);
            modal.find('.modal-title').text(name);
            modal.find('.modal-body').html(full_desc);
        });
    });
</script>
@stop

