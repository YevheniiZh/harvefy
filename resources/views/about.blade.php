@extends('adminlte::page')

@section('title', 'About')

@section('content')
    <div class="row">
        <div class="col-12">
            <button type="button" class="btn btn-success float-right" id="addPlantBtn" data-toggle="modal" data-target="#addPlant" data-whatever="@mdo">Add about</button>
        </div>
    </div>
<hr>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">About info</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Type</th>
                            <th>Image</th>
                            <th>Description</th>
                            <th>Manage</th>
                        </tr>
                        </thead>
                        <tbody id="ajaxPosts">
                        @foreach( $items as $index => $item)
                            <tr class="item-row">
                                <td>{{ $item->id }}</td>
                                <td>{{ $types[$item->type_id] }}</td>
                                <td><img src="{{ $item->image }}" style="
    max-width: 50px;
    max-height: 50px;
"></td>
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#modal-lg" data-name="{{ $item->name }}" data-full_desc="{{ $item->description }}">View full description</a>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" value="{{ $item->id }}" class="btn btn-default edit_post" title="Edit" data-toggle="modal" data-target="#addPlant"><i class='fas fa-edit'></i></button>
                                        <button type="button" value="{{ $item->id }}" class="btn btn-danger delete_item" title="Remove"><i class='fas fa-trash-alt'></i></button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>

    <div class="modal fade" id="modal-lg" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addPlant" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabel">New plant</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <form role="form" id="myForm" method="post" action="/admin/about"  enctype="multipart/form-data">
                <div class="modal-body">
                    <p id="form-errors" class="text-center error invalid-feedback" style="display: inherit;"></p>
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Type</label>
                                <select class="select2" id="type_id" name="type_id" data-placeholder="Select type" style="width: 100%;">
                                    <option></option>
                                    @foreach( $types as $index => $item)
                                        <option value="{{ $index }}">{{ $item }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control textarea" rows="3" name="description"></textarea>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <img id='img-upload' style="max-width: 38px; max-height: 38px"/>
                                    <input type="text" class="form-control" readonly>
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                            Choose image file<input id="imageFile" type="file" class="custom-file-input" name="file">
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submitModal" data-mode='save' class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>

<!-- Summernote -->
<script src="../vendor/summernote/summernote.js" defer></script>
<link rel="stylesheet" href="../vendor/summernote/summernote.css" />

<script src="../vendor/sweetalert2/sweetalert2.min.js"></script>
<link rel="stylesheet" href="../vendor/sweetalert2/sweetalert2.css">
<script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>

<!-- Select2 -->
<link rel="stylesheet" href="../vendor/select2/css/select2.min.css">
<script src="../vendor/select2/js/select2.full.js" defer></script>
<style>
    .select2-results__option[aria-selected=true] {
        display: none;
    }
    .select2-selection {
        height: 38px !important;
    }
</style>
<script>
    var items = {!! json_encode($itemsJs) !!};

    $(document).ready(function() {
        $('#type_id').select2({minimumResultsForSearch: -1});

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        $('.textarea').summernote({
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link']],
                ['view', ['codeview']],
            ]
        });

        $('.edit_post').on('click', function(e){
            let id = $(this).val();
            $('#modalLabel').text('Edit about (id: ' + id + ')');
            $('#submitModal').text('Update').attr('data-mode', 'edit').attr('data-id', id);
            populate('#myForm', items[id]);
        });

        function populate(frm, data) {
            $.each(data, function(key, value) {
                var ctrl = $('[id='+key+']', frm);
                if (key === 'image') {
                    //$('.btn-file :file').trigger('fileselect', [value.split('/').pop()]);
                    $('#img-upload').attr('src', value);
                } else if (key === 'description') {
                    $(".textarea").summernote("code", value);
                }
                if (key === 'type_id') {
                    ctrl.val(value).trigger('change');
                }
            });
        }

        $("form#myForm").on('submit', function(e){
            e.preventDefault();
            let data = new FormData(this);
            $('#form-errors').text('');
            let method, url;
            if ($('#submitModal').data('mode') === 'edit') {
                let id = $('#submitModal').data('id');
                data.append('fileLink', items[id]['image']);
                url = '/admin/about/id/' + id;
            } else {
                url = '/admin/about';
            }
            $.ajax({
                url: url,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
                success: function(data){
                    Toast.fire({
                        icon: 'success',
                        title: data.data
                    });
                    $('#addPlant').modal('hide');
                    location.reload();
                },
                error: function (jqXHR, exception) {
                    Toast.fire({
                        icon: 'error',
                        title: jqXHR.responseJSON.message
                    });
                    $.each(jqXHR.responseJSON.errors, function (key, item) {
                        $('#form-errors').append(item.message + '<br>');
                    });
                    $("#addPlant").scrollTop(0);
                },
            });
        });

        $('#addPlant').on('hidden.bs.modal', function(){
            $(this).find('form')[0].reset();
            $('.textarea').summernote('reset');
            $('#img-upload').removeAttr('src');
            $('#type_id').val('').trigger('change');
            $('#form-errors').text('');
        });

        // File input preview
        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }

        });
        function readURL(input) {
            $('#img-upload').removeAttr('src');
            if (input.files && input.files[0] && input.files[0].type.includes('image')) {
                console.log(input.files[0])
                var reader = new FileReader();
                reader.onload = function (e) {
                    if (e.target.result) {
                        $('#img-upload').attr('src', e.target.result);
                    }
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imageFile").change(function(){
            readURL(this);
        });

        $('.delete_item').on('click', function(e){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    let item = $(this);
                    let id = item.val();
                    $.ajax({
                        data: {
                            "_token": "{{ csrf_token() }}"
                        },
                        url: '/admin/about/id/' + id,
                        type: 'DELETE',
                        success: function(result) {
                            Toast.fire({
                                icon: 'success',
                                title: result.data
                            });
                            item.parents('.item-row').remove();
                        },
                        error: function (jqXHR, exception) {
                            Toast.fire({
                                icon: 'error',
                                title: jqXHR.responseJSON.message
                            });
                            $.each(jqXHR.responseJSON.errors, function (key, item) {
                                $('#form-errors').append(item.message + '<br>');
                            });
                        }
                    });
                }
            })
        });

        $('#addPlantBtn').on('click', function (event) {
            $('#modalLabel').text('New plant');
            $('#submitModal').text('Save').attr('data-mode', 'save');
        });

        $('#modal-lg').on('show.bs.modal', function (event) {
            const button = $(event.relatedTarget);
            const name = button.data('name');
            const full_desc = button.data('full_desc');
            const modal = $(this);
            modal.find('.modal-title').text(name);
            modal.find('.modal-body').html(full_desc);
        });
    });
</script>
@stop

