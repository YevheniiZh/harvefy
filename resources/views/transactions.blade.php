@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Payments</h1>
@stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Payments</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Date</th>
                            <th>Type</th>
                            <th>Status</th>
                            <th>Amount</th>
                            <th>Id</th>
                            {{--<th></th>--}}
                        </tr>
                        </thead>
                        <tbody id="ajaxPosts">
                        @foreach( $transactions as $index => $item)
                            <tr>
                                <td>{{ $index+1 }}</td>
                                <td>{{ $item['date'] }}</td>
                                <td>{{ $item['type'] }}</td>
                                <td>{{ $item['status'] }}</td>
                                <td>{{ $item['amount'] }}</td>
                                <td>{{ $item['id'] }}</td>
                                {{--<th><a href="{{ Request::url() }}/{{ $post->id }}">подробнее</a></th>--}}
{{--                                <td>--}}
{{--                                    <div class="btn-group">--}}
{{--                                        <button type="button" value="{{ $post->id }}" class="btn edit_post" title="Редактировать" disabled><i class='fa fa-edit'></i></button>--}}
{{--                                        <button type="button" value="{{ $post->id }}" class="btn btn-info change_category" title="Изменить категорию"><i class='fa fa-tag'></i></button>--}}
{{--                                        @if($post->status == 0)--}}
{{--                                            <button type="button" value="{{ $post->id }}" class="btn btn-success status_post" title="Опубликовать"><i class='fa fa-unlock-alt'></i></button>--}}
{{--                                        @else--}}
{{--                                            <button type="button" value="{{ $post->id }}" class="btn btn-warning status_post" title="Снять с публикации"><i class='fa fa-lock'></i></button>--}}
{{--                                        @endif--}}
{{--                                        <button type="button" value="{{ $post->id }}" class="btn btn-danger delete_post" title="Удалить"><i class='fa fa-close'></i></button>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->


        </div>
        <!-- /.col -->
    </div>


@stop

<script>
    $(function(){
        $('#myForm').on('submit', function(e){
            e.preventDefault();
            $.post('http://www.somewhere.com/path/to/post',
                $('#myForm').serialize(),
                function(data, status, xhr){
                    // do something here with response;
                });
        });
    });
</script>