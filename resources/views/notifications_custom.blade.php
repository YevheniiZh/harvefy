@extends('adminlte::page')

@section('title', 'Push notifications')

@section('content')

    <div class="row">
        <div class="card">
            <div class="card-header d-flex p-0">
                <ul class="nav nav-pills p-2">
                    <li class="nav-item"><a class="nav-link" href="{{ $links['notifications'] }}">Notifications</a></li>
                    <li class="nav-item"><a class="nav-link active" href="{{ $links['notificationsCustom'] }}">Custom
                            notifications</a></li>
                </ul>
            </div><!-- /.card-header -->
        </div><!-- /.card-header -->

        <div class="col-12">
            <button type="button" class="btn btn-default float-left" id="downloadPlantsBtn" data-toggle="modal" data-target="#customNotifications" data-whatever="@mdo">Send custom notifications</button>
        </div>
    </div>
    <hr>

    <div class="modal fade" id="customNotifications" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Send custom notifications</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="notification">
                    <p id="form-errors" class="text-center error invalid-feedback" style="display: inherit;"></p>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="optionsRadios" id="allUsers" value="1" onclick="usersCheckbox()">
                                    Send to all users
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="optionsRadios" id="oneUser" value="2" onclick="usersCheckbox()">
                                    Send to one
                                </label>
                            </div>
                            <input type="text" class="form-control" id="email" placeholder="Email" style="display: none">
                        </div>
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" id="notificationTitle" maxlength="125">
                        </div>
                        <div class="form-group">
                            <label>Text</label>
                            <textarea class="form-control" id="notificationText" rows="3" maxlength="125"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success" id="sendNotificationBtn" onclick="sendNotification()" disabled>Send</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Custom push notifications</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>To all users</th>
                            <th>To email</th>
                            <th>Title</th>
                            <th>Text</th>
                            <th>Status</th>
                            <th>Response</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                        </tr>
                        </thead>
                        <tbody id="ajaxPosts">
                        @foreach( $items as $index => $item)
                            <tr>
                                <td>{{ $item['id'] }}</td>
                                <td>
                                    @if($item['to_all_users'])
                                        <i class="fas fa-check" style="color:#82c91e;"></i>
                                    @else
                                        <i class="fas fa-minus" style="color:#b1b1b1;"></i>
                                    @endif</td>
                                <td>{{ $item['email'] }}</td>
                                <td>{{ $item['title'] }}</td>
                                <td>{{ $item['body'] }}</td>
                                <td>
                                    @if($item['status'])
                                        <i class="fas fa-check" style="color:#82c91e;"></i>
                                    @else
                                        <i class="fas fa-minus" style="color:#b1b1b1;"></i>
                                    @endif
                                </td>
                                <td>
                                    @if($item['response'])
                                        <p title="{{ $item['response'] }}">response</p>
                                    @endif
                                </td>
                                <td>{{ $item['created_at'] }}</td>
                                <td>{{ $item['updated_at'] }}</td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>


@stop

<script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/sweetalert2/sweetalert2.min.js"></script>
<link rel="stylesheet" href="../vendor/sweetalert2/sweetalert2.css">

<script>
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
$(document).ready(function() {

    $('#notificationTitle').bind('input propertychange', function() {
        checkCanSend();
    });
    $('#notificationText').bind('input propertychange', function() {
        checkCanSend();
    });
    $('#email').bind('input propertychange', function() {
        checkCanSend();
    });
});

function checkCanSend() {
    let title = $('#notificationTitle').val();
    let text = $('#notificationText').val();
    let toAllUsers = $('#allUsers').is(":checked");
    let toOneUser = $('#oneUser').is(":checked");
    let email = $('#email').val();

    if (title !== '' && text !== '' && (toAllUsers || toOneUser && email !== '')) {
        $('#sendNotificationBtn').removeAttr('disabled')
    } else {
        $('#sendNotificationBtn').attr('disabled', 'disabled')
    }
}

function sendNotification() {
    let toAllUsers = $('#allUsers').is(":checked");
    let email = $('#email').val();
    let notificationTitle = $('#notificationTitle').val();
    let notificationText = $('#notificationText').val();

    $('#form-errors').text('');

    Swal.fire({
        title: 'Are you sure?',
        text: 'Send to: ' + (toAllUsers ? 'all users' : email) + ', Title: ' + notificationTitle + ', Text: ' + notificationText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, send!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                data: {
                    "_token": "{{ csrf_token() }}",
                    'toAllUsers': toAllUsers ? 1 : 0,
                    'email': email,
                    'notificationTitle': notificationTitle,
                    'notificationText': notificationText,
                },
                url: '/admin/notifications/send',
                type: 'POST',
                success: function (result) {
                    Toast.fire({
                        icon: 'success',
                        title: result.data
                    });
                    $('#customNotifications').modal('hide');
                    setTimeout(location.reload.bind(location), 500);
                },
                error: function (jqXHR, exception) {
                    Toast.fire({
                        icon: 'error',
                        title: jqXHR.responseJSON.message
                    });
                    $.each(jqXHR.responseJSON.errors, function (key, item) {
                        $('#form-errors').append(item.message + '<br>');
                    });
                },
            });
        }
    })
}

function usersCheckbox() {
    if (document.getElementById('oneUser').checked) {
        document.getElementById('email').style.display = 'block';
    }
    else document.getElementById('email').style.display = 'none';
    checkCanSend();
}
</script>