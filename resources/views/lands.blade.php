@extends('adminlte::page')

@section('title', 'Lands')

@section('content_header')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('/vendor/datatables/css/dataTables.bootstrap4.min.css') }}">
@stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Lands</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>User id</th>
                            <th>Type</th>
                            <th>Name</th>
                            <th>Products</th>
                            <th>Paid</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                            @if($isAdmin)
                                <th>Manager</th>
                            @endif
                            {{--<th></th>--}}
                        </tr>
                        </thead>
                        <tbody id="ajaxPosts">
                        @foreach( $lands as $index => $item)
                            <tr>
                                <td>{{ $item['id'] }}</td>
                                <td>{{ $item['user_id'] }}</td>
                                <td title="{{ $types[$item['type']] }}">{{ $item['type'] }}</td>
                                <td>{{ $item['name'] }}</td>
                                <td>
                                    @foreach($item['plants'] as $key => $plant){{ $loop->first ? '' : ', ' }}
                                        {{ $plant['plant_type']['name'] }}@endforeach
                                    @foreach($item['meat'] as $key => $plant){{ $loop->first ? '' : ', ' }}
                                        {{ $plant['meat_type']['name'] }}@endforeach
                                </td>
                                <td>
                                    @if($item['is_payed'])
                                        <i class="fas fa-check" style="color:#82c91e;"></i>
                                    @else
                                        <i class="fas fa-minus" style="color:#b1b1b1;"></i>
                                    @endif
                                </td>
                                <td>{{ $item['created_at'] }}</td>
                                <td>{{ $item['updated_at'] }}</td>
                                @if($isAdmin)
                                    <td data-id="{{ $item['user']['partner_id'] }}">
                                        @if($partners[$item['user']['partner_id']]??false)
                                            {{ $partners[$item['user']['partner_id']]['name'] }} {{ $partners[$item['user']['partner_id']]['surname'] }}
                                        @else
                                            Наши
                                        @endif
                                    </td>
                                @endif
                                {{--<th><a href="{{ Request::url() }}/{{ $post->id }}">подробнее</a></th>--}}
{{--                                <td>--}}
{{--                                    <div class="btn-group">--}}
{{--                                        <button type="button" value="{{ $post->id }}" class="btn edit_post" title="Редактировать" disabled><i class='fa fa-edit'></i></button>--}}
{{--                                        <button type="button" value="{{ $post->id }}" class="btn btn-info change_category" title="Изменить категорию"><i class='fa fa-tag'></i></button>--}}
{{--                                        @if($post->status == 0)--}}
{{--                                            <button type="button" value="{{ $post->id }}" class="btn btn-success status_post" title="Опубликовать"><i class='fa fa-unlock-alt'></i></button>--}}
{{--                                        @else--}}
{{--                                            <button type="button" value="{{ $post->id }}" class="btn btn-warning status_post" title="Снять с публикации"><i class='fa fa-lock'></i></button>--}}
{{--                                        @endif--}}
{{--                                        <button type="button" value="{{ $post->id }}" class="btn btn-danger delete_post" title="Удалить"><i class='fa fa-close'></i></button>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->


        </div>
        <!-- /.col -->
    </div>


@stop

<script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>
<!-- DataTables -->
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="../vendor/datatables/js/dataTables.bootstrap4.min.js"></script>

<script>
    var partners = {!! json_encode($partners) !!};
    var isAdmin = {{$isAdmin}};

    $(document).ready(function() {
        $("#example2").DataTable({
            order: [0, 'desc'],
            paging: false,
            initComplete: function () {
                if (!isAdmin) {
                    return;
                }
                this.api().columns([8]).every( function () {
                    var column = this;
                    var select = $('<select><option value="">Все</option></select>')
                        .appendTo( $(column.header()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
                            column.search( val ? '^'+val+'$' : '', true, false ).draw();
                        } );

                    select.append( '<option value="Наши">Наши</option>' )
                    $.each(partners, function ( i, item ) {
                        let name = item.name + (item.surname ? ' '+ item.surname : '');
                        select.append( '<option value="'+name+'">'+name + '</option>' )
                    } );
                } );
            }
        });
    });
</script>