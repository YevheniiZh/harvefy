@extends('adminlte::page')

@section('title', 'Users')

@section('content_header')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('/vendor/datatables/css/dataTables.bootstrap4.min.css') }}">
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Users</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>User id</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>City</th>
                            <th>Address</th>
                            <th>Promo code</th>
                            <th>Registration date</th>
                            @if($isAdmin)
                            <th>Manager</th>
                            @endif
                            {{--<th></th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $users as $index => $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone }}</td>
                                <td>{{ $user->cityModel->name }}</td>
                                <td>{{ $user->address }}</td>
                                <td>{{ $user->promocode }}</td>
                                <td>{{ $user->created_at }}</td>
                                @if($isAdmin)
                                <td data-id="{{ $user->partner_id }}">
                                    @if($partners[$user->partner_id]??false)
                                    {{ $partners[$user->partner_id]['name'] }} {{ $partners[$user->partner_id]['surname'] }}
                                    @else
Наши
                                    @endif
                                </td>
                                @endif
                            </tr>                                {{--<th><a href="{{ Request::url() }}/{{ $user->id }}">подробнее</a></th>--}}

                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

@stop
<!-- DataTables -->
<script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="../vendor/datatables/js/dataTables.bootstrap4.min.js"></script>

<script>
    var partners = {!! json_encode($partners) !!};
    var isAdmin = {{$isAdmin}};

    $(document).ready(function() {
        $("#example1").DataTable({
            initComplete: function () {
                if (!isAdmin) {
                    return;
                }
                this.api().columns([7]).every( function () {
                    var column = this;
                    var select = $('<select><option value="">Все</option></select>')
                        .appendTo( $(column.header()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
                            column.search( val ? '^'+val+'$' : '', true, false ).draw();
                        } );

                    select.append( '<option value="Наши">Наши</option>' )
                    $.each(partners, function ( i, item ) {
                        let name = item.name + (item.surname ? ' '+ item.surname : '');
                        select.append( '<option value="'+name+'">'+name + '</option>' )
                    } );
                } );
            }
        });
    });
</script>