@extends('adminlte::page')

@section('title', 'Meat types')

@section('content')
    <div class="row">
        <div class="col-12">
            <button type="button" class="btn btn-success float-right" id="addMeatBtn" data-toggle="modal" data-target="#addMeat" data-whatever="@mdo">Add meat</button>
        </div>
    </div>
<hr>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Meat types list</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Video</th>
                            <th>Short description</th>
                            <th>Full description</th>
                            <th>About culture</th>
                            <th>Days to grow</th>
                            <th>Harvests per month</th>
                            <th>Out of stock</th>
                            <th>Teaser</th>
                            <th>Manage</th>
                        </tr>
                        </thead>
                        <tbody id="ajaxPosts">
                        @foreach( $items as $index => $item)
                            <tr class="item-row">
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->name }}</td>
                                <td><img src="{{ $item->image }}" style="
    max-width: 50px;
    max-height: 50px;
"></td>
                                <td>
                                    @if($item->video)
                                        <a href="{{ $item->video}}" target="_blank">View video</a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>{{ $item->short_desc }}</td>
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#modal-lg" data-name="{{ $item->name }}" data-full_desc="{{ $item->full_desc }}">View full description</a>
                                </td>
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#modal-lg" data-name="{{ $item->name }}" data-full_desc="{{ $item->about_culture }}">View about culture</a>
                                </td>
                                <td>{{ $item->days_to_grow }}</td>
                                <td>{{ $item->harvests_per_month }}</td>
                                <td>
                                    @if($item->out_of_stock)
                                        <i class="fas fa-check" style="color:#82c91e;"></i>
                                    @else
                                        <i class="fas fa-minus" style="color:#b1b1b1;"></i>
                                    @endif
                                </td>
                                <td>
                                    @if($item->teaser)
                                        <i class="fas fa-check" style="color:#82c91e;"></i>
                                    @else
                                        <i class="fas fa-minus" style="color:#b1b1b1;"></i>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" value="{{ $item->id }}" class="btn btn-default edit_post" title="Edit" data-toggle="modal" data-target="#addMeat"><i class='fas fa-edit'></i></button>
                                        <button type="button" value="{{ $item->id }}" class="btn btn-danger delete_item" title="Remove"><i class='fas fa-trash-alt'></i></button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>

    <div class="modal fade" id="modal-lg" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addMeat" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabel">New meat</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <form role="form" id="myForm" method="post" action="/admin/meat_types"  enctype="multipart/form-data">
                <div class="modal-body">
                    <p id="form-errors" class="text-center error invalid-feedback" style="display: inherit;"></p>
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" name="name">
                            </div>
                            <div class="form-group">
                                <label>Short description</label>
                                <input type="text" class="form-control" name="short_desc">
                            </div>
                            <div class="form-group">
                                <label>Full description</label>
                                <textarea class="form-control" rows="3" name="full_desc"></textarea>
                            </div>
                            <div class="form-group">
                                <label>About culture</label>
                                <textarea class="form-control" rows="3" name="about_culture"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Days to grow</label>
                                <input type="number" class="form-control" name="days_to_grow">
                            </div>
                            <div class="form-group">
                                <label>(!) Config price parameter</label>
                                <input type="text" class="form-control" name=config_price_param>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <img id='img-upload' style="max-width: 38px; max-height: 38px"/>
                                    <input type="text" class="form-control" readonly>
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                            Choose image file<input id="imageFile" type="file" class="custom-file-input" name="file">
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" readonly>
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                            Choose video file<input id="videoFile" type="file" class="custom-file-input" name="videoFile">
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-switch custom-switch-on-success">
                                    <input type="checkbox" class="custom-control-input" name="out_of_stock" id="out_of_stock">
                                    <label class="custom-control-label" for="out_of_stock">Out of stock</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-switch custom-switch-on-success">
                                    <input type="checkbox" class="custom-control-input" name="teaser" id="teaser">
                                    <label class="custom-control-label" for="teaser">Teaser</label>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submitModal" data-mode='save' class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>

<script src="../vendor/sweetalert2/sweetalert2.min.js"></script>
<link rel="stylesheet" href="../vendor/sweetalert2/sweetalert2.css">
<script type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>

<script>
    var items = {!! json_encode($itemsJs) !!};

    $(document).ready(function() {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        $('.edit_post').on('click', function(e){
            let id = $(this).val();
            $('#modalLabel').text('Edit meat type (id: ' + id + ')');
            $('#submitModal').text('Update').attr('data-mode', 'edit').attr('data-id', id);
            populate('#myForm', items[id]);
        });

        function populate(frm, data) {
            $.each(data, function(key, value) {
                var ctrl = $('[name='+key+']', frm);
                switch(ctrl.prop("type")) {
                    case "radio": case "checkbox":
                        ctrl.each(function() {
                            if($(this).attr('value') == value) $(this).attr("checked",value);
                        });
                        ctrl.attr("checked",value);
                        break;
                    default:
                        ctrl.val(value);
                }
                if (key === 'meat_type_ids') {
                    ctrl.val(value).trigger('change');
                } else if (key === 'image') {
                    //$('.btn-file :file').trigger('fileselect', [value.split('/').pop()]);
                    $('#img-upload').attr('src', value);
                }
            });
        }

        $("form#myForm").on('submit', function(e){
            e.preventDefault();
            let data = new FormData(this);
            $('#form-errors').text('');
            let method, url;
            if ($('#submitModal').data('mode') === 'edit') {
                let id = $('#submitModal').data('id');
                data.append('fileLink', items[id]['image']);
                url = '/admin/meat_types/id/' + id;
            } else {
                url = '/admin/meat_types';
            }
            $.ajax({
                url: url,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
                success: function(data){
                    Toast.fire({
                        icon: 'success',
                        title: data.data
                    });
                    $('#addMeat').modal('hide');
                    location.reload();
                },
                error: function (jqXHR, exception) {
                    Toast.fire({
                        icon: 'error',
                        title: jqXHR.responseJSON.message
                    });
                    $.each(jqXHR.responseJSON.errors, function (key, item) {
                        $('#form-errors').append(item.message + '<br>');
                    });
                    $("#addMeat").scrollTop(0);
                },
            });
        });

        $('#addMeat').on('hidden.bs.modal', function(){
            $(this).find('form')[0].reset();
            $('#img-upload').removeAttr('src');
            $('#form-errors').text('');
        });

        // File input preview
        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }

        });
        function readURL(input) {
            $('#img-upload').removeAttr('src');
            if (input.files && input.files[0] && input.files[0].type.includes('image')) {
                console.log(input.files[0])
                var reader = new FileReader();
                reader.onload = function (e) {
                    if (e.target.result) {
                        $('#img-upload').attr('src', e.target.result);
                    }
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imageFile").change(function(){
            readURL(this);
        });

        $('.delete_item').on('click', function(e){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    let item = $(this);
                    let id = item.val();
                    $.ajax({
                        data: {
                            "_token": "{{ csrf_token() }}"
                        },
                        url: '/admin/meat_types/id/' + id,
                        type: 'DELETE',
                        success: function(result) {
                            Toast.fire({
                                icon: 'success',
                                title: result.data
                            });
                            item.parents('.item-row').remove();
                        },
                        error: function (jqXHR, exception) {
                            Toast.fire({
                                icon: 'error',
                                title: jqXHR.responseJSON.message
                            });
                            $.each(jqXHR.responseJSON.errors, function (key, item) {
                                $('#form-errors').append(item.message + '<br>');
                            });
                        }
                    });
                }
            })
        });

        $('#addMeatBtn').on('click', function (event) {
            $('#modalLabel').text('New meat');
            $('#submitModal').text('Save').attr('data-mode', 'save');
        });

        $('#modal-lg').on('show.bs.modal', function (event) {
            const button = $(event.relatedTarget);
            const name = button.data('name');
            const full_desc = button.data('full_desc');
            const modal = $(this);
            modal.find('.modal-title').text(name);
            modal.find('.modal-body').html(full_desc);
        });
    });
</script>
@stop

