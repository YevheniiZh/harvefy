$.AdminLTESidebarTweak = {};

$.AdminLTESidebarTweak.options = {
    EnableRemember: true,
    NoTransitionAfterReload: false
    //Removes the transition after page reload.
};

$(function () {
    "use strict";

    function setCookie(value) {
        console.log(value)
        let name = 'toggleState';
        let days = 365;
        let d = new Date;
        d.setTime(d.getTime() + 24 * 60 * 60 * 1000 * days);
        document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
    }

    $('[data-widget="pushmenu"]').on("click", function () {
        if ($.AdminLTESidebarTweak.options.EnableRemember) {
            if ($('body').hasClass('sidebar-collapse')) {
                setCookie('opened');
            } else {
                setCookie('closed');
            }
        }
    });
});