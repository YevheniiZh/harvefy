<?php
//ini_set('display_errors' , 0);
//
//function checkPageAuthorization($isProduction) {
//
//    $env = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/../.env');
//    $authorized = false;
//    if($isProduction) {
//        $valid_passwords = ['admin' => 'harvy_pass'/*$INI['adminPanelKycPass']*/];
//        $valid_users = array_keys($valid_passwords);
//
//        if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
//            $user = $_SERVER['PHP_AUTH_USER'];
//            $pass = $_SERVER['PHP_AUTH_PW'];
//
//            $authorized = (in_array($_SERVER['PHP_AUTH_USER'], $valid_users)) && ($pass == $valid_passwords[$user]);
//        }
//
//        if (!$authorized) {
//            header('WWW-Authenticate: Basic realm="My Realm"');
//            header('HTTP/1.0 401 Unauthorized');
//            $authorized = false;
//            echo '<script>window.location.reload();</script>';
//        }
//    }
//    return $authorized;
//}
//if(!checkPageAuthorization(1)) {
//    exit;
//}
//?>
<!-- HTML for static distribution bundle build -->
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>API</title>
    <link rel="stylesheet" type="text/css" href="/swagger/swagger-ui.css" >
    <link rel="icon" type="image/png" href="/swagger/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="/swagger/favicon-16x16.png" sizes="16x16" />
    <style>
      html
      {
        box-sizing: border-box;
        overflow: -moz-scrollbars-vertical;
        overflow-y: scroll;
      }

      *,
      *:before,
      *:after
      {
        box-sizing: inherit;
      }

      body
      {
        margin:0;
        background: #cacaca;
      }
    </style>
  </head>

  <body>
    <div id="swagger-ui"></div>

    <script src="/swagger/swagger-ui-bundle.js"> </script>
    <script src="/swagger/swagger-ui-standalone-preset.js"> </script>
    <script>
        window.onload = function() {
            /*if (location.host === 'harvefy.com' ) {
                window.location = '/';
            }*/
            // Begin Swagger UI call region
            const ui = SwaggerUIBundle({
                url: "/v2/swagger.json",
                enableCORS: false,
                dom_id: '#swagger-ui',
                docExpansion: 'none',
                deepLinking: true,
                presets: [
                    SwaggerUIBundle.presets.apis,
                    SwaggerUIStandalonePreset
                ],
                plugins: [
                    SwaggerUIBundle.plugins.DownloadUrl
                ],
                layout: "StandaloneLayout"
            })
            // End Swagger UI call region

            window.ui = ui
        }
  </script>
  </body>
</html>
