<?php

namespace App\Services;

use App\Models\SubscriptionStatusLog;

/**
 * Class SubscriptionStatusLogService
 * @package App\Services
 */
class SubscriptionStatusLogService extends Service
{
    private $model;

    public function __construct(SubscriptionStatusLog $delivery)
    {
        $this->model = $delivery;
    }

    /**
     * @param $params
     * @return SubscriptionStatusLog
     */
    public function create($params): SubscriptionStatusLog
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $id
     * @return int
     */
    public static function getCountAttempts(int $id): int
    {
        return SubscriptionStatusLog::where('subscription_id', $id)->count();
    }
}
