<?php

namespace App\Services;

use App\Models\City;
use App\Models\Land;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

/**
 * Class LandService
 * @package App\Services
 */
class LandService extends Service
{
    private $model;

    public function __construct(Land $land)
    {
        $this->model = $land;
    }

    /**
     * @param $params
     * @return Land
     */
    public function create($params): Land
    {
        $this->model->fill($params);
        $this->model->meat = Arr::get($params, 'meat', []);
        $this->model->plants = Arr::get($params, 'plants', []);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $id
     * @param array $params
     * @return Land
     */
    public function update(int $id, array $params): Land
    {
        $model = $this->model->where('id', $id)->first();
        $model->fill($params);
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param int $landId
     * @param array $plant
     * @return Land
     */
    public function addPlant(int $landId, array $plant): Land
    {
        $model = Land::where('id', $landId)->first();
        $plants = collect($model->plants);
        $plants = $plants->filter(function($item) use ($plant) {
            return $item['position'] != $plant['position'];
        })->values()->toArray();
        $plants[] = $plant;
        $model->plants = $plants;
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param int $landId
     * @param array $newMeat
     * @return Land
     */
    public function addMeat(int $landId, array $newMeat): Land
    {
        $model = Land::where('id', $landId)->first();
        $meat = $model->meat;
        $meat[] = $newMeat;
        $model->meat = $meat;
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param int $id
     * @param bool $isPayed
     * @return Land|null
     */
    public static function setIsPayed(int $id, bool $isPayed = true):? Land
    {
        $model = Land::where('id', $id)->first();
        if(!$model) {
            return null;
        }
        $model->is_payed = $isPayed;
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param int $userId
     * @return Collection|null
     */
    public static function get(int $userId): ?Collection
    {
        return Land::where(['user_id' => $userId, 'is_payed' => 1])->with('subscription')->get();
    }

    /**
     * @param int $id
     * @param int $userId
     * @return Land
     */
    public static function getById(int $id, int $userId): ?Land
    {
        return Land::where('id', $id)->where('user_id', $userId)->with('subscription')->first();
    }

    /**
     * @return array
     */
    public static function getAvailableLandTypes(): array
    {
        $result = [];
        $userCityId = Auth::user()->city_id;
        $city = CityService::getById($userCityId)->toArray();
        $plantsAvailable = Arr::get($city, City::PLANTS_AVAILABLE_FIELD);
        $meatAvailable = Arr::get($city, City::MEAT_AVAILABLE_FIELD);

        foreach (Land::LAND_TYPES as $key => $status) {
            if (($key === Land::TYPE_MEAT && !$meatAvailable) ||
                ($key !== Land::TYPE_MEAT && !$plantsAvailable)) {
                continue;
            }
            $result[$key] = $status;
        }
        return $result;
    }

    /**
     * @param array $result
     * @return array|null
     * @throws \Exception
     */
    public static function mapProduct(array $result):? array
    {
        if ($result['type'] === Land::TYPE_MEAT) {
            return self::mapMeat($result);
        }

        return self::mapPlants($result);
    }

    /**
     * @param array $result
     * @return array|null
     * @throws \Exception
     */
    public static function mapMeat(array $result):? array
    {
        $meatTypeIds = array_column($result['meat'], 'meat_type_id');
        $meatIds = array_column($result['meat'], 'id');

        $meatTypes = MeatTypeService::getByIds($meatTypeIds);
        $meat = MeatService::getByIds($meatIds);

        foreach ($result['meat'] as $key => $item) {
            $item['meat_type'] = $meatTypes[$item['meat_type_id']];
            if (Arr::get($item, 'id')) {
                $item['meat_item'] = Arr::get($meat, $item['id']);
                $grow = MeatService::getHoursInterval(Arr::get($item['meat_item'], 'created_at', 0));
                $harvest_in = $item['meat_type']['days_to_grow'] - $grow['days'];
                $item['meat_item']['harvest_in'] = max($harvest_in, 0);
                unset($item['autowatering']);
            }

            $result['meat'][$key] = $item;
        }
        return $result;
    }

    /**
     * @param array $result
     * @return array|null
     * @throws \Exception
     */
    public static function mapPlants(array $result):? array
    {
        $plantTypeIds = array_column($result['plants'], 'plant_type_id');
        $plantIds = array_column($result['plants'], 'id');

        $plantTypes = PlantTypeService::getByIds($plantTypeIds);
        $plants = PlantService::getByIds($plantIds);

        foreach ($result['plants'] as $key => $plant) {
            $plant['plant_type'] = $plantTypes[$plant['plant_type_id']];
            if (Arr::get($plant, 'id')) {
                $plant['plant_item'] = $plants[$plant['id']];
                $grow = PlantService::getHoursInterval(Arr::get($plant['plant_item'], 'created_at', 0));
                $harvest_in = $plant['plant_type']['days_to_grow'] - $grow['days'];
                $plant['plant_item']['harvest_in'] = max($harvest_in, 0);
                unset($plant['autowatering']);
            }

            $result['plants'][$key] = $plant;
        }
        return $result;
    }

    /**
     * @param int $id
     */
    public static function delete(int $id): void
    {
        Land::where('id', $id)->delete();
    }
}
