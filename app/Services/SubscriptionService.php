<?php

namespace App\Services;

use App\Models\Land;
use App\Models\Subscription;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

/**
 * Class SubscriptionService
 * @package App\Services
 */
class SubscriptionService extends Service
{
    private $model;

    public function __construct(Subscription $subscription)
    {
        $this->model = $subscription;
    }

    /**
     * @param $params
     * @return Subscription
     */
    public function create($params): Subscription
    {
        $this->model->fill($params);
        $this->model->next_payment = Carbon::now();
        $this->model->save();
        $this->model->refresh();

        app(SubscriptionStatusLogService::class)->create([
            'subscription_id' => $this->model->id,
            'old_status' => 0,
            'new_status' => Subscription::STATUS_ACTIVE,
            'action' => 'subscribe'
        ]);

        return $this->model;
    }

    /**
     * @param int $userId
     * @return Collection|null
     */
    public static function get(int $userId): ?Collection
    {
        return Subscription::where(['user_id' => $userId])->with('land', 'payment')->get();
    }

    /**
     * @param int $id
     * @param int $userId
     * @return Subscription
     */
    public static function getById(int $id, int $userId): ?Subscription
    {
        return Subscription::where('id', $id)->where('user_id', $userId)->with('land', 'payment')->first();
    }

    /**
     * @param int $landId
     * @return Subscription
     */
    public static function getByLandId(int $landId): ?Subscription
    {
        return Subscription::where('land_id', $landId)->first();
    }


    /**
     * @param int $landType
     * @return string
     */
    public function getPriceConfigByType(int $landType): string
    {
        return Arr::get(Subscription::TYPES_CONFIG, $landType, '');
    }

    /**
     * @param int $id
     * @return Subscription|null
     */
    public static function unsubscribe(int $id):? Subscription
    {
        $model = Subscription::where('id', $id)->with('payment')->first();
        $oldStatus = $model->status;
        $model->status = Subscription::STATUS_UNSUBSCRIBE_REQUESTED;
        $model->canceled = true;
        $model->cancel_date = Carbon::now();
        $model->save();
        $model->refresh();

        app(SubscriptionStatusLogService::class)->create([
            'subscription_id' => $id,
            'old_status' => $oldStatus,
            'new_status' => $model->status,
            'action' => 'unsubscribe'
        ]);
        return $model;
    }

    /**
     * @param int $id
     * @param array $psResponse
     * @return Subscription
     */
    public static function updatePsResponse(int $id, array $psResponse): Subscription
    {
        $model = Subscription::where('id', $id)->first();
        $model->ps_response = $psResponse;
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param int $id
     * @param int $status
     * @return Subscription
     */
    public static function updateStatus(int $id, int $status): Subscription
    {
        $model = Subscription::where('id', $id)->first();
        $model->status = $status;
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param int $payment_id
     * @return Subscription
     */
    public static function prolong(int $payment_id): Subscription
    {
        $model = Subscription::where('payment_id', $payment_id)->first();
        $model->next_payment = Carbon::createFromTimestamp($model->next_payment)->addMonth();
        $model->status = Subscription::STATUS_ACTIVE;
        $model->canceled = false;
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param int $payment_id
     * @return Subscription
     */
    public static function cancelDueCantCharge(int $payment_id): Subscription
    {
        $model = Subscription::where('payment_id', $payment_id)->first();
        $oldStatus = $model->status;
        $model->status = Subscription::STATUS_UNSUBSCRIBED_DUE_CANT_CHARGE;
        $model->canceled = true;
        $model->cancel_date = Carbon::now();
        $model->save();
        $model->refresh();

        app(SubscriptionStatusLogService::class)->create([
            'subscription_id' => $model->id,
            'old_status' => $oldStatus,
            'new_status' => $model->status,
            'action' => 'cancelDueCantCharge'
        ]);

        return $model;
    }


    /**
     * @return array
     * @throws \Exception
     */
    public static function checkStatusForUpdate(): array
    {
        $result = [];
        $subscriptions = Subscription::with('payment')->get();
        foreach ($subscriptions as $subscription) {
            if (array_key_exists($subscription['status'], Subscription::HANDLED_BY_SCHEDULE_STATUSES)) {
                if ($statusResult = self::handleByStatus($subscription['status'], $subscription)) {
                    $result[(int)$subscription['id']] = $statusResult;
                }
            }
        }
        return $result;
    }

    /**
     * @param int $status
     * @param $subscriptions
     * @return array|null
     * @throws \Exception
     */
    public static function handleByStatus(int $status, $subscriptions):? array
    {
        $newStatus = null;
        $cancel_date = $subscriptions['next_payment'] ?: $subscriptions['cancel_date'];
        switch ($status) {
            case Subscription::STATUS_ACTIVE:
                if (Carbon::createFromTimestamp($cancel_date) < Carbon::now()) {
                    if (!$subscriptions['payment']['transaction_id']) {
                        Log::error('Error handleByStatus subscription ' . $subscriptions['id']);
                        break;
                    }
                    $result = app(PaymentService::class)->getStatusLiqpay($subscriptions['payment']['transaction_id']);
                    if (Arr::get($result, 'status') === PaymentService::LIQPAY_STATUS_UNSUBSCRIBED) {
                        $newStatus = Subscription::STATUS_UNSUBSCRIBED_DUE_CANT_CHARGE;
                        LandService::setIsPayed($subscriptions['land_id'], false);
                    }
                }
                break;
            case Subscription::STATUS_UNSUBSCRIBE_REQUESTED:
                if (Carbon::createFromTimestamp($cancel_date) < Carbon::now()) {
                    $newStatus = Subscription::STATUS_UNSUBSCRIBED;
                    LandService::setIsPayed($subscriptions['land_id'], false);
                }
                break;
        }

        if ($newStatus) {
            self::updateStatus($subscriptions['id'], $newStatus);
            return ['old_status' => $status, 'new_status' => $newStatus];
        }
        return null;
    }

}
