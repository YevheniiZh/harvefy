<?php

namespace App\Services;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

/**
 * Class PartnerService
 * @package App\Services
 */
class PartnerService
{
    private $model;

    public function __construct(Admin $model)
    {
        $this->model = $model;
    }

    /**
     * @param $params
     * @return Admin
     */
    public static function create(array $params): Admin
    {
        $model = new Admin();
        $model->fill($params);
        $model->password = Hash::make($params['password']);
        $model->is_partner = true;
        $model->save();

        return $model;
    }

    /**
     * @param int $id
     * @return Admin|null
     */
    public static function get(int $id): ?Admin
    {
        return Admin::where('id', $id)->first();
    }

    /**
     * @param bool $allFields
     * @return array
     */
    public static function getAllPartners(bool $allFields = false): array
    {
        $fields = $allFields ? ['*'] : ['id', 'name'];
        return Admin::where(['is_partner' => true])->get($fields)->toArray();
    }

    /**
     * @param int $id
     * @return Admin|null
     */
    public static function getPartner(int $id): ?Admin
    {
        return Admin::where(['id' => $id, 'is_partner' => true])->first();
    }

    /**
     * @param array $ids
     * @return Admin|null
     */
    public static function getByIds(array $ids):? array
    {
        return Admin::whereIn('id', $ids)->get()->keyBy('id')->toArray();
    }

    /**
     * @param Admin $model
     * @param array $data
     * @return Collection|null
     */
    public static function update(Admin $model, array $data):? Admin
    {
        $model->fill($data);
        if ($password = Arr::get($data, 'password')) {
            $model->password = Hash::make($password);
        }
        $model->save();

        return $model;
    }

    /**
     * @param int $id
     */
    public static function delete(int $id): void
    {
        Admin::where('id', $id)->delete();
    }

}
