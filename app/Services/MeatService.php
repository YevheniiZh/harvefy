<?php

namespace App\Services;

use App\Casts\Timezone;
use App\Helpers\MailHelper;
use App\Mail\NewPlants;
use App\Models\Config;
use App\Models\Meat;
use App\Models\MeatType;
use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;

/**
 * Class MeatService
 * @package App\Services
 */
class MeatService extends ProductService
{
    private $model;
    public static $modelClass = Meat::class;
    public static $typeField = 'meat_type';
    public const TYPE_ID = 2;

    protected const NOTIFICATION_TITLES = [
        Meat::STATUS_HAS_GROWN => 'Мясо готове',
        Meat::STATUS_IN_BASKET => 'Оформіть доставку 🚚',
        Meat::STATUS_WATERING_REQUIRED => 'Нагодуй мене',
        Meat::STATUS_DIED => 'Бичок зголоднів і втік',
        Meat::STATUS_DIED_IN_BASKET => 'Бичок зголоднів і втік',
    ];

    protected const NOTIFICATION_TITLE_DIED_AFTER = 'Ваша продукція зіпсується через {hours} години!';

    public function __construct(Meat $landMeat)
    {
        $this->model = $landMeat;
    }

    /**
     * @param $params
     * @return Meat
     */
    public function create($params): Meat
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $id
     * @return Meat|null
     */
    public static function get(int $id): ?Meat
    {
        return Meat::where('id', $id)->with('meatType', 'delivery')->first();
    }

    /**
     * @param array $ids
     * @return array|null
     */
    public static function getByIds(array $ids):? array
    {
        return Meat::whereIn('id', $ids)->get()->keyBy('id')->toArray();
    }

    /**
     * @param int $id
     * @return array|null
     */
    public static function getByLandId(int $id):? array
    {
        return Meat::where('land_id', $id)->with('meatType')->first()->toArray();
    }

    /**
     * @param int $landId
     * @return Meat|null
     */
    public static function getActiveMeat(int $landId):? Meat
    {
        $statusIds = [Meat::STATUS_IS_GROWING, Meat::STATUS_WATERING_REQUIRED, Meat::STATUS_HAS_GROWN];
        return Meat::where('land_id', $landId)->whereIn('status', $statusIds)->first();
    }

    /**
     * @param int $landId
     * @return Meat|null
     */
    public static function getLandMeatTypeId(int $landId):? int
    {
        $meat = Meat::where('land_id', $landId)->first();
        return $meat ? $meat->meat_type_id : null;
    }

    /**
     * @return array
     */
    public static function getAllPayed(): array
    {
        return Meat::/*whereHas('land', function ($query) {
            return $query->where('is_payed', 1);
        })->*/with('meatType', 'subscription', 'land')->get()->toArray();
    }


    /**
     * @return array
     */
    public static function getAllNotGrow(): array
    {
        return Meat::whereIn('status', [Product::STATUS_IS_GROWING, Product::STATUS_WATERING_REQUIRED])
            ->with('meatType', 'subscription', 'land')->get()->toArray();
    }

    /**
     * @return array
     */
    public static function getAllGrownToday(): array
    {
        return Meat::where('status', Product::STATUS_HAS_GROWN)
            ->whereRaw('DATE(status_updated_at) = CURDATE()')
            ->with('meatType', 'subscription', 'land')->get()->toArray();
    }

    /**
     * @param int $userId
     * @param array $statusIds
     * @return Collection|null
     */
    public static function getByUserId(int $userId, array $statusIds = []): ?Collection
    {
        return Meat::where('user_id', $userId)->whereHas('land', function ($query) {
            return $query->where('is_payed', 1);
        })->whereIn('status', $statusIds)->with('meatType', 'delivery')->get();
    }

    /**
     * @return array
     * @throws \Throwable
     */
    public static function sendNewMeat(): array
    {
        if (\Carbon\Carbon::now(ConfigService::LOCAL_TIMEZONE)->hour < 16) {
            $period = '16:00 - 9:00';
            $whereQuery = "CONVERT_TZ(created_at,'+00:00','+02:00')
between date_format(DATE_SUB(CONVERT_TZ(now(),'+00:00','+02:00'), INTERVAL 1 DAY),'%Y-%m-%d 16:00:00') 
    and date_format(CONVERT_TZ(now(),'+00:00','+02:00'),'%Y-%m-%d 09:00:00')";
        } else {
            $period = '9:00 - 16:00';
            $whereQuery = "CONVERT_TZ(created_at,'+00:00','+02:00')
between date_format(CONVERT_TZ(now(),'+00:00','+02:00'),'%Y-%m-%d 09:00:00')
    and date_format(CONVERT_TZ(now(),'+00:00','+02:00'),'%Y-%m-%d 16:00:00')";
        }
        $lastItems = static::$modelClass::whereHas('land', function ($query) {
            return $query->where('is_payed', 1);
        })->whereRaw($whereQuery)
            ->selectRaw('meat_type_id, count(id) as count')
            ->groupBy('meat_type_id')
            ->withCasts(['created_at' => Timezone::class])
            ->get();
        if (!$lastItems->count()) {
            return ['No new meat'];
        }

        $meatTypes = Service::indexById(MeatType::all(['id', 'name'])->toArray());

        $message = view('new_plants_email', [
            'lastItems' => $lastItems,
            'product_types' => $meatTypes
        ])->render();
        $data = new \stdClass();
        $data->subject = "New meat {$period}";
        $data->message = $message;
        $result = [];
        $emails = Config::getAdditional(Config::NEW_PLANTS_TO_EMAIL);
        $emails[] = env('NEW_PLANTS_TO_EMAIL');
        foreach ($emails as $email) {
            $result[] = MailHelper::sendEmail($email, new NewPlants($data));
        }
        return $result;
    }

    /**
     * @param array $product
     * @return bool
     */
    public static function checkHasGrown(array $product): bool
    {
        $daysToGrow = (int) MeatTypeService::get($product['meat_type_id'])->days_to_grow;
        $grownOn = Carbon::createFromTimestamp($product['created_at'])->addDays($daysToGrow);
        return $grownOn <= Carbon::now();
    }

    /**
     * @param array $product
     * @return bool
     */
    public static function checkHasGrownToday(array $product): bool
    {
        $daysToGrow = (int) MeatTypeService::get($product['meat_type_id'])->days_to_grow;
        $grownOn = Carbon::createFromTimestamp($product['created_at'])->addDays($daysToGrow);
        return $grownOn <= Carbon::now()->endOfDay();
    }

    /**
     * @param Meat $item
     * @param int $growDays
     * @return int
     */
    public function harvestIn(Meat $item, int $growDays): int
    {
        return max($item->meatType->days_to_grow - $growDays, 0);
    }

    /**
     * @param Meat $item
     * @param int $growHours
     * @return int
     */
    public function grownOnPercent(Meat $item, int $growHours): int
    {
        $days_to_grow = $item->meatType->days_to_grow;
        $percent = round(100 / ($days_to_grow * 24) * $growHours);
        $percent = $percent > 100 ? 100 : (int) $percent;

        if ($item->status < Meat::STATUS_HAS_GROWN && $percent === 100) {
            $percent = 99;
        }
        return $percent;
    }

    /**
     * @param int $id
     * @param int $harvest_option_id
     * @return Meat
     */
    public static function harvestMeat(int $id, int $harvest_option_id): Meat
    {
        $status = Meat::STATUS_IN_BASKET;
        $model = Meat::where('id', $id)->first();
        $oldStatus = $model->status;
        if ($oldStatus === $status) {
            return $model;
        }
        $model->status = $status;
        $model->harvest_option_id = $harvest_option_id;
        $model->status_updated_at = Carbon::now();
        $model->save();
        $model->refresh();

        app(PlantStatusLogService::class)->create([
            'product_id' => $id,
            'product_type_id' => self::TYPE_ID,
            'old_status' => $oldStatus,
            'new_status' => $status,
            'action' => 'harvest'
        ]);

        return $model;
    }
}
