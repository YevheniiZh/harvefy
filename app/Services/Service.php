<?php

namespace App\Services;


/**
 * Class Service
 * @package App\Services
 */
class Service
{
    /**
     * @param array $list
     * @return array
     */
    public static function indexById(array $list): array
    {
        $keys = array_column($list, 'id');

        return array_combine($keys, $list);
    }

    /**
     * @param int $length
     * @return string
     */
    public static function generateRandomString(int $length = 10): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
