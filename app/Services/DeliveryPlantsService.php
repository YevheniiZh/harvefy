<?php

namespace App\Services;

use App\Models\DeliveryPlants;

/**
 * Class DeliveryPlantsService
 * @package App\Services
 */
class DeliveryPlantsService extends Service
{
    private $model;

    public function __construct(DeliveryPlants $delivery)
    {
        $this->model = $delivery;
    }

    /**
     * @param $params
     * @return DeliveryPlants
     */
    public function create($params): DeliveryPlants
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $id
     * @return
     */
    public static function delete(int $id)
    {
        return DeliveryPlants::where('delivery_id', $id)->delete();
    }
}
