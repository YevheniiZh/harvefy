<?php

namespace App\Services;

use App\Models\MeatType;

/**
 * Class MeatTypeService
 * @package App\Services
 */
class MeatTypeService
{
    private $model;

    public function __construct(MeatType $land)
    {
        $this->model = $land;
    }

    /**
     * @param $params
     * @return MeatType
     */
    public function create($params): MeatType
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $id
     * @return MeatType|null
     */
    public static function get(int $id): ?MeatType
    {
        return MeatType::where('id', $id)->first();
    }

    /**
     * @param array $ids
     * @return MeatType|null
     */
    public static function getByIds(array $ids):? array
    {
        return MeatType::whereIn('id', $ids)->get()->keyBy('id')->toArray();
    }

    /**
     * @param int $id
     * @param array $data
     * @return Collection|null
     */
    public static function update(int $id, array $data):? MeatType
    {
        $model = MeatType::where('id', $id)->first();
        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * @param int $id
     */
    public static function delete(int $id): void
    {
        MeatType::where('id', $id)->delete();
    }
}
