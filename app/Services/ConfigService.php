<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Psr\Http\Message\ResponseInterface;

/**
 * Class ConfigService used for getting configuration from config micro service.
 *
 * @package App\Services
 */
class ConfigService
{

    /**
     * @var int
     */
    public const SLEEP = 3;

    /**
     *
     * Name of config file
     *
     * @var string
     */
    public const ENV_FILE_NAME = '.env';

    /**
     * @var string
     */
    public const EXAMPLE_FILE_NAME = 'config.example.json';

    /**
     * @var string
     */
    public const CONFIG_SCHEMA = 'config.schema.json';

    /**
     * @var string
     */
    public const LOCAL_TIMEZONE = 'Europe/Kiev';

    /**
     * @var string
     */
    private $uri;

    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $schemaPath;


    /**
     * ConfigService constructor.
     *
     * @param string|null $uri
     */
    public function __construct(string $uri = null)
    {
        $this->uri = $uri ?: self::ENV_FILE_NAME;
        $this->path = app()->basePath(self::ENV_FILE_NAME);
        $this->schemaPath = app()->basePath(self::CONFIG_SCHEMA);
    }

    /**
     * @return void
     * @throws \RuntimeException
     */
    public function load(): void
    {

        /**
         * For testing purposes, change to TRUE and use EXAMPLE_FILE_NAME for modifying
         */
        $this->loadConfigExample(false);

        if (File::exists($this->path)) {
            return;
        }

        if (!$this->uri) {
            $this->configurationNotLoaded();
        }

        $client = new Client([
            'base_uri' => $this->uri,
        ]);

        try {

            /**
             * @var ResponseInterface $response
             */
            app('log')->info('Reading config-service by url ' . $this->uri);
            $response = $client->get('');
            $config = json_decode($response->getBody()->getContents(), true)['data'];
            $this->store($config);
            return;
        } catch (\Exception $e) {

        }

        $this->configurationNotLoaded();
    }

    /**
     * @throws \RuntimeException
     */
    private function configurationNotLoaded(): void
    {
        throw new \RuntimeException('Configuration not loaded');
    }

    /**
     * @param array $config
     * @return ConfigService
     */
    private function store(array $config): ConfigService
    {
        $this->validate($config);
        $configString = '';
        $configArr = Arr::dot($config);
        foreach ($configArr as $key => $value) {
            $envKey = Str::upper(str_replace('.', '_', $key));
            $envVal = is_string($value) ? '"' . $value . '"' : $value;
            $configString .= $envKey . '=' . $envVal . "\n";
        }
        File::put($this->path, $configString);
        return $this;
    }

    /**
     * @param array $config
     */
    private function validate(array $config): void
    {
        if (!File::exists($this->schemaPath)) {
            app('log')->error('Configuration schema not exists.');
            $this->sleep();
            exit(1);
        }
        $rules = json_decode(File::get($this->schemaPath), true);

        $validator = validator($config, $rules);

        if ($validator->fails()) {
            app('log')->error(implode("\n", [
                'Config validation throws errors:',
                $validator->errors()->toJson(JSON_PRETTY_PRINT),
            ]));
            app('log')->error(
                sprintf(
                    'An %s instance have got a wrong data from config-service. Will be stopped in %s seconds.',
                    config('converter.name'),
                    self::SLEEP
                )
            );
            $this->sleep();
            exit(1);
        }
    }

    /**
     * @param bool $load
     */
    private function loadConfigExample(bool $load): void
    {
        if ($load && app()->basePath(self::EXAMPLE_FILE_NAME)) {
            $this->store(json_decode(File::get(app()->basePath(self::EXAMPLE_FILE_NAME)), true));
            return;
        }
    }

    /**
     * @param int $seconds
     */
    public function sleep(int $seconds = self::SLEEP): void
    {
        sleep($seconds);
    }
}
