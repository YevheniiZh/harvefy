<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserService
 * @package App\Services
 */
class UserService
{
    private $model;
    private $requestData;

    public function __construct(User $user, Request $request)
    {
        $this->model = $user;
        $this->requestData = $request->all();
    }

    /**
     * @param $params
     * @return User
     */
    public function create($params): User
    {
        $this->model->fill($params);
        $this->model->password = Hash::make(Arr::get($params, 'password'));
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param string $email
     * @param string $newPassword
     * @return User
     */
    public static function updatePassword(string $email, string $newPassword): User
    {
        if ($model = User::where('email', $email)->first()) {
            $model->password = Hash::make($newPassword);
            $model->save();
            $model->refresh();
        }
        return $model;
    }

    public static function generateRandomString($length = 10): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @param int $id
     * @return User
     */
    public static function get(int $id): ?User
    {
        return User::where('id', $id)->first();
    }

    /**
     * @param int $partnerId
     * @return array
     */
    public static function getUserIdsByPartnerId(int $partnerId): array
    {
        return User::where('partner_id', $partnerId)->get()->pluck('id')->toArray();
    }

    /**
     * @param string $email
     * @return User
     */
    public static function getByEmail(string $email): ?User
    {
        return User::where('email', $email)->first();
    }

    /**
     * @param string $email
     * @return bool
     */
    public static function existsByEmail(string $email): bool
    {
        return User::where('email', $email)->exists();
    }

    /**
     * @param int $id
     */
    public static function delete(int $id): void
    {
        User::where('id', $id)->delete();
    }
}
