<?php

namespace App\Services;

use App\Models\Street;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class StreetService
 * @package App\Services
 */
class StreetService extends Service
{
    private $model;

    public function __construct(Street $model)
    {
        $this->model = $model;
    }

    /**
     * @param $params
     * @return Street
     */
    public function create($params): Street
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $id
     * @return Street
     */
    public static function getById(int $id): ?Street
    {
        return Street::where('id', $id)->first();
    }

    /**
     * @param string $street
     * @return Street
     */
    public static function getInSybstring(string $street): ?Street
    {
        return Street::whereRaw("'{$street}' LIKE CONCAT('%', name, '%')")
            ->with('admin')->first();
    }

    /**
     * @return Collection|null
     */
    public static function getAll(): ?Collection
    {
        return Street::all(['id', 'name']);
    }

    /**
     * @param string $street
     * @return Collection|null
     */
    public static function getAllBySybstring(string $street): ?Collection
    {
        return Street::whereRaw("name LIKE '%$street%'")
            //->with('admin')
            ->get(['id', 'name']);
    }

    /**
     * @return array|null
     */
    public static function getActive():? array
    {
        return Street::where('active', true)->get()->toArray();
    }

    /**
     * @param int $id
     * @param array $data
     * @return Collection|null
     */
    public static function update(int $id, array $data):? Street
    {
        $model = Street::where('id', $id)->first();
        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * @param int $id
     * @return void
     */
    public static function delete(int $id): void
    {
        Street::where('id', $id)->delete();
    }
}
