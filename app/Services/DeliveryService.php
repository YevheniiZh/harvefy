<?php

namespace App\Services;

use App\Http\Controllers\Admin\DeliveryController;
use App\Models\Config;
use App\Models\Delivery;
use App\Models\LandImage;
use App\Models\Meat;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

/**
 * Class DeliveryService
 * @package App\Services
 */
class DeliveryService extends Service
{
    private $model;

    private const DAYS = [
        1 => Carbon::MONDAY,
        2 => Carbon::TUESDAY,
        3 => Carbon::WEDNESDAY,
        4 => Carbon::THURSDAY,
        5 => Carbon::FRIDAY,
        6 => Carbon::SATURDAY,
        7 => Carbon::SUNDAY,
    ];

    public function __construct(Delivery $delivery)
    {
        $this->model = $delivery;
    }

    /**
     * @param $params
     * @return Delivery
     */
    public function create($params): Delivery
    {
        $this->model->fill($params);
        $this->model->plant_ids = Arr::get($params, 'plant_ids', []);
        $this->model->meat_ids = Arr::get($params, 'meat_ids', []);
        $this->model->scheduled_date = self::getScheduledDate(Arr::get($params, 'day'));
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param array $product
     * @param string $modelClass
     * @return Delivery
     */
    public static function addToActiveDelivery(array $product, string $modelClass):? Delivery
    {
        $productId = $product['id'];
        if (!$delivery = self::getNotDelivered($product['user_id'])) {
            return null;
        }
        if ($modelClass === Meat::class) {
            $delivery->meat_ids = array_merge($delivery->meat_ids, [$productId]);
            $productTypeId = LandImage::LAND_TYPE_MEAT;
        } else {
            $delivery->plant_ids = array_merge($delivery->plant_ids, [$productId]);
            $productTypeId = LandImage::LAND_TYPE_MICROGREEN;
        }
        $delivery->save();

        app(DeliveryPlantsService::class)->create([
            'delivery_id' => $delivery->id,
            'product_id' => $productId,
            'product_type_id' => $productTypeId,
        ]);
        return $delivery;
    }

    /**
     * @param int $id
     * @return Delivery|null
     */
    public static function setIsPayed(int $id):? Delivery
    {
        $model = Delivery::where('id', $id)->first();
        if(!$model) {
            return null;
        }
        $model->is_payed = true;
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param int $id
     * @return Delivery|null
     */
    public static function setDelivered(int $id):? Delivery
    {
        $model = Delivery::where('id', $id)->with('delivery_plants')->first();
        if(!$model) {
            return null;
        }
        $model->delivered = true;
        $model->delivery_date = Carbon::now();
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param array $plantIds
     * @return Collection|null
     */
    public function checkForPayedPlants(array $plantIds): ?Delivery
    {
        return Delivery::where('is_payed', 1)
            ->whereHas('delivery_plants', static function ($query) use ($plantIds) {
            return $query->whereIn('product_id', $plantIds)->where('product_type_id', LandImage::LAND_TYPE_MICROGREEN);
        })->first();
    }

    /**
     * @param array $plantIds
     * @return Collection|null
     */
    public function checkForPayedMeat(array $plantIds): ?Delivery
    {
        return Delivery::where('is_payed', 1)
            ->whereHas('delivery_plants', static function ($query) use ($plantIds) {
            return $query->whereIn('product_id', $plantIds)->where('product_type_id', LandImage::LAND_TYPE_MEAT);
        })->first();
    }

    /**
     * @param int $userId
     * @return Collection|null
     */
    public static function getNotDelivered(int $userId): ?Delivery
    {
        return Delivery::where(['user_id' => $userId, 'delivered' => false, 'is_payed' => true])->first();
    }

    /**
     * @param int $userId
     * @return Collection|null
     */
    public static function get(int $userId): ?Collection
    {
        return Delivery::where('user_id', $userId)->get();
    }

    /**
     * @param int $id
     * @param int $userId
     * @return Delivery
     */
    public static function getById(int $id, int $userId): ?Delivery
    {
        return Delivery::where('id', $id)->where('user_id', $userId)->first();
    }

    /**
     * @param int $userId
     * @return Delivery
     */
    public static function getLast(int $userId): ?Delivery
    {
        return Delivery::where('user_id', $userId)->latest('id')->first();
    }

    /**
     * @return array
     */
    public static function getAvailableDays(): array
    {
        $days = DeliveryController::DAYS;   //Config::getAdditional(Config::DELIVERY_DAYS);
        $today = Carbon::today(ConfigService::LOCAL_TIMEZONE);
        $availableDays = [];
        $todayNum = (int) $today->format('N');
        switch ($todayNum) {
            case 1:
            case 2:
                $availableDays[] = 3;
                break;
            case 3:
            case 4:
                $availableDays[] = 5;
                break;
            case 5:
            case 6:
            case 7:
                $availableDays[] = 1;
                break;
        }
        return collect($days)->only($availableDays)->pluck('name')->values()->toArray();
    }

    /**
     * @return array
     */
    public static function getAvailableDaysOld(): array
    {
        $days = Config::getAdditional(Config::DELIVERY_DAYS);
        $today = Carbon::today(ConfigService::LOCAL_TIMEZONE);
        $availableDays = [$today->copy()->add(2, 'day')->format('N')];
        if (Carbon::now(ConfigService::LOCAL_TIMEZONE)->hour < 12) {
            $availableDays[] = $today->copy()->add(1, 'day')->format('N');
        }
        return collect($days)->only($availableDays)->values()->toArray();
    }

    /**
     * @param string $day
     * @return int
     */
    public static function getIdByDayName(string $day): int
    {
        $days = Config::getAdditional(Config::DELIVERY_DAYS);
        return array_search($day, $days, true);
    }

    /**
     * @param string $day
     * @return Carbon
     */
    public static function getScheduledDate(string $day): Carbon
    {
        $dayNum = self::getIdByDayName($day);
        $today = Carbon::today(ConfigService::LOCAL_TIMEZONE);
        return $today->next(self::DAYS[$dayNum]);
    }

    /**
     * @param int $id
     * @param int $userId
     * @return
     */
    public static function delete(int $id, int $userId)
    {
        return Delivery::where('id', $id)->where('user_id', $userId)->delete();
    }
}
