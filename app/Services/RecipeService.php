<?php

namespace App\Services;

use App\Models\Recipe;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class RecipeService
 * @package App\Services
 */
class RecipeService
{
    private $model;

    public function __construct(Recipe $land)
    {
        $this->model = $land;
    }

    /**
     * @param $params
     * @return Recipe
     */
    public function create($params): Recipe
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $id
     * @return Recipe|null
     */
    public static function get(int $id): ?Recipe
    {
        return Recipe::where('id', $id)->first();
    }

    /**
     * @param array $ids
     * @return Recipe|null
     */
    public static function getByIds(array $ids):? array
    {
        return Recipe::whereIn('id', $ids)->get()->keyBy('id')->toArray();
    }

    /**
     * @param int $id
     * @return Recipe|null
     */
    public static function getByPlantTypeId(int $id):? Collection
    {
        return Recipe::whereHas('plantTypes', function ($query) use ($id){
            return $query->where('plant_type_id', '=', $id);
        })->get();
    }

    /**
     * @param int $id
     * @param array $data
     * @return Collection|null
     */
    public static function update(int $id, array $data):? Recipe
    {
        $model = Recipe::where('id', $id)->first();
        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * @param int $id
     */
    public static function delete(int $id): void
    {
        Recipe::where('id', $id)->delete();
    }

}
