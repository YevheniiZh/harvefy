<?php

namespace App\Services;

use App\Models\About;

/**
 * Class AboutService
 * @package App\Services
 */
class AboutService
{
    private $model;

    public function __construct(About $land)
    {
        $this->model = $land;
    }

    /**
     * @param $params
     * @return About
     */
    public function create($params): About
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $id
     * @return About|null
     */
    public static function get(int $id): ?About
    {
        return About::where('id', $id)->first();
    }

    /**
     * @param array $ids
     * @return About|null
     */
    public static function getByIds(array $ids):? array
    {
        return About::whereIn('id', $ids)->get()->keyBy('id')->toArray();
    }

    /**
     * @param int $id
     * @param array $data
     * @return Collection|null
     */
    public static function update(int $id, array $data):? About
    {
        $model = About::where('id', $id)->first();
        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * @param int $id
     */
    public static function delete(int $id): void
    {
        About::where('id', $id)->delete();
    }
}
