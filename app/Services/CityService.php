<?php

namespace App\Services;

use App\Models\City;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class CityService
 * @package App\Services
 */
class CityService extends Service
{
    private $model;

    public function __construct(City $model)
    {
        $this->model = $model;
    }

    /**
     * @param $params
     * @return City
     */
    public function create($params): City
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $id
     * @return City
     */
    public static function getById(int $id): ?City
    {
        return City::where('id', $id)->first();
    }

    /**
     * @return array|null
     */
    public static function getAvailable():? array
    {
        return City::where('plants_available', true)->orWhere('meat_available', true)->get()->toArray();
    }

    /**
     * @param int $id
     * @param array $data
     * @return Collection|null
     */
    public static function update(int $id, array $data):? City
    {
        $model = City::where('id', $id)->first();
        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * @param int $id
     * @return void
     */
    public static function delete(int $id): void
    {
        City::where('id', $id)->delete();
    }
}
