<?php

namespace App\Services;

use App\Models\PlantType;

/**
 * Class PlantTypeService
 * @package App\Services
 */
class PlantTypeService
{
    private $model;

    public function __construct(PlantType $land)
    {
        $this->model = $land;
    }

    /**
     * @param $params
     * @return PlantType
     */
    public function create($params): PlantType
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $id
     * @return PlantType|null
     */
    public static function get(int $id): ?PlantType
    {
        return PlantType::where('id', $id)->first();
    }

    /**
     * @param array $ids
     * @return PlantType|null
     */
    public static function getByIds(array $ids):? array
    {
        return PlantType::whereIn('id', $ids)->get()->keyBy('id')->toArray();
    }

    /**
     * @param int $id
     * @param array $data
     * @return Collection|null
     */
    public static function update(int $id, array $data):? PlantType
    {
        $model = PlantType::where('id', $id)->first();
        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * @param int $id
     */
    public static function delete(int $id): void
    {
        PlantType::where('id', $id)->delete();
    }
}
