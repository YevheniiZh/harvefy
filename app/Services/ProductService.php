<?php

namespace App\Services;

use App\Models\Config;
use App\Models\Product;
use App\Models\Subscription;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Arr;

/**
 * Class ProductService
 * @package App\Services
 */
class ProductService
{
    /*
     * reminder hours sequence before {event}
     */
    protected const WATERING_REQUIRED_REMINDER_HOURS = [24, 16, 8, 4];
    protected const HARVEFY_REQUIRED_REMINDER_HOURS = [24, 16, 8, 4];
    protected const DELIVERY_REQUIRED_REMINDER_HOURS = [24, 16, 8, 4];

    /**
     * @param int $id
     * @param int $status
     * @param string|null $action
     * @return Product
     */
    public static function updateStatus(int $id, int $status, string $action = null): Product
    {
        $model = static::$modelClass::where('id', $id)->first();
        $oldStatus = $model->status;
        if ($oldStatus === $status) {
            return $model;
        }
        $model->status = $status;
        $model->status_updated_at = Carbon::now();
        if ($status === Product::STATUS_WATERING_REQUIRED) {
            $model->status_watering_required_at = Carbon::now();
        }
        $model->save();
        $model->refresh();

        app(PlantStatusLogService::class)->create([
            'product_id' => $id,
            'product_type_id' => static::TYPE_ID,
            'old_status' => $oldStatus,
            'new_status' => $status,
            'action' => $action
        ]);

        return $model;
    }

    /**
     * @param int $id
     * @param array $params
     * @return Product
     */
    public static function update(int $id, array $params): Product
    {
        $model = static::$modelClass::where('id', $id)->first();
        $model->fill($params);
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param int $id
     * @return Product
     */
    public static function pourOn(int $id): Product
    {
        $model = static::$modelClass::where('id', $id)->first();
        $oldStatus = $model->status;
        $model->status = Product::STATUS_IS_GROWING;
        $model->last_watering = Carbon::now();
        $model->watering_log = array_merge((array) $model->watering_log, [$model->last_watering]);
        $model->save();
        $model->refresh();

        app(PlantStatusLogService::class)->create([
            'product_id' => $id,
            'product_type_id' => static::TYPE_ID,
            'old_status' => $oldStatus,
            'new_status' => Product::STATUS_IS_GROWING,
            'action' => 'pourOn'
        ]);

        return $model;
    }

    /**
     * @param int $timestamp
     * @return array
     * @throws \Exception
     */
    public static function getHoursInterval(int $timestamp): array {
        $created_at = (new DateTime())->setTimestamp($timestamp);
        $interval = (new DateTime())->diff($created_at);
        $days = $interval->format('%a');
        $hours = 0;
        if ($days) {
            $hours += 24 * $days;
        }
        $hours += (int) $interval->format('%H');
        return ['days' => $days, 'hours' => $hours];
    }

    /**
     * @param int $id
     */
    public static function delete(int $id): void
    {
        static::$modelClass::where('id', $id)->delete();
    }


    /**
     * @return array
     * @throws \Exception
     */
    public static function runAutowatering(): array
    {
        $products = static::$modelClass::where('autowatering', true)
            ->whereIn('status', [Product::STATUS_IS_GROWING, Product::STATUS_WATERING_REQUIRED])
            ->where(static function ($query) {
                $query->where('last_watering', null);
                $query->orWhereRaw('DATE_ADD(last_watering, INTERVAL 1 DAY) < now()');
            })
            /*->whereHas('subscription', function ($query) {
                return $query->whereNotIn('status', [Subscription::STATUS_UNSUBSCRIBED, Subscription::STATUS_UNSUBSCRIBED_DUE_CANT_CHARGE]);
            })*/
            ->get()->keyBy('id')->toArray();
        foreach ($products as $productId => $product) {
            self::pourOn($productId);
        }
        return array_keys($products);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public static function checkStatusForUpdate(): array
    {
        $result = [];
        $products = static::getAllPayed();
        foreach ($products as $product) {
            if (array_key_exists($product['status'], Product::HANDLED_BY_SCHEDULE_STATUSES)) {
                if ($statusResult = self::handleByStatus($product['status'], $product)) {
                    $result[(int)$product['id']] = $statusResult;
                }
            }
        }
        return $result;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public static function sendHasGrownToday(): array
    {
        $result = [];
        $products = static::getAllGrownToday();
        foreach ($products as $product) {
            $result[] = self::sendNotification($product, Product::STATUS_HAS_GROWN);
        }
        return $result;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public static function changeStatusHasGrownToday(): array
    {
        $result = [];
        $products = static::getAllNotGrow();
        foreach ($products as $product) {
            if (static::checkHasGrownToday($product)) {
                self::updateStatus($product['id'], Product::STATUS_HAS_GROWN, 'changeStatusHasGrownToday');
                $result[] = $product['id'];
            }
        }
        return $result;
    }

    /**
     * @param int $status
     * @param $product
     * @return array|null
     * @throws \Exception
     */
    public static function handleByStatus(int $status, array $product):? array
    {
        $newStatus = $notification = null;
        switch ($status) {
            case Product::STATUS_IS_GROWING:
                if (static::checkHasGrown($product)) {
                    $newStatus = Product::STATUS_HAS_GROWN;
                    DeliveryService::addToActiveDelivery($product, static::$modelClass);
                    //$notification = self::sendNotification($product, $newStatus);
                    break;
                }
                $last_watering = $product['last_watering'] ?: $product['created_at'];
                [$diffHours, $diffMinutes, $minutes] = self::getDiffFrom($last_watering);
                $minutesBefore = Config::getValue(Config::MINUTES_BEFORE_WATERING_REQUIRED);
                if ($diffMinutes >= $minutesBefore) {
                    $newStatus = Product::STATUS_WATERING_REQUIRED;
                    $notification = self::sendNotification($product, $newStatus);
                }
                break;

            case Product::STATUS_WATERING_REQUIRED:
                if (static::checkHasGrown($product)) {
                    $newStatus = Product::STATUS_HAS_GROWN;
                    DeliveryService::addToActiveDelivery($product, static::$modelClass);
                    break;
                }
                [$diffHours, $diffMinutes, $minutes] = self::getDiffFrom($product['status_watering_required_at']);
                $minutesBeforeDie = Config::getValue(Config::MINUTES_BEFORE_DIE_WITHOUT_WATERING);
                $reminderHours = self::getSequenceAfter(self::WATERING_REQUIRED_REMINDER_HOURS, $minutesBeforeDie);
                if ($diffMinutes >= $minutesBeforeDie) {
                    $newStatus = Product::STATUS_DIED;
                    $notification = self::sendNotification($product, $newStatus);
                } elseif ($minutes < 10 && in_array($diffHours, $reminderHours, true)) {
                    $notification = self::sendNotification($product, $status);
                }
                break;

            case Product::STATUS_HAS_GROWN:
                [$diffHours, $diffMinutes, $minutes] = self::getDiffFrom($product['status_updated_at']);
                $minutesBeforeDie = Config::getValue(Config::MINUTES_BEFORE_DIE_AFTER_GROWN);
                $reminderHours = self::getSequenceAfter(self::HARVEFY_REQUIRED_REMINDER_HOURS, $minutesBeforeDie);
                if ($diffMinutes >= $minutesBeforeDie) {
                    $newStatus = Product::STATUS_DIED;
                    $notification = self::sendNotification($product, $newStatus);
                } elseif ($minutes < 10 && in_array($diffHours, $reminderHours, true)) {
                    $notification = self::sendNotification($product, $status);
                }
                break;
            case Product::STATUS_IN_BASKET:
                [$diffHours, $diffMinutes, $minutes] = self::getDiffFrom($product['status_updated_at']);
                $minutesBeforeDieInBasket = Config::getValue(Config::MINUTES_BEFORE_DIE_IN_BASKET);
                $reminderHours = self::getSequenceAfter(self::DELIVERY_REQUIRED_REMINDER_HOURS, $minutesBeforeDieInBasket);
                if ($diffMinutes >= $minutesBeforeDieInBasket) {
                    $newStatus = Product::STATUS_DIED_IN_BASKET;
                    $notification = self::sendNotification($product, $newStatus);
                } elseif ($minutes < 10 && in_array($diffHours, $reminderHours, true)) {
                    $hours = ceil($minutesBeforeDieInBasket / 60) - $diffHours;
                    $notification = self::sendNotification($product, $status,
                        str_replace('{hours}', $hours, static::NOTIFICATION_TITLE_DIED_AFTER));
                }
                break;
        }

        if ($newStatus) {
            self::updateStatus($product['id'], $newStatus, 'cron');
            return ['old_status' => $status, 'new_status' => $newStatus, 'notification' => $notification];
        }
        return null;
    }

    /**
     * @param array $sequence
     * @param int $minutesBeforeDie
     * @return array
     */
    protected static function getSequenceAfter(array $sequence, int $minutesBeforeDie): array {
        return array_map(static function ($hour) use ($minutesBeforeDie) {
            return (int) ceil($minutesBeforeDie / 60) - $hour;
        }, $sequence);
    }

    /**
     * @param $timestamp
     * @return array
     */
    protected static function getDiffFrom($timestamp): array {
        $status_updated_at = Carbon::createFromTimestamp($timestamp);
        $diffHours = Carbon::now()->diffInHours($status_updated_at);
        $diffMinutes = Carbon::now()->diffInMinutes($status_updated_at);
        $minutes = $diffMinutes - ($diffHours * 60);
        return [$diffHours, $diffMinutes, $minutes];
    }

    /**
     * @param array $product
     * @param int $newStatus
     * @param string|null $body
     * @return array|null
     */
    public static function sendNotification(array $product, int $newStatus, string $body = ''):? array
    {
        $subscriptionNextPayment = Arr::get($product, 'subscription.next_payment');
        $subscriptionActive = Carbon::now()->lt(Carbon::createFromTimestamp($subscriptionNextPayment));
        if ($subscriptionActive && $client_token_ids = NotificationService::getClientTokenIds($product['user_id'])) {
            $data = [
                'title' => static::NOTIFICATION_TITLES[$newStatus],
                'body' => $body ?: Arr::get($product, 'land.name') . ' | ' .
                    Arr::get($product, static::$typeField . '.name')
            ];
            $response = [];
            foreach ($client_token_ids as $client_token_id) {
                $result = NotificationService::sendNotification($data, $client_token_id);
                $result['client_token_id'] = $client_token_id;
                NotificationService::logNotification($result, $product, $data['title']);
                $response[] = $result;
            }
            return $response;
        }
        return null;
    }
}
