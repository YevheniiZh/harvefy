<?php

namespace App\Services;

use App\Models\AdminStreets;

/**
 * Class AdminStreetsService
 * @package App\Services
 */
class AdminStreetsService extends Service
{
    private $model;

    public function __construct(AdminStreets $delivery)
    {
        $this->model = $delivery;
    }

    /**
     * @param $params
     * @return AdminStreets
     */
    public function create($params): AdminStreets
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $id
     * @return
     */
    public static function delete(int $id)
    {
        return AdminStreets::where('street_id', $id)->delete();
    }
}
