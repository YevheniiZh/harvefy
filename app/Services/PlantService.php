<?php

namespace App\Services;

use App\Casts\Timezone;
use App\Helpers\MailHelper;
use App\Mail\NewPlants;
use App\Models\Config;
use App\Models\Plant;
use App\Models\PlantType;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class PlantService
 * @package App\Services
 */
class PlantService extends ProductService
{
    private $model;
    public static $modelClass = Plant::class;
    public static $typeField = 'plant_type';
    public const TYPE_ID = 1;

    protected const NOTIFICATION_TITLES = [
        Plant::STATUS_HAS_GROWN => 'Можна збирати врожай',
        Plant::STATUS_IN_BASKET => 'Оформіть доставку 🚚',
        Plant::STATUS_WATERING_REQUIRED => 'Полий мене',
        Plant::STATUS_DIED => 'Рослина зів‘яла на городі',
        Plant::STATUS_DIED_IN_BASKET => 'Рослина зів’яла у кошику',
    ];

    protected const NOTIFICATION_TITLE_DIED_AFTER = 'Ваша продукція зіпсується через {hours} години!';

    public function __construct(Plant $landPlant)
    {
        $this->model = $landPlant;
    }

    /**
     * @param $params
     * @return Plant
     */
    public function create($params): Plant
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $id
     * @return Plant|null
     */
    public static function get(int $id): ?Plant
    {
        return Plant::where('id', $id)->with('plantType', 'delivery')->first();
    }

    /**
     * @param array $ids
     * @return Plant|null
     */
    public static function getByIds(array $ids):? array
    {
        return Plant::whereIn('id', $ids)->with('delivery')->get()->keyBy('id')->toArray();
    }

    /**
     * @param int $landId
     * @param int $position
     * @return Plant|null
     */
    public static function getActivePlantByPostision(int $landId, int $position):? Plant
    {
        $statusIds = [Plant::STATUS_IS_GROWING, Plant::STATUS_WATERING_REQUIRED, Plant::STATUS_HAS_GROWN];
        return Plant::where('land_id', $landId)->whereIn('status', $statusIds)->where('land_position', $position)->first();
    }

    /**
     * @return array
     */
    public static function getAllPayed(): array
    {
        return Plant::/*whereHas('land', function ($query) {
            return $query->where('is_payed', 1);
        })->*/with('plantType', 'subscription', 'land')->get()->toArray();
    }

    /**
     * @return array
     */
    public static function getAllNotGrow(): array
    {
        return Plant::whereIn('status', [Product::STATUS_IS_GROWING, Product::STATUS_WATERING_REQUIRED])
            ->with('plantType', 'subscription', 'land')->get()->toArray();
    }

    /**
     * @return array
     */
    public static function getAllGrownToday(): array
    {
        return Plant::where('status', Product::STATUS_HAS_GROWN)
            ->whereRaw('DATE(status_updated_at) = CURDATE()')
            ->with('plantType', 'subscription', 'land')->get()->toArray();
    }

    /**
     * @param array $product
     * @return bool
     */
    public static function checkHasGrown(array $product): bool
    {
        $daysToGrow = (int) PlantTypeService::get($product['plant_type_id'])->days_to_grow;
        $grownOn = Carbon::createFromTimestamp($product['created_at'])->addDays($daysToGrow);
        return $grownOn <= Carbon::now();
    }

    /**
     * @param array $product
     * @return bool
     */
    public static function checkHasGrownToday(array $product): bool
    {
        $daysToGrow = (int) PlantTypeService::get($product['plant_type_id'])->days_to_grow;
        $grownOn = Carbon::createFromTimestamp($product['created_at'])->addDays($daysToGrow);
        return $grownOn <= Carbon::now()->endOfDay();
    }

    /**
     * @param int $userId
     * @param array $statusIds
     * @return Collection|null
     */
    public static function getByUserId(int $userId, array $statusIds = []): ?Collection
    {
        return Plant::where('user_id', $userId)->whereHas('land', function ($query) {
            return $query->where('is_payed', 1);
        })->whereIn('status', $statusIds)->with('plantType', 'delivery')->get();
    }

    /**
     * @return array
     * @throws \Throwable
     */
    public static function sendNewPlants(): array
    {
        if (Carbon::now(ConfigService::LOCAL_TIMEZONE)->hour < 16) {
            $period = '16:00 - 9:00';
            $whereQuery = "CONVERT_TZ(created_at,'+00:00','+02:00')
between date_format(DATE_SUB(CONVERT_TZ(now(),'+00:00','+02:00'), INTERVAL 1 DAY),'%Y-%m-%d 16:00:00') 
    and date_format(CONVERT_TZ(now(),'+00:00','+02:00'),'%Y-%m-%d 09:00:00')";
        } else {
            $period = '9:00 - 16:00';
            $whereQuery = "CONVERT_TZ(created_at,'+00:00','+02:00')
between date_format(CONVERT_TZ(now(),'+00:00','+02:00'),'%Y-%m-%d 09:00:00')
    and date_format(CONVERT_TZ(now(),'+00:00','+02:00'),'%Y-%m-%d 16:00:00')";
        }
        $lastItems = static::$modelClass::whereHas('land', function ($query) {
            return $query->where('is_payed', 1);
        })->whereRaw($whereQuery)
            ->selectRaw('plant_type_id, count(id) as count')
            ->groupBy('plant_type_id')
            ->withCasts(['created_at' => Timezone::class])
            ->get();
        if (!$lastItems->count()) {
            return ['No new plants'];
        }

        $plantTypes = Service::indexById(PlantType::all(['id', 'name'])->toArray());

        $message = view('new_plants_email', [
            'lastItems' => $lastItems,
            'product_types' => $plantTypes
        ])->render();
        $data = new \stdClass();
        $data->subject = "New plants {$period}";
        $data->message = $message;
        $result = [];
        $emails = Config::getAdditional(Config::NEW_PLANTS_TO_EMAIL);
        $emails[] = env('NEW_PLANTS_TO_EMAIL');
        foreach ($emails as $email) {
            $result[] = MailHelper::sendEmail($email, new NewPlants($data));
        }
        return $result;
    }

    /**
     * @param Plant $item
     * @param int $growDays
     * @return int
     */
    public function harvestIn(Plant $item, int $growDays): int
    {
        return max($item->plantType->days_to_grow - $growDays, 0);
    }

    /**
     * @param Plant $item
     * @param int $growHours
     * @return int
     */
    public function grownOnPercent(Plant $item, int $growHours): int
    {
        $days_to_grow = $item->plantType->days_to_grow;
        $percent = round(100 / ($days_to_grow * 24) * $growHours);
        $percent = $percent > 100 ? 100 : (int) $percent;

        if ($item->status < Plant::STATUS_HAS_GROWN && $percent === 100) {
            $percent = 99;
        }
        return $percent;
    }
}
