<?php

namespace App\Services\V1;

use App\Models\V1\PlantStatusLog;

/**
 * Class PlantStatusLogService
 * @package App\Services
 */
class PlantStatusLogService extends Service
{
    private $model;

    public function __construct(PlantStatusLog $delivery)
    {
        $this->model = $delivery;
    }

    /**
     * @param $params
     * @return PlantStatusLog
     */
    public function create($params): PlantStatusLog
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }
}
