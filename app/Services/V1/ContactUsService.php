<?php

namespace App\Services\V1;

use App\Models\V1\ContactUs;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class ContactUsService
 * @package App\Services
 */
class ContactUsService extends Service
{
    private $model;

    public function __construct(ContactUs $contactUs)
    {
        $this->model = $contactUs;
    }

    /**
     * @param $params
     * @return ContactUs
     */
    public function create($params): ContactUs
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $id
     * @return ContactUs|null
     */
    public static function markAsHandled(int $id):? ContactUs
    {
        $model = ContactUs::where('id', $id)->first();
        if(!$model) {
            return null;
        }
        $model->status = true;
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param int $userId
     * @return Collection|null
     */
    public static function get(int $userId): ?Collection
    {
        return ContactUs::where('user_id', $userId)->get();
    }

    /**
     * @param int $id
     * @param int $userId
     * @return ContactUs
     */
    public static function getById(int $id, int $userId): ?ContactUs
    {
        return ContactUs::where('id', $id)->where('user_id', $userId)->first();
    }

    /**
     * @param int $id
     */
    public static function delete(int $id): void
    {
        ContactUs::where('id', $id)->delete();
    }

}
