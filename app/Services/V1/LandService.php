<?php

namespace App\Services\V1;

use App\Models\V1\Land;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

/**
 * Class LandService
 * @package App\Services
 */
class LandService extends Service
{
    private $model;

    public function __construct(Land $land)
    {
        $this->model = $land;
    }

    /**
     * @param $params
     * @return Land
     */
    public function create($params): Land
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $id
     * @param array $params
     * @return Land
     */
    public function update(int $id, array $params): Land
    {
        $model = Land::where('id', $id)->first();
        $model->fill($params);
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param int $landId
     * @param array $plant
     * @return Land
     */
    public function addPlant(int $landId, array $plant): Land
    {
        $model = Land::where('id', $landId)->first();
        $plants = collect($model->plants);
        $plants = $plants->filter(function($item) use ($plant) {
            return $item['position'] != $plant['position'];
        })->values()->toArray();
        $plants[] = $plant;
        $model->plants = $plants;
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param int $id
     * @param bool $isPayed
     * @return Land|null
     */
    public static function setIsPayed(int $id, bool $isPayed = true):? Land
    {
        $model = Land::where('id', $id)->first();
        if(!$model) {
            return null;
        }
        $model->is_payed = $isPayed;
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param int $userId
     * @return Collection|null
     */
    public static function get(int $userId): ?Collection
    {
        return Land::where(['user_id' => $userId, 'is_payed' => 1])->with('subscription')->get();
    }

    /**
     * @param int $id
     * @param int $userId
     * @return Land
     */
    public static function getById(int $id, int $userId): ?Land
    {
        return Land::where('id', $id)->where('user_id', $userId)->with('subscription')->first();
    }

    /**
     * @param array $result
     * @return array|null
     */
    public static function mapPlants(array $result):? array
    {
        $plantTypeIds = array_column($result['plants'], 'plant_type_id');
        $plantIds = array_column($result['plants'], 'id');

        $plantTypes = PlantTypeService::getByIds($plantTypeIds);
        $plants = PlantService::getByIds($plantIds);

        foreach ($result['plants'] as $key => $plant) {
            $plant['plant_type'] = $plantTypes[$plant['plant_type_id']];
            if (Arr::get($plant, 'id')) {
                $plant['plant_item'] = $plants[$plant['id']];
                $grow = PlantService::getHoursInterval(Arr::get($plant['plant_item'], 'created_at', 0));
                $harvest_in = $plant['plant_type']['days_to_grow'] - $grow['days'];
                $plant['plant_item']['harvest_in'] = max($harvest_in, 0);
                unset($plant['autowatering']);
            }

            $result['plants'][$key] = $plant;
        }
        return $result;
    }

    /**
     * @param int $id
     */
    public static function delete(int $id): void
    {
        Land::where('id', $id)->delete();
    }
}
