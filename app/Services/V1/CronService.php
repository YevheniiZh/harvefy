<?php

namespace App\Services\V1;

use App\Helpers\CarbonHelper;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;

class CronService
{

    public static function executeTasksForThisTime(): void
    {
        $allTasks = self::getTasksOrFail();
        $tasksToExecute = [];

        foreach ($allTasks as $task) {
            if (self::checkIfTimeToExecute($task)) {
                $tasksToExecute[] = $task['command'];
            }
        }

        foreach ($tasksToExecute as $task) {
            // Call the method of a class from App\Console\Commands
            $task->handle();
        }
    }

    public static function getTasksOrFail()
    {
        $tasks = (array)config('cron_tasks');
        // Optional protection in case of a mistake in configuration
        self::throwErrorIfTasksNotCorrect($tasks);

        return $tasks;
    }

    private static function throwErrorIfTasksNotCorrect(array $tasks): void
    {
        //$timeFields = ['day', 'hour', 'minute'];

        foreach ($tasks as $task) {
            if (!isset($task['command']) || !is_object($task['command'])
                || !isset($task['modeInterval']) || !isset($task['modeIntervalMinutes'])) {

                throw new \Exception('Invalid command class for CRON task.');
            }

            /*foreach ($timeFields as $field) {
                if (!isset($task[$field]) || !is_numeric($task[$field])) {
                    throw new \Exception('Incorrect time details for CRON task command: ' . get_class($task['command']));
                }
            }*/
        }
    }

    private static function checkIfTimeToExecute($task): bool
    {
        if ($task['modeInterval']) {
            $minutes = $task['modeIntervalMinutes'];
            $currentMinutes = CarbonHelper::getMinutesSinceStartOfDay();

            if ($currentMinutes % $minutes === 0) {
                if (Arr::get($task, 'type') === 'SendNewPlants') {
                    $hourNow = Carbon::now(ConfigService::LOCAL_TIMEZONE)->hour;
                    return $hourNow === 9 || $hourNow === 16;
                }
                return true;
            }
        } else {
            CarbonHelper::getDayOfWeekAndHourAndMinute($day, $hour, $minute);

            if ($day == $task['day'] && $hour == $task['hour'] && $minute == $task['minute']) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $file
     * @param array $data
     */
    public static function writeLog(string $file, array $data): void {
        file_put_contents(
            base_path() . '/' . $file,
            date('d-m-Y H:i:s') . ' handled ' . json_encode($data) . PHP_EOL,
            FILE_APPEND
        );
    }
}