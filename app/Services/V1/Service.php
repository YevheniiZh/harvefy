<?php

namespace App\Services\V1;


/**
 * Class Service
 * @package App\Services
 */
class Service
{
    /**
     * @param array $list
     * @return array
     */
    public static function indexById(array $list): array
    {
        $keys = array_column($list, 'id');

        return array_combine($keys, $list);
    }

}
