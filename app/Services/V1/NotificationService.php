<?php

namespace App\Services\V1;

use App\Models\V1\Notification;
use App\Models\V1\NotificationCustomLog;
use App\Models\V1\NotificationLog;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

/**
 * Class NotificationService
 * @package App\Services
 */
class NotificationService extends Service
{
    private const SERVER_KEY = 'AAAANfr57UY:APA91bH6UN3cgIiYZh-mpg0q7npTo1bOmvWdA6371CBbusqaOV5BieyfKLYsKfwUuGhCNWs41YQLFcF95Gw2PXboCt3ZFl0S5vRV-8V3f7vwl0OA8ai7AKSX1kROD3nTK3LeDXvpptgq';
    private const SERVER_URL = 'https://fcm.googleapis.com/fcm/send';

    private $model;

    public function __construct(Notification $subscription)
    {
        $this->model = $subscription;
    }

    /**
     * @param $params
     * @return Notification
     */
    public function create($params): Notification
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $userId
     * @param string $token
     * @return Collection|null
     */
    public function findByToken(int $userId, string $token): ?Notification
    {
        return Notification::where(['user_id' => $userId, 'client_token_id' => $token])->latest('id')->first();
    }

    /**
     * @param int $userId
     * @return Collection|null
     */
    public static function get(int $userId): ?Collection
    {
        return Notification::where('user_id', $userId)->get();
    }

    /**
     * @param int $userId
     * @return string|null
     */
    public static function getClientLastTokenId(int $userId): ?string
    {
        if ($notification = Notification::where('user_id', $userId)->latest('id')->first()) {
            return $notification->client_token_id;
        }
        return null;
    }

    /**
     * @param int $userId
     * @return array|null
     */
    public static function getClientTokenIds(int $userId): ?array
    {
        return Notification::where(['user_id' => $userId, 'active' => 1])->get()->pluck('client_token_id')->toArray();
    }

    /**
     * @return array|null
     */
    public static function getAllClientTokenIds(): ?array
    {
        return Notification::where('active', 1)->get()->pluck('client_token_id')->toArray();
    }

    /**
     * @param int $id
     * @param int $userId
     * @return Notification
     */
    public static function getById(int $id, int $userId): ?Notification
    {
        return Notification::where('id', $id)->where('user_id', $userId)->first();
    }

    /**
     * @param array $notificationData
     * @param string $client_token_id
     * @return array|null
     */
    public static function sendNotification(array $notificationData, string $client_token_id):? array
    {
        $request_body = [
            'to' => $client_token_id,
            'notification' => [
                'title' => Arr::get($notificationData, 'title', ''),
                'body' => Arr::get($notificationData, 'body', ''),
                'icon' => Arr::get($notificationData, 'icon', ''),
                'click_action' => Arr::get($notificationData, 'url', ''),
            ],
        ];

        return self::curlSendRequest($request_body);
    }

    /**
     * @param array $response
     * @param array $plant
     * @param string $title
     */
    public static function logNotification(array $response, array $plant, string $title = ''): void
    {
        $params = [
            'user_id' => Arr::get($plant, 'user_id'),
            'plant_id' => Arr::get($plant, 'id'),
            'title' => $title,
            'status' => Arr::get($response, 'success', false),
            'response' => json_encode($response),
        ];
        $model = new NotificationLog();
        $model->fill($params);
        $model->save();
        $model->refresh();
    }

    /**
     * @param array $response
     * @param int $toAllUsers
     * @param string $email
     * @param string $title
     * @param string $body
     */
    public static function logCustomNotification(array $response, int $toAllUsers, $email, string $title = '', string $body = ''): void
    {
        $params = [
            'to_all_users' => $toAllUsers,
            'email' => $toAllUsers ? null : $email,
            'title' => $title,
            'body' => $body,
            'status' => Arr::get($response, 'success', false),
            'response' => json_encode($response),
        ];
        $model = new NotificationCustomLog();
        $model->fill($params);
        $model->save();
        $model->refresh();
    }

    /**
     * @param array $request_body
     * @return array
     */
    public static function curlSendRequest(array $request_body): array {
        $fields = json_encode($request_body);
        $request_headers = [
            'Content-Type: application/json',
            'Authorization: key=' . self::SERVER_KEY,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::SERVER_URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return (array) json_decode($response);
    }
}
