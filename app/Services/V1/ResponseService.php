<?php

namespace App\Services\V1;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ResponseService used for auto formatting structure of responses.
 * Just simple helper, can be omitted.
 *
 * @package App\Services
 */
class ResponseService
{
    /**
     * @param array $data
     * @param array $errors
     * @param int $httpStatusCode
     * @param string $message
     * @return JsonResponse
     */
    public static function response(
        ?array $data = [],
        array $errors = [],
        int $httpStatusCode = Response::HTTP_OK,
        $message = '',
        $meta = null
    ): JsonResponse
    {
        if ($httpStatusCode === 419) {
            $status = 'Authentication timeout';
        } else {
            $status = Response::$statusTexts[$httpStatusCode];
        }
        $responseArr = [
            'code' => $httpStatusCode,
            'status' => $status,
        ];
        if ($data !== null) {
            $responseArr['data'] = $data;
        }

        if ($message !== '') {
            $responseArr['message'] = $message;
        }

        if ($httpStatusCode >= 400) {
            $responseArr['errors'] = [];
        }

        if ($errors) {
            $responseArr['errors'] = $errors;
        }

        if (!empty($meta)) {
            $responseArr['_meta'] = $meta;
        }

        return response()->json($responseArr, $httpStatusCode);
    }

    /**
     * @param array $data
     * @param int $httpStatusCode
     * @return JsonResponse
     */
    public static function success($data = [], int $httpStatusCode = Response::HTTP_OK, $message = ''): JsonResponse
    {
        $data = [
            'code' => $httpStatusCode,
            'status' => Response::$statusTexts[$httpStatusCode],
            'data' => $data,
        ];
        if ($message !== '') {
            $data['message'] = $message;
        }
        return response()->json($data, $httpStatusCode);
    }

    /**
     * @param string $message
     * @param int $httpStatusCode
     * @param null $errors
     * @return JsonResponse
     */
    public static function error(
        string $message = '',
        int $httpStatusCode = Response::HTTP_UNPROCESSABLE_ENTITY,
        $errors = null
    ): JsonResponse
    {
        $json = [
            'code' => $httpStatusCode,
            'status' => Response::$statusTexts[$httpStatusCode],
            'message' => $message,
            'errors' => [],
        ];
        if ($errors) {
            $formatted = [];
            try {
                foreach ($errors as $field => $error) {
                    if (!\is_array($error)) {
                        $formatted[] = [
                            'message' => $error,
                        ];
                    } else {
                        $formatted[] = [
                            'field' => $field,
                            'message' => $error[0],
                        ];
                    }
                }
            } catch (\Throwable $e) {

            }
            $json['errors'] = $formatted;
        }
        file_put_contents(
            $_SERVER['DOCUMENT_ROOT'].'/error.log',
            PHP_EOL. Carbon::now()->toDateTimeString() .' '. request()->url() .PHP_EOL. json_encode(['request' => request()->all(), 'result' => $json]).PHP_EOL,
            FILE_APPEND
        );

        return response()->json($json, $httpStatusCode);
    }

    /**
     * @param string $message
     * @param int $httpStatusCode
     * @param null $errors
     * @return JsonResponse
     */
    public static function multiplyError(
        string $message = '',
        int $httpStatusCode = Response::HTTP_UNPROCESSABLE_ENTITY,
        $errors = null
    ): JsonResponse
    {
        $json = [
            'code' => $httpStatusCode,
            'status' => Response::$statusTexts[$httpStatusCode],
            'message' => $message,
            'errors' => $errors ?: [],
        ];
        return response()->json($json, $httpStatusCode);
    }

    /**
     * @param $data
     * @return JsonResponse
     */
    public static function ok($data): JsonResponse
    {
        return self::success($data);
    }

    /**
     * @param string $token
     * @param int $userId
     * @return JsonResponse
     */
    public static function respondWithToken(string $token, int $userId): JsonResponse
    {
        return self::success([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user_id' => $userId
        ]);
    }

    /**
     * @param $data
     * @return JsonResponse
     */
    public static function created($data): JsonResponse
    {
        return self::success($data, Response::HTTP_CREATED);
    }

    /**
     * @param $data
     * @return JsonResponse
     */
    public static function accepted($data, $message = ''): JsonResponse
    {
        return self::success($data, Response::HTTP_ACCEPTED, $message);
    }

    /**
     * @param $data
     * @return JsonResponse
     */
    public static function noContent($data): JsonResponse
    {
        return self::success($data, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param string $message
     * @return JsonResponse
     */
    public static function unauthorized(): JsonResponse
    {
        return self::error('Unauthorized', Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @param string $message
     * @return JsonResponse
     */
    public static function notFound(string $errors, string $message = "Attempt to use absent data"): JsonResponse
    {
        return self::error($message, Response::HTTP_NOT_FOUND, [$errors]);
    }

    /**
     * @param array $errors
     * @return JsonResponse
     */
    public static function unprocessableEntity(array $errors): JsonResponse
    {
        return self::error("Attempt to use absent data", Response::HTTP_UNPROCESSABLE_ENTITY, $errors);
    }

    /**
     * @param string $message
     * @return JsonResponse
     */
    public static function notAllowed(string $message): JsonResponse
    {
        return self::error($message, Response::HTTP_METHOD_NOT_ALLOWED);
    }

    /**
     * @param string $message
     * @return JsonResponse
     */
    public static function dbError(string $message): JsonResponse
    {
        return self::error('Db error', Response::HTTP_INTERNAL_SERVER_ERROR, [$message]);
    }

    /**
     * @param string $message
     * @return JsonResponse
     */
    public static function systemError(string $message): JsonResponse
    {
        return self::error('System error', Response::HTTP_INTERNAL_SERVER_ERROR, [$message]);
    }

    /**
     * @param string $message
     * @return JsonResponse
     */
    public static function conflict(string $message): JsonResponse
    {
        return self::error($message, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @param string $message
     * @param array $errors
     * @return JsonResponse
     */
    public static function validationError(string $message, array $errors = []): JsonResponse
    {
        return self::error($message, Response::HTTP_BAD_REQUEST, $errors);
    }

    /**
     * @param string $entityName
     * @param string $attribute
     * @param string $value
     * @return JsonResponse
     */
    public static function notExistEntity(string $entityName, string $attribute, string $value): JsonResponse
    {
        $msg = implode(' ', ['The', $entityName, 'with', $attribute , $value, 'does not exists.']);
        return self::notFound($msg);
    }

    /**
     * @param string $entityName
     * @param array $attributes
     * @param string $value
     * @return JsonResponse
     */
    public static function existEntity(string $entityName, array $attributes): JsonResponse
    {
        $str = '';
        foreach ($attributes as $attribute => $value) {
            $str .= $attribute . ' ' . $value . ' ';
        }
        $msg = implode(' ', ['The', $entityName, 'with', $str, 'already exists.']);
        return self::conflict($msg);
    }


    /**
     * @param string $entityName
     * @param string $entityUsed
     * @param string $field
     * @return JsonResponse
     */
    public static function foreignKeyConflict(string $entityName, string $entityUsed, $field = 'id'): JsonResponse
    {
        $message = 'Operation is not allowed here.';
        $code = Response::HTTP_UNPROCESSABLE_ENTITY;
        $error = [$field => [sprintf('Can not delete %s that being used by %s.', $entityName, $entityUsed)]];

        return self::error($message, $code, $error);
    }


    /**
     * @param $data
     * @param $meta
     * @return JsonResponse
     */
    public static function paginationResponse(array $data, array $meta): JsonResponse
    {
        $httpStatusCode = Response::HTTP_OK;
        $array['code'] = $httpStatusCode;
        $array['status'] = Response::$statusTexts[$httpStatusCode];
        $array['data'] = $data;
        if(!empty($meta)) {
            $array['_meta'] = $meta;
        }

        return response()->json($array, $httpStatusCode);
    }
}
