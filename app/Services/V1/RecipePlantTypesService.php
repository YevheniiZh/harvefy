<?php

namespace App\Services\V1;

use App\Models\V1\RecipePlantTypes;

/**
 * Class RecipePlantTypesService
 * @package App\Services
 */
class RecipePlantTypesService extends Service
{
    private $model;

    public function __construct(RecipePlantTypes $delivery)
    {
        $this->model = $delivery;
    }

    /**
     * @param $params
     * @return RecipePlantTypes
     */
    public function create($params): RecipePlantTypes
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $recipe_id
     */
    public static function delete(int $recipe_id): void
    {
        RecipePlantTypes::where('recipe_id', $recipe_id)->delete();
    }
}
