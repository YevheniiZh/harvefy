<?php

namespace App\Services\V1;

use App\Casts\Timezone;
use App\Helpers\MailHelper;
use App\Mail\NewPlants;
use App\Models\LandImage;
use App\Models\V1\Config;
use App\Models\V1\Plant;
use App\Models\V1\PlantType;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

/**
 * Class PlantService
 * @package App\Services
 */
class PlantService
{
    private $model;

    /*
     * reminder hours sequence before {event}
     */
    private const WATERING_REQUIRED_REMINDER_HOURS = [24, 16, 8, 4];
    private const HARVEFY_REQUIRED_REMINDER_HOURS = [24, 16, 8, 4];
    private const DELIVERY_REQUIRED_REMINDER_HOURS = [24, 16, 8, 4];

    private const NOTIFICATION_TITLES = [
        Plant::STATUS_HAS_GROWN => 'Можна збирати врожай',
        Plant::STATUS_IN_BASKET => 'Оформіть доставку 🚚',
        Plant::STATUS_WATERING_REQUIRED => 'Полий мене',
        Plant::STATUS_DIED => 'Рослина зів‘яла на городі',
        Plant::STATUS_DIED_IN_BASKET => 'Рослина зів’яла у кошику',
    ];

    private const NOTIFICATION_TITLE_DIED_AFTER = 'Ваша продукція зіпсується через {hours} години!';

    public function __construct(Plant $landPlant)
    {
        $this->model = $landPlant;
    }

    /**
     * @param $params
     * @return Plant
     */
    public function create($params): Plant
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $id
     * @return Plant|null
     */
    public static function get(int $id): ?Plant
    {
        return Plant::where('id', $id)->with('plantType', 'delivery')->first();
    }

    /**
     * @param array $ids
     * @return Plant|null
     */
    public static function getByIds(array $ids):? array
    {
        return Plant::whereIn('id', $ids)->get()->keyBy('id')->toArray();
    }

    /**
     * @param int $landId
     * @param int $position
     * @return Plant|null
     */
    public static function getActivePlantByPostision(int $landId, int $position):? Plant
    {
        $statusIds = [Plant::STATUS_IS_GROWING, Plant::STATUS_WATERING_REQUIRED, Plant::STATUS_HAS_GROWN];
        return Plant::where('land_id', $landId)->whereIn('status', $statusIds)->where('land_position', $position)->first();
    }

    /**
     * @return array
     */
    public static function getAllPayed(): array
    {
        return Plant::whereHas('land', function ($query) {
            return $query->where('is_payed', 1);
        })->with('plantType', 'subscription', 'land')->get()->toArray();
    }

    /**
     * @param int $userId
     * @param array $statusIds
     * @return Collection|null
     */
    public static function getByUserId(int $userId, array $statusIds = []): ?Collection
    {
        return Plant::where('user_id', $userId)->whereIn('status', $statusIds)->with('plantType', 'delivery')->get();
    }

    /**
     * @param int $id
     * @param int $status
     * @param string|null $action
     * @return Plant
     */
    public static function updateStatus(int $id, int $status, string $action = null): Plant
    {
        $model = Plant::where('id', $id)->first();
        $oldStatus = $model->status;
        if ($oldStatus === $status) {
            return $model;
        }
        $model->status = $status;
        $model->status_updated_at = Carbon::now();
        if ($status === Plant::STATUS_WATERING_REQUIRED) {
            $model->status_watering_required_at = Carbon::now();
        }
        $model->save();
        $model->refresh();

        app(PlantStatusLogService::class)->create([
            'product_id' => $id,
            'product_type_id' => LandImage::LAND_TYPE_MICROGREEN,
            'old_status' => $oldStatus,
            'new_status' => $status,
            'action' => $action
        ]);

        return $model;
    }

    /**
     * @param int $id
     * @param array $params
     * @return Plant
     */
    public static function update(int $id, array $params): Plant
    {
        $model = Plant::where('id', $id)->first();
        $model->fill($params);
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param int $id
     * @return Plant
     */
    public static function pourOn(int $id): Plant
    {
        $model = Plant::where('id', $id)->first();
        $oldStatus = $model->status;
        $model->status = Plant::STATUS_IS_GROWING;
        $model->last_watering = Carbon::now();
        $model->watering_log = array_merge((array) $model->watering_log, [$model->last_watering]);
        $model->save();
        $model->refresh();

        app(PlantStatusLogService::class)->create([
            'product_id' => $id,
            'product_type_id' => LandImage::LAND_TYPE_MICROGREEN,
            'old_status' => $oldStatus,
            'new_status' => Plant::STATUS_IS_GROWING,
            'action' => 'pourOn'
        ]);

        return $model;
    }

    /**
     * @param int $timestamp
     * @return array
     * @throws \Exception
     */
    public static function getHoursInterval(int $timestamp): array {
        $created_at = (new DateTime())->setTimestamp($timestamp);
        $interval = (new DateTime())->diff($created_at);
        $days = $interval->format('%a');
        $hours = 0;
        if ($days) {
            $hours += 24 * $days;
        }
        $hours += (int) $interval->format('%H');
        return ['days' => $days, 'hours' => $hours];
    }

    /**
     * @param int $id
     */
    public static function delete(int $id): void
    {
        Plant::where('id', $id)->delete();
    }


    /**
     * @param Plant $item
     * @param int $growDays
     * @return int
     */
    public function harvestIn(Plant $item, int $growDays): int
    {
        return max($item->plantType->days_to_grow - $growDays, 0);
    }

    /**
     * @param Plant $item
     * @param int $growHours
     * @return int
     */
    public function grownOnPercent(Plant $item, int $growHours): int
    {
        $days_to_grow = $item->plantType->days_to_grow;
        $percent = round(100 / ($days_to_grow * 24) * $growHours);
        $percent = $percent > 100 ? 100 : (int) $percent;

        if ($item->status < Plant::STATUS_HAS_GROWN && $percent === 100) {
            $percent = 99;
        }
        return $percent;
    }


    /**
     * @return array
     * @throws \Exception
     */
    public static function runAutowatering(): array
    {
        $plants = Plant::where('autowatering', true)
            ->whereIn('status', [Plant::STATUS_IS_GROWING, Plant::STATUS_WATERING_REQUIRED])
            ->where(static function ($query) {
                $query->where('last_watering', null);
                $query->orWhereRaw('DATE_ADD(last_watering, INTERVAL 1 DAY) < now()');
            })
            ->get()->keyBy('id')->toArray();
        foreach ($plants as $plantId => $plant) {
            self::pourOn($plantId);
        }
        return array_keys($plants);
    }

    /**
     * @return array
     * @throws \Throwable
     */
    public static function sendNewPlants(): array
    {
        if (Carbon::now(ConfigService::LOCAL_TIMEZONE)->hour < 16) {
            $period = '16:00 - 9:00';
            $whereQuery = "CONVERT_TZ(created_at,'+00:00','+02:00')
between date_format(DATE_SUB(CONVERT_TZ(now(),'+00:00','+02:00'), INTERVAL 1 DAY),'%Y-%m-%d 16:00:00') 
    and date_format(CONVERT_TZ(now(),'+00:00','+02:00'),'%Y-%m-%d 09:00:00')";
        } else {
            $period = '9:00 - 16:00';
            $whereQuery = "CONVERT_TZ(created_at,'+00:00','+02:00')
between date_format(CONVERT_TZ(now(),'+00:00','+02:00'),'%Y-%m-%d 09:00:00')
    and date_format(CONVERT_TZ(now(),'+00:00','+02:00'),'%Y-%m-%d 16:00:00')";
        }
        $lastItems = Plant::whereHas('land', function ($query) {
            return $query->where('is_payed', 1);
        })->whereRaw($whereQuery)
            ->selectRaw('plant_type_id, count(id) as count')
            ->groupBy('plant_type_id')
            ->withCasts(['created_at' => Timezone::class])
            ->get();
        if (!$lastItems->count()) {
            return ['No new plants'];
        }

        $plantTypes = Service::indexById(PlantType::all(['id', 'name'])->toArray());

        $message = view('new_plants_email', [
            'lastItems' => $lastItems,
            'product_types' => $plantTypes
        ])->render();
        $data = new \stdClass();
        $data->subject = "New plants {$period}";
        $data->message = $message;
        $result = [];
        $emails = Config::getAdditional(Config::NEW_PLANTS_TO_EMAIL);
        $emails[] = env('NEW_PLANTS_TO_EMAIL');
        foreach ($emails as $email) {
            $result[] = MailHelper::sendEmail($email, new NewPlants($data));
        }
        return $result;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public static function checkStatusForUpdate(): array
    {
        $result = [];
        $plants = self::getAllPayed();
        foreach ($plants as $plant) {
            if (array_key_exists($plant['status'], Plant::HANDLED_BY_SCHEDULE_STATUSES)) {
                if ($statusResult = self::handleByStatus($plant['status'], $plant)) {
                    $result[(int)$plant['id']] = $statusResult;
                }
            }
        }
        return $result;
    }

    /**
     * @param array $plant
     * @return bool
     */
    public static function checkHasGrown(array $plant): bool
    {
        $daysToGrow = (int) PlantTypeService::get($plant['plant_type_id'])->days_to_grow;
        $grownOn = Carbon::createFromTimestamp($plant['created_at'])->addDays($daysToGrow);
        return $grownOn <= Carbon::now();
    }

    /**
     * @param int $status
     * @param $plant
     * @return array|null
     * @throws \Exception
     */
    public static function handleByStatus(int $status, array $plant):? array
    {
        $newStatus = $notification = null;
        switch ($status) {
            case Plant::STATUS_IS_GROWING:
                if (self::checkHasGrown($plant)) {
                    $newStatus = Plant::STATUS_HAS_GROWN;
                    $notification = self::sendNotification($plant, $newStatus);
                    break;
                }
                $last_watering = $plant['last_watering'] ?: $plant['created_at'];
                $diff = Carbon::now()->diffInHours(Carbon::createFromTimestamp($last_watering));
                if ($diff >= Plant::HOURS_BEFORE_WATERING) {
                    $newStatus = Plant::STATUS_WATERING_REQUIRED;
                    $notification = self::sendNotification($plant, $newStatus);
                }
                break;

            case Plant::STATUS_WATERING_REQUIRED:
                if (self::checkHasGrown($plant)) {
                    $newStatus = Plant::STATUS_HAS_GROWN;
                    break;
                }
                [$diffHours, $diffMinutes, $minutes] = self::getDiffFrom($plant['status_watering_required_at']);
                $minutesBeforeDie = Config::getValue(Config::MINUTES_BEFORE_DIE_WITHOUT_WATERING);
                $reminderHours = self::getSequenceAfter(self::WATERING_REQUIRED_REMINDER_HOURS, $minutesBeforeDie);
                if ($diffMinutes >= $minutesBeforeDie) {
                    $newStatus = Plant::STATUS_DIED;
                    $notification = self::sendNotification($plant, $newStatus);
                } elseif ($minutes < 10 && in_array($diffHours, $reminderHours, true)) {
                    $notification = self::sendNotification($plant, $status);
                }
                break;

            case Plant::STATUS_HAS_GROWN:
                [$diffHours, $diffMinutes, $minutes] = self::getDiffFrom($plant['status_updated_at']);
                $minutesBeforeDie = Config::getValue(Config::MINUTES_BEFORE_DIE_AFTER_GROWN);
                $reminderHours = self::getSequenceAfter(self::HARVEFY_REQUIRED_REMINDER_HOURS, $minutesBeforeDie);
                if ($diffMinutes >= $minutesBeforeDie) {
                    $newStatus = Plant::STATUS_DIED;
                    $notification = self::sendNotification($plant, $newStatus);
                } elseif ($minutes < 10 && in_array($diffHours, $reminderHours, true)) {
                    $notification = self::sendNotification($plant, $status);
                }
                break;
            case Plant::STATUS_IN_BASKET:
                [$diffHours, $diffMinutes, $minutes] = self::getDiffFrom($plant['status_updated_at']);
                $minutesBeforeDieInBasket = Config::getValue(Config::MINUTES_BEFORE_DIE_IN_BASKET);
                $reminderHours = self::getSequenceAfter(self::DELIVERY_REQUIRED_REMINDER_HOURS, $minutesBeforeDieInBasket);
                if ($diffMinutes >= $minutesBeforeDieInBasket) {
                    $newStatus = Plant::STATUS_DIED_IN_BASKET;
                    $notification = self::sendNotification($plant, $newStatus);
                } elseif ($minutes < 10 && in_array($diffHours, $reminderHours, true)) {
                    $hours = ceil($minutesBeforeDieInBasket / 60) - $diffHours;
                    $notification = self::sendNotification($plant, $status,
                        str_replace('{hours}', $hours, self::NOTIFICATION_TITLE_DIED_AFTER));
                }
                break;
        }

        if ($newStatus) {
            self::updateStatus($plant['id'], $newStatus, 'cron');
            return ['old_status' => $status, 'new_status' => $newStatus, 'notification' => $notification];
        }
        return null;
    }

    /**
     * @param array $sequence
     * @param int $minutesBeforeDie
     * @return array
     */
    private static function getSequenceAfter(array $sequence, int $minutesBeforeDie): array {
        return array_map(static function ($hour) use ($minutesBeforeDie) {
            return (int) ceil($minutesBeforeDie / 60) - $hour;
        }, $sequence);
    }

    /**
     * @param $timestamp
     * @return array
     */
    private static function getDiffFrom($timestamp): array {
        $status_updated_at = Carbon::createFromTimestamp($timestamp);
        $diffHours = Carbon::now()->diffInHours($status_updated_at);
        $diffMinutes = Carbon::now()->diffInMinutes($status_updated_at);
        $minutes = $diffMinutes - ($diffHours * 60);
        return [$diffHours, $diffMinutes, $minutes];
    }

    /**
     * @param array $plant
     * @param int $newStatus
     * @param string|null $body
     * @return array|null
     */
    public static function sendNotification(array $plant, int $newStatus, string $body = ''):? array
    {
        $subscriptionNextPayment = Arr::get($plant, 'subscription.next_payment');
        $subscriptionActive = Carbon::now()->lt(Carbon::createFromTimestamp($subscriptionNextPayment));
        if ($subscriptionActive && $client_token_ids = NotificationService::getClientTokenIds($plant['user_id'])) {
            $data = [
                'title' => self::NOTIFICATION_TITLES[$newStatus],
                'body' => $body ?: Arr::get($plant, 'land.name') . ' | ' . Arr::get($plant, 'plant_type.name')
            ];
            $response = [];
            foreach ($client_token_ids as $client_token_id) {
                $result = NotificationService::sendNotification($data, $client_token_id);
                $result['client_token_id'] = $client_token_id;
                NotificationService::logNotification($result, $plant, $data['title']);
                $response[] = $result;
            }
            return $response;
        }
        return null;
    }
}
