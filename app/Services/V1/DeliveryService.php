<?php

namespace App\Services\V1;

use App\Models\V1\Delivery;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

/**
 * Class DeliveryService
 * @package App\Services
 */
class DeliveryService extends Service
{
    private $model;

    public function __construct(Delivery $delivery)
    {
        $this->model = $delivery;
    }

    /**
     * @param $params
     * @return Delivery
     */
    public function create($params): Delivery
    {
        $this->model->fill($params);
        $this->model->meat_ids = Arr::get($params, 'meat_ids', []);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $id
     * @return Delivery|null
     */
    public static function setIsPayed(int $id):? Delivery
    {
        $model = Delivery::where('id', $id)->first();
        if(!$model) {
            return null;
        }
        $model->is_payed = true;
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param int $id
     * @return Delivery|null
     */
    public static function setDelivered(int $id):? Delivery
    {
        $model = Delivery::where('id', $id)->with('delivery_plants')->first();
        if(!$model) {
            return null;
        }
        $model->delivered = true;
        $model->delivery_date = Carbon::now();
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param array $plantIds
     * @return Collection|null
     */
    public function checkForPayedPlants(array $plantIds): ?Delivery
    {
        return Delivery::where('is_payed', 1)
            ->whereHas('delivery_plants', static function ($query) use ($plantIds) {
            return $query->whereIn('product_id', $plantIds);
        })->first();
    }

    /**
     * @param int $userId
     * @return Collection|null
     */
    public static function get(int $userId): ?Collection
    {
        return Delivery::where('user_id', $userId)->get();
    }

    /**
     * @param int $id
     * @param int $userId
     * @return Delivery
     */
    public static function getById(int $id, int $userId): ?Delivery
    {
        return Delivery::where('id', $id)->where('user_id', $userId)->first();
    }

    /**
     * @param int $id
     * @param int $userId
     */
    public static function delete(int $id, int $userId)
    {
        return Delivery::where('id', $id)->where('user_id', $userId)->delete();
    }
}
