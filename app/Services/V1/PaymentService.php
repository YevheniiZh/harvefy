<?php

namespace App\Services\V1;

use App\Models\V1\Payment;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use WayForPay\SDK\Collection\ProductCollection;
use WayForPay\SDK\Credential\AccountSecretCredential;
use WayForPay\SDK\Domain\Client;
use WayForPay\SDK\Exception\ApiException;
use WayForPay\SDK\Wizard\PurchaseWizard;

/**
 * Class PaymentService
 * @package App\Services
 */
class PaymentService
{
    public const CURRENCY = 'UAH';
    private const RETURN_URL = '/api/payments/return';
    private const SERVICE_URL = '/api/payments/service';
    private const LIQPAY_VERSION = 3;
    private const LIQPAY_LANGUAGE = 'ru';

    public const LIQPAY_STATUS_ERROR = 'error';
    public const LIQPAY_STATUS_FAILURE = 'failure';
    public const LIQPAY_STATUS_REVERSED = 'reversed';
    public const LIQPAY_STATUS_WAIT_ACCEPT = 'wait_accept';
    public const LIQPAY_STATUS_SUBSCRIBED = 'subscribed';
    public const LIQPAY_STATUS_UNSUBSCRIBED = 'unsubscribed';
    public const LIQPAY_STATUS_SUCCESS = 'success';

    public const LIQPAY_SUCCESS_STATUSES = [
        self::LIQPAY_STATUS_SUBSCRIBED,
        self::LIQPAY_STATUS_SUCCESS,
        self::LIQPAY_STATUS_WAIT_ACCEPT,
    ];

    private $model;
    protected $liqpay;

    public function __construct(Payment $payment)
    {
        $this->model = $payment;
        $this->liqpay = new LiqPay(env('LIQPAY_PUBLIC_KEY'), env('LIQPAY_PRIVATE_KEY'));
    }

    /**
     * @return LiqPay
     */
    public function getLiqPay(): LiqPay
    {
        return $this->liqpay;
    }

    /**
     * @param $params
     * @return Payment
     */
    public function create(array $params): Payment
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param string $orderId
     * @param float $amount
     * @param string $description
     * @return array|null
     */
    public function startPaymentLiqpay(
        string $orderId,
        float $amount,
        string $description
    ):? array
    {
        return $this->liqpay->cnb_form_raw([
            'action' => 'pay',
            'amount' => $amount,
            'currency' => self::CURRENCY,
            'description' => $description,
            'order_id' => $orderId,
            'result_url' => env('DOMAIN') . self::RETURN_URL,
            'server_url' => env('DOMAIN') . self::SERVICE_URL,
            'language' => self::LIQPAY_LANGUAGE,
            'version' => self::LIQPAY_VERSION
        ]);
    }

    /**
     * @param string $orderId
     * @param float $amount
     * @param string $description
     * @return array|null
     */
    public function startSubscribeLiqpay(
        string $orderId,
        float $amount,
        string $description
    ):? array
    {
        return $this->liqpay->cnb_form_raw([
            'action' => 'subscribe',
            'amount' => $amount,
            'currency' => self::CURRENCY,
            'description' => $description,
            'order_id' => $orderId,
            'subscribe' => 1,
            'subscribe_date_start' => Carbon::now()->toDateTimeString(),
            'subscribe_periodicity' => 'month',
            'result_url' => env('DOMAIN') . self::RETURN_URL,
            'server_url' => env('DOMAIN') . self::SERVICE_URL,
            'language' => self::LIQPAY_LANGUAGE,
            'version' => self::LIQPAY_VERSION
        ]);
    }

    /**
     * @param string $orderId
     * @return array|null
     */
    public function unsubscribeLiqpay(
        string $orderId
    ):? array
    {
        return (array) $this->liqpay->api("request", [
            'action' => 'unsubscribe',
            'order_id' => $orderId,
            'version' => self::LIQPAY_VERSION
        ]);
    }

    /**
     * @param string $orderId
     * @return array|null
     */
    public function getStatusLiqpay(
        string $orderId
    ):? array
    {
        return (array) $this->liqpay->api("request", [
            'action' => 'status',
            'order_id' => $orderId,
            'version' => self::LIQPAY_VERSION
        ]);
    }

    /**
     * @param string $data
     * @return array|null
     */
    public function decodeResultLiqpay(
        string $data
    ):? array
    {
        return $this->liqpay->decode_params($data);
    }

    /**
     * @param float $amount
     * @param int $userId
     * @param int $itemId
     * @param string $orderId
     * @param string $itemType
     * @return array
     */
    public function savePayment(float $amount, int $userId, int $itemId, string $orderId, string $itemType): array
    {
        $paymentData = [
            'user_id' => $userId,
            'item_id' => $itemId,
            'item_type' => $itemType,
            'amount' => $amount,
            'currency' => self::CURRENCY,
            'type' => 'PURCHASE',
            'transaction_id' => $orderId,
            'ps_response' => '',
        ];
        $model = $this->create($paymentData);

        return $model->toArray();
    }

    /**
     * @param $params
     * @return Payment
     */
    public function finishPayment(array $params):? Payment
    {
        $model = Payment::where('transaction_id', $params['order_id'])->first();
        if(!$model) {
            return null;
        }
        $model->ps_response = $params;
        $model->status = Arr::get($params, 'status');
        $model->payment_system = Arr::get($params, 'paytype', '');
        $model->card = Arr::get($params, 'sender_card_mask2', '');
        $model->card_type = Arr::get($params, 'sender_card_type', '');
        $model->fee = Arr::get($params, 'commission_credit', '0.00');
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param $params
     * @return Payment
     */
    public function storeCallbackResponse(array $params):? Payment
    {
        $model = Payment::where('transaction_id', Arr::get($params, 'order_id'))->first();
        if(!$model) {
            return null;
        }
        $callbacks = (array) json_decode($model->ps_callback, true);
        $callbacks[time()] = $params;
        $model->ps_callback = $callbacks;
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param int $userId
     * @return Collection|null
     */
    public static function get(int $userId): ?Collection
    {
        return Payment::where('user_id', $userId)->get();
    }

    /**
     * @param array $plants
     * @return int
     */
    public function getPlantsSumAmount(array $plants): int
    {
        $amounts = array_column($plants, 'price');
        return array_sum($amounts);
    }

    /**
     * @param int $id
     * @param int $userId
     * @return Payment
     */
    public static function getById(int $id, int $userId): ?Payment
    {
        return Payment::where('id', $id)->where('user_id', $userId)->first();
    }

    /**
     * @param int $id
     */
    public static function delete(int $id): void
    {
        Payment::where('id', $id)->delete();
    }
}
