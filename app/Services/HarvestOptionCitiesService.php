<?php

namespace App\Services;

use App\Models\HarvestOptionCities;

/**
 * Class HarvestOptionCitiesService
 * @package App\Services
 */
class HarvestOptionCitiesService extends Service
{
    private $model;

    public function __construct(HarvestOptionCities $model)
    {
        $this->model = $model;
    }

    /**
     * @param $params
     * @return HarvestOptionCities
     */
    public function create($params): HarvestOptionCities
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @param int $harvest_option_id
     */
    public static function delete(int $harvest_option_id): void
    {
        HarvestOptionCities::where('harvest_option_id', $harvest_option_id)->delete();
    }
}
