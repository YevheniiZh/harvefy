<?php

namespace App\Services;

use App\Models\HarvestOption;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class HarvestOptionService
 * @package App\Services
 */
class HarvestOptionService extends Service
{
    private $model;

    public function __construct(HarvestOption $model)
    {
        $this->model = $model;
    }

    /**
     * @param $params
     * @return HarvestOption
     */
    public function create($params): HarvestOption
    {
        $this->model->fill($params);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * @return array
     */
    public static function getAll(): array
    {
        return HarvestOption::with('cities')->get()->toArray();
    }

    /**
     * @param int $cityId
     * @return Collection|null
     */
    public static function getByCityId(int $cityId): ?Collection
    {
        return HarvestOption::whereHas('cities', function ($query) use ($cityId){
            return $query->where('city_id', $cityId);
        })->get();
    }

    /**
     * @param int $id
     * @return array
     */
    public static function getByMeatTypeId(int $id): array
    {
        return HarvestOption::where('meat_type_id', $id)->with('cities')->get()->toArray();
    }

    /**
     * @param int $id
     * @param int $cityId
     * @return Collection
     */
    public static function getByMeatTypeIdCityId(int $id, int $cityId): Collection
    {
        return HarvestOption::where('meat_type_id', $id)
            ->whereHas('cities', function ($query) use ($cityId){
                return $query->where('city_id', $cityId);
            })->get();
    }

    /**
     * @param int $id
     * @param array $data
     * @return Collection|null
     */
    public static function update(int $id, array $data):? HarvestOption
    {
        $model = HarvestOption::where('id', $id)->first();
        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * @param int $id
     * @return void
     */
    public static function delete(int $id): void
    {
        HarvestOption::where('id', $id)->delete();
    }
}
