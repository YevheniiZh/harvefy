<?php

namespace App\Models;


class HarvestOptionCities extends BaseModel
{
    protected $table = 'harvest_option_cities';

    protected $fillable = [
        'harvest_option_id',
        'city_id',
    ];

    protected $casts = [
        'harvest_option_id' => 'integer',
        'city_id' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

}
