<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Validation\Rule;

class Meat extends Product
{
    protected $table = 'meat';

    protected $fillable = [
        'user_id',
        'land_id',
        'meat_type_id',
        'harvest_option_id',
        'status',
        'last_watering',
        'watering_log',
        'autowatering',
        'status_watering_required_at'
    ];

    protected $casts = [
        'user_id' => 'integer',
        'land_id' => 'integer',
        'meat_type_id' => 'integer',
        'harvest_option_id' => 'integer',
        'status' => 'integer',
        'last_watering' => 'timestamp',
        'status_updated_at' => 'timestamp',
        'status_watering_required_at' => 'timestamp',
        'watering_log' => 'array',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
        'autowatering' => 'boolean',
    ];

    /**
     * @return array
     */
    public static function getCreateRules(): array
    {
        return [
            'name' => ['required', 'between:2,50', 'string'],
            'file' => ['required', 'mimetypes:image/*', 'file'],
            'videoFile' => ['mimetypes:video/*', 'file'],
            'price' => ['required', 'regex:/^\d+(\.\d{1,2})?$/'],
            'short_desc' => ['required', 'string'],
            'full_desc' => ['required', 'string'],
            'days_to_grow' => ['required', 'between:1,360', 'integer'],
            'out_of_stock' => ['required', 'boolean'],
            'teaser' => ['required', 'boolean'],
            'autowatering' => ['boolean'],
        ];
    }

    /**
     * @return array
     */
    public static function getUpdateRules(): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT, 'exists:meat,id'],
            'autowatering' => ['boolean'],
        ];
    }

    /**
     * @param int $userId
     * @return array
     */
    public static function getHarvestRules(int $userId): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT,
                Rule::exists('meat', 'id')->where('user_id', $userId)->where('status', self::STATUS_HAS_GROWN)],
        ];
    }

    /**
     * @param array $harvestOptionIds
     * @return array
     */
    public static function getHarvestOptionRules(array $harvestOptionIds): array
    {
        return [
            'harvest_option_id' => ['required', 'integer', Rule::in($harvestOptionIds)]
        ];
    }

    /**
     * @param int $userId
     * @return array
     */
    public static function getPourOnRules(int $userId): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT,
                Rule::exists('meat', 'id')->where(function ($query) use ($userId) {
                    $query->where('user_id', $userId);
                })->where('status', self::STATUS_WATERING_REQUIRED)
            ]
        ];
    }

    /**
     * @return HasOne
     */
    public function meatType(): HasOne
    {
        return $this->hasOne(MeatType::class, 'id', 'meat_type_id');
    }

    /**
     * @return HasOne
     */
    public function land(): HasOne
    {
        return $this->hasOne(Land::class, 'id', 'land_id');
    }

    /**
     * @return HasOneThrough
     */
    public function delivery(): HasOneThrough
    {
        return $this->hasOneThrough(Delivery::class, DeliveryPlants::class, 'product_id', 'id', null, 'delivery_id')
            ->where('product_type_id', LandImage::LAND_TYPE_MEAT);
    }

    /**
     * @return HasOne
     */
    public function subscription(): HasOne
    {
        return $this->hasOne(Subscription::class, 'land_id', 'land_id');
    }

    /**
     * @return HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}
