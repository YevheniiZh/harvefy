<?php

namespace App\Models;


class PlantStatusLog extends BaseModel
{
    protected $table = 'plant_status_log';

    protected $fillable = [
        'product_id',
        'product_type_id',
        'old_status',
        'new_status',
        'action',
    ];

    protected $casts = [
        'product_id' => 'integer',
        'product_type_id' => 'integer',
        'old_status' => 'integer',
        'new_status' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

}
