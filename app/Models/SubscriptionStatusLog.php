<?php

namespace App\Models;


class SubscriptionStatusLog extends BaseModel
{
    protected $table = 'subscription_status_log';

    protected $fillable = [
        'subscription_id',
        'old_status',
        'new_status',
        'action',
    ];

    protected $casts = [
        'subscription_id' => 'integer',
        'old_status' => 'integer',
        'new_status' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

}
