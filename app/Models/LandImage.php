<?php

namespace App\Models;


class LandImage extends BaseModel
{
    protected $fillable = [
        'image',
        'land_type_id',
        'product_type_id',
        'active'
    ];

    public const LAND_TYPE_MICROGREEN = 1;
    public const LAND_TYPE_MEAT = 2;

    public const LAND_TYPES = [
        self::LAND_TYPE_MICROGREEN => 'microgreen',
        self::LAND_TYPE_MEAT => 'meat',
    ];

    /**
     * @return array
     */
    public static function getCreateRules(): array
    {
        return [
            'file' => ['required', 'mimetypes:image/*', 'file'],
            'land_type_id' => ['required', 'integer', 'exists:image_types,id'],
            'product_type_id' => ['integer'],
            'file_name' => ['required_unless:land_type_id,1,2'],
            'active' => ['boolean'],
        ];
    }

    /**
     * @return array
     */
    public static function getDeleteRules(): array
    {
        return [
            'id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:land_images,id']
        ];
    }

    public function getImageAttribute($value)
    {
        return $value ? url($value) : $value;
    }

    /**
     * @param int $landTypeId
     * @param int|null $productTypeId
     * @return mixed
     */
    public static function getAllByType(int $landTypeId, int $productTypeId = null)
    {
        $builder = self::where(['land_type_id' => $landTypeId]);
        if ($productTypeId) {
            $builder->where(['product_type_id' => $productTypeId]);
        }
        return $builder->get();
    }

}
