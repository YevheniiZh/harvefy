<?php

namespace App\Models;


class ImageType extends BaseModel
{
    protected $fillable = [
        'name',
    ];

    /**
     * @return array
     */
    public static function getCreateRules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255', 'unique:image_types'],
        ];
    }

    /**
     * @return array
     */
    public static function getDeleteRules(): array
    {
        return [
            'id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:image_types,id']
        ];
    }


}
