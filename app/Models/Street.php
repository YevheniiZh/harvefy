<?php

namespace App\Models;

use App\Casts\Timezone;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;

class Street extends BaseModel
{
    protected $hidden = ['active'];

    protected $fillable = [
        'name',
        'active',
        'city_id',
    ];

    protected $casts = [
        'id' => 'int',
        'active' => 'bool',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * @return array
     */
    public static function getCreateRules(): array
    {
        return [
            'name' => ['required', 'between:2,50', 'string'],
            'admin_id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:admins,id'],
        ];
    }

    /**
     * @return array
     */
    public static function getFindRules(): array
    {
        return [
            'street' => ['between:2,50', 'string'],
        ];
    }

    /**
     * @return array
     */
    public static function getUpdateRules(): array
    {
        return [
            'id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:streets,id'],
            'name' => ['required', 'between:2,50', 'string'],
            'admin_id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:admins,id'],
        ];
    }

    /**
     * @return array
     */
    public static function getDeleteRules(): array
    {
        return [
            'id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:streets,id'
                //, 'unique:users,city_id'
            ]
        ];
    }

    /**
     * @return HasOne
     */
    public function city(): HasOne
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    /**
     * @return HasOne
     */
    public function admin_streets(): HasOne
    {
        return $this->hasOne(AdminStreets::class, 'street_id', 'id');
    }

    /**
     * @return HasOneThrough
     */
    public function admin(): HasOneThrough
    {
        return $this->hasOneThrough(Admin::class, AdminStreets::class, 'street_id',  'id', null,  'admin_id')
            //->select(['plant_type_id', 'plants.id', 'plants.created_at'])
            ->withCasts(['created_at' => Timezone::class]);
    }

}
