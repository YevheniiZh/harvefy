<?php

namespace App\Models;

class City extends BaseModel
{
    protected $hidden = ['active'];

    public const PLANTS_AVAILABLE_FIELD = 'plants_available';
    public const MEAT_AVAILABLE_FIELD = 'meat_available';

    protected $fillable = [
        'name',
        'active',
        'plants_available',
        'meat_available',
    ];

    protected $casts = [
        'id' => 'int',
        'active' => 'bool',
        'plants_available' => 'bool',
        'meat_available' => 'bool',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * @return array
     */
    public static function getCreateRules(): array
    {
        return [
            'name' => ['required', 'between:2,50', 'string'],
            'plants_available' => ['required', 'boolean'],
            'meat_available' => ['required', 'boolean'],
        ];
    }

    /**
     * @return array
     */
    public static function getUpdateRules(): array
    {
        return [
            'id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:cities,id'],
            'name' => ['required', 'between:2,50', 'string'],
            'plants_available' => ['required', 'boolean'],
            'meat_available' => ['required', 'boolean'],
        ];
    }

    /**
     * @return array
     */
    public static function getDeleteRules(): array
    {
        return [
            'id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:cities,id',
                'unique:users,city_id'
            ]
        ];
    }
}
