<?php

namespace App\Models;


use App\Services\LandService;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Validation\Rule;

class Land extends BaseModel
{
    public const TYPE_3_PLANTS = 1;
    public const TYPE_2_PLANTS = 2;
    public const TYPE_MEAT= 3;

    public const LAND_TYPES = [
        self::TYPE_3_PLANTS => '3_PLANTS',
        self::TYPE_2_PLANTS => '2_PLANTS',
        self::TYPE_MEAT => 'MEAT'
    ];

    public const LAND_TYPES_COUNT = [
        self::TYPE_3_PLANTS => 3,
        self::TYPE_2_PLANTS => 2,
        self::TYPE_MEAT => 1
    ];

    protected $fillable = [
        'user_id',
        'type',
        'name',
        'plants',
        'meat',
        'main_image',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'type' => 'integer',
        'plants' => 'array',
        'meat' => 'array',
        'is_payed' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    /**
     * @param int $type
     * @return array
     */
    public static function getCreateRules(int $type): array
    {
        if ($type === self::TYPE_MEAT) {
            $rules = [
                'meat' => ['required', 'array', 'min:' . self::LAND_TYPES_COUNT[self::TYPE_MEAT], 'max:' . self::LAND_TYPES_COUNT[self::TYPE_MEAT]],
                'meat.*.meat_type_id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:meat_types,id'],
                'meat.*.autowatering' => ['boolean']
            ];
        } else {
            $countPlantsLimit = self::LAND_TYPES_COUNT[$type];
            $rules = [
                'plants' => ['required', 'array', 'min:' . $countPlantsLimit, 'max:' . $countPlantsLimit],
                'plants.*.plant_type_id' => ['required', 'integer', 'max:' . self::MAX_INT,
                    Rule::exists('plant_types', 'id')->where('out_of_stock', 0)
                ],
                'plants.*.position' => ['required', 'integer', 'min:1', 'max:' . $countPlantsLimit, 'distinct'],
                'plants.*.autowatering' => ['boolean'],
                'plants.*.data' => ['array'],
            ];
        }
        $landTypes = LandService::getAvailableLandTypes();
        return array_merge([
            'user_id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT],
            'type' => ['required', 'integer', 'max:' . self::MAX_INT, Rule::in(array_keys($landTypes))],
            'name' => ['required', 'string'],
        ], $rules);
    }

    /**
     * @param int $userId
     * @return array
     */
    public static function getUpdateRules(int $userId): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT,
                Rule::exists('lands', 'id')->where('user_id', $userId)],
            'name' => ['string']
        ];
    }

    /**
     * @param int $userId
     * @param int $type
     * @return array
     */
    public static function getAddPlantRules(int $userId, int $type): array
    {
        $countPlantsLimit = self::LAND_TYPES_COUNT[$type];
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT,
                Rule::exists('lands', 'id')->where('user_id', $userId)->where('is_payed', true)],
            'plant_type_id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:plant_types,id'],
            'position' => ['required', 'integer', 'min:1', 'max:' . $countPlantsLimit, 'distinct'],
            'autowatering' => ['boolean'],
        ];
    }

    /**
     * @param int $userId
     * @return array
     */
    public static function getAddMeatRules(int $userId): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT,
                Rule::exists('lands', 'id')->where('user_id', $userId)->where('is_payed', true)],
            'autowatering' => ['boolean'],
        ];
    }

    /**
     * @return array
     */
    public static function getByIdRules(): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT]
        ];
    }

    public function getMainImageAttribute($value)
    {
        return $value ? url($value) : $value;
    }

    /**
     * @return HasMany
     */
    public function plants(): HasMany
    {
        return $this->hasMany(Plant::class, 'land_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function meat(): HasMany
    {
        return $this->hasMany(Meat::class, 'land_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function subscription(): HasOne
    {
        return $this->hasOne(Subscription::class, 'land_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}
