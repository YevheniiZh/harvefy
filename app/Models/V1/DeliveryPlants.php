<?php

namespace App\Models\V1;


use Illuminate\Database\Eloquent\Relations\HasOne;

class DeliveryPlants extends BaseModel
{
    protected $table = 'delivery_plants';

    protected $fillable = [
        'delivery_id',
        'product_type_id',
        'plant_id',
    ];

    protected $casts = [
        'delivery_id' => 'integer',
        'product_type_id' => 'integer',
        'plant_id' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * @return HasOne
     */
    public function delivery(): HasOne
    {
        return $this->hasOne(Delivery::class, 'id', 'delivery_id');
    }
}
