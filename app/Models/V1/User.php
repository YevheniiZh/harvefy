<?php

namespace App\Models\V1;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    public const PASSWORD_MAX_LENGTH = 50;
    public const EMAIL_MAX_LENGTH = 50;
    public const MAX_INT = 2147483647;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'city', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'city_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * @return array
     */
    public static function getCreateRules(): array
    {
        return [
            'name' => ['between:2,50', 'string'],
            'email' => ['required', 'email', 'regex:' . self::getEmailRegex(), 'min:2', 'max:' . self::EMAIL_MAX_LENGTH, 'unique:users,email'],
            'password' => ['required', 'string', 'min:5', 'max:' . self::PASSWORD_MAX_LENGTH, 'password_without_spaces'],
            'city' => ['string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
        ];
    }

    public static function getEmailRegex(): string
    {
        return "/(?:[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/";
    }

    /**
     * @return array
     */
    public static function getAuthenticationRules(): array
    {
        return [
            'email' => ['required', 'email', 'max:' . self::EMAIL_MAX_LENGTH],
            'password' => ['required', 'string', 'min:5', 'max:' . self::PASSWORD_MAX_LENGTH,'password_without_spaces']
        ];
    }

    /**
     * @return array
     */
    public static function getForgotPasswordRules(): array
    {
        return [
            'email' => ['required', 'email', 'max:' . self::EMAIL_MAX_LENGTH, 'exists:users,email'],
        ];
    }

    /**
     * @return array
     */
    public static function getByIdRules(): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT]
        ];
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function boot(): void
    {
        parent::boot();
        self::creating(function ($model) {
            $model->name = '';
            $model->city_id = 1;
        });
    }
}
