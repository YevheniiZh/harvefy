<?php

namespace App\Models\V1;

class Config extends BaseModel
{
    public const CONFIG_SUBSCRIPTION_PRICE = 'subscription_price';
    public const CONFIG_DELIVERY_PRICE = 'delivery_price';
    public const MINUTES_BEFORE_DIE_WITHOUT_WATERING = 'minutes_before_die_without_watering';
    public const MINUTES_BEFORE_DIE_AFTER_GROWN = 'minutes_before_die_after_grown';
    public const MINUTES_BEFORE_DIE_IN_BASKET = 'minutes_before_die_in_basket';
    public const NEW_PLANTS_TO_EMAIL = 'new_plants_to_email';
    public const DELIVERY_DAYS = 'delivery_days';

    protected $primaryKey = 'name';

    protected $fillable = [
        'name',
        'value',
        //'description',
        'additional',
    ];

    protected $casts = [
        'name' => 'string',
        'value' => 'integer',
        'description' => 'string',
        'additional' => 'json',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * @param int $userId
     * @return array
     */
    public static function getCreateRules(int $userId): array
    {
        return [
            'name' => ['required', 'string'],
            'value' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT],
        ];
    }

    /**
     * @return array
     */
    public static function getUpdateRules(): array
    {
        return [
            'name' => ['required', 'string', 'exists:configs,name'],
            'value' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT],
            //'description' => ['string', 'nullable'],
            'additional' => ['array', 'nullable']
        ];
    }

    public static function getValue(string $name)
    {
        if ($model = self::where('name', $name)->first()) {
            return $model->value;
        }
        return null;
    }

    public static function getAdditional(string $name)
    {
        if ($model = self::where('name', $name)->first()) {
            return $model->additional;
        }
        return null;
    }
}
