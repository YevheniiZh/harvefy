<?php

namespace App\Models\V1;


use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Validation\Rule;

class Plant extends BaseModel
{
    public const STATUS_IS_GROWING = 1;
    public const STATUS_WATERING_REQUIRED = 2; // By cron
    public const STATUS_HAS_GROWN = 3;         // By cron
    public const STATUS_DIED = 4;              // By cron
    public const STATUS_IN_BASKET = 5;
    public const STATUS_DELIVERY_REQUESTED = 6;
    public const STATUS_DELIVERED = 7;
    public const STATUS_DIED_IN_BASKET = 8;

    public const HOURS_BEFORE_WATERING = 24;

    public const STATUSES = [
        self::STATUS_IS_GROWING => 'IS_GROWING',
        self::STATUS_WATERING_REQUIRED => 'WATERING_REQUIRED',
        self::STATUS_HAS_GROWN => 'HAS_GROWN',
        self::STATUS_DIED => 'DIED_IN_GARDEN',
        self::STATUS_IN_BASKET => 'IN_BASKET',
        self::STATUS_DELIVERY_REQUESTED => 'DELIVERY_REQUESTED',
        self::STATUS_DELIVERED => 'DELIVERED',
        self::STATUS_DIED_IN_BASKET => 'DIED_IN_BASKET',
    ];

    public const HANDLED_BY_SCHEDULE_STATUSES = [
        self::STATUS_IS_GROWING => [
            self::STATUS_WATERING_REQUIRED,
            self::STATUS_HAS_GROWN,
            self::STATUS_DIED,
        ],
        self::STATUS_WATERING_REQUIRED => [
            self::STATUS_DIED,
        ],
        self::STATUS_HAS_GROWN => [
            self::STATUS_DIED,
        ],
        self::STATUS_IN_BASKET => [
            self::STATUS_DIED_IN_BASKET,
        ]
    ];

    protected $fillable = [
        'user_id',
        'land_id',
        'land_position',
        'plant_type_id',
        'status',
        'last_watering',
        'watering_log',
        'autowatering',
        'status_watering_required_at'
    ];

    protected $casts = [
        'user_id' => 'integer',
        'land_id' => 'integer',
        'land_position' => 'integer',
        'plant_type_id' => 'integer',
        'status' => 'integer',
        'last_watering' => 'timestamp',
        'status_updated_at' => 'timestamp',
        'status_watering_required_at' => 'timestamp',
        'watering_log' => 'array',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
        'autowatering' => 'boolean',
    ];

    /**
     * @return array
     */
    public static function getCreateRules(): array
    {
        return [
            'name' => ['required', 'between:2,50', 'string'],
            'file' => ['required', 'mimetypes:image/*', 'file'],
            'videoFile' => ['mimetypes:video/*', 'file'],
            'price' => ['required', 'regex:/^\d+(\.\d{1,2})?$/'],
            'short_desc' => ['required', 'string'],
            'full_desc' => ['required', 'string'],
            'days_to_grow' => ['required', 'between:1,360', 'integer'],
            'out_of_stock' => ['required', 'boolean'],
            'teaser' => ['required', 'boolean'],
            'autowatering' => ['boolean'],
        ];
    }

    /**
     * @return array
     */
    public static function getUpdateRules(): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT, 'exists:plants,id'],
            'autowatering' => ['boolean'],
        ];
    }

    /**
     * @return array
     */
    public static function getHarvestRules(int $userId): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT,
                Rule::exists('plants', 'id')->where(function ($query) use ($userId) {
                    $query->where('user_id', $userId);
                })->where('status', self::STATUS_HAS_GROWN)],
        ];
    }

    /**
     * @param int $userId
     * @return array
     */
    public static function getPourOnRules(int $userId): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT,
                Rule::exists('plants', 'id')->where(function ($query) use ($userId) {
                    $query->where('user_id', $userId);
                })->where('status', self::STATUS_WATERING_REQUIRED)
            ]
        ];
    }

    /**
     * @return HasOne
     */
    public function plantType(): HasOne
    {
        return $this->hasOne(PlantType::class, 'id', 'plant_type_id');
    }

    /**
     * @return HasOne
     */
    public function land(): HasOne
    {
        return $this->hasOne(Land::class, 'id', 'land_id');
    }

    /**
     * @return HasOneThrough
     */
    public function delivery(): HasOneThrough
    {
        return $this->hasOneThrough(Delivery::class, DeliveryPlants::class, 'product_id', 'id', null, 'delivery_id');
    }

    /**
     * @return HasOne
     */
    public function subscription(): HasOne
    {
        return $this->hasOne(Subscription::class, 'land_id', 'land_id');
    }
}
