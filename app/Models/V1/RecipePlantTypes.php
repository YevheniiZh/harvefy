<?php

namespace App\Models\V1;


class RecipePlantTypes extends BaseModel
{
    protected $table = 'recipe_plant_types';

    protected $fillable = [
        'recipe_id',
        'plant_type_id',
    ];

    protected $casts = [
        'recipe_id' => 'integer',
        'plant_type_id' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

}
