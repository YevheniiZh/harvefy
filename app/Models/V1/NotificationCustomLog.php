<?php

namespace App\Models\V1;


class NotificationCustomLog extends BaseModel
{
    protected $table = 'notifications_custom_log';

    protected $fillable = [
        'to_all_users',
        'email',
        'title',
        'body',
        'status',
        'response',
    ];

    protected $casts = [
        'to_all_users' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];
}
