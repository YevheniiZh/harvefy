<?php

namespace App\Models\V1;

use App\Helpers\Enum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class BaseModel extends Model
{
    public const MAX_INT = 2147483647;

    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * @return array
     */
    public static function getAllRules(): array
    {
        return [
            'status' => ['required', 'array'],
            'status.*' => ['distinct', 'integer', Rule::in(array_keys(Plant::STATUSES)), 'sometimes', 'required'],
        ];
    }

    /**
     * @return array
     */
    public static function getByIdRules(): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT]
        ];
    }

    /**
     * @param $value
     * @return string
     * @throws \Exception
     */
    /*public function getCreatedAtAttribute($value): string
    {
        return (new Carbon($value))->toDateTimeString();
    }*/

    /**
     * @param $value
     * @return string
     * @throws \Exception
     */
    /*public function getCreatedAtAttribute($value): string
    {
        return (new Carbon($value))->setTimezone(ConfigService::LOCAL_TIMEZONE);
    }*/

    /**
     * @param $value
     * @return string
     * @throws \Exception
     */
    /*public function getUpdatedAtAttribute($value): string
    {
        return (new Carbon($value))->toDateTimeString();
    }*/
}
