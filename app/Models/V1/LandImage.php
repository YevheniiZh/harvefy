<?php

namespace App\Models\V1;


class LandImage extends BaseModel
{
    protected $fillable = [
        'image',
        'active'
    ];

    /**
     * @return array
     */
    public static function getCreateRules(): array
    {
        return [
            'file' => ['required', 'mimetypes:image/*', 'file'],
            'active' => ['boolean'],
        ];
    }

    /**
     * @return array
     */
    public static function getDeleteRules(): array
    {
        return [
            'id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:land_images,id']
        ];
    }

    public function getImageAttribute($value)
    {
        return $value ? url($value) : $value;
    }
}
