<?php

namespace App\Models\V1;


use App\Casts\Timezone;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Validation\Rule;

class Delivery extends BaseModel
{
    protected $table = 'delivery';

    protected $fillable = [
        'user_id',
        'land_id',
        'plant_ids',
        'meat_ids',
        'city',
        'address',
        'day',
        'name',
        'phone',
        'delivered',
        'delivery_date',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'plant_ids' => 'array',
        'meat_ids' => 'array',
        'delivered' => 'boolean',
        'delivery_date' => 'timestamp',
        'is_payed' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * @param int $userId
     * @return array
     */
    public static function getCreateRules(int $userId): array
    {
        return [
            'user_id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT],
            'plant_ids' => ['required', 'array', 'min:1', 'max:' . self::MAX_INT],
            'plant_ids.*' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT,
                Rule::exists('plants', 'id')->where(function ($query) use ($userId) {
                    $query->where('user_id', $userId);
                })->where('status', Plant::STATUS_IN_BASKET),
                //'unique:delivery_plants,plant_id'
            ],
            'city' => ['required', 'string'],
            'address' => ['required', 'string'],
            'day' => ['required', 'string'],
            'name' => ['required', 'between:2,100', 'string'],
            'phone' => ['required', 'string'],
        ];
    }

    /**
     * @return array
     */
    public static function getByIdRules(): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT]
        ];
    }

    /**
     * @param int $userId
     * @return array
     */
    public static function getDeleteByIdRules(int $userId): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT,
                Rule::exists('delivery', 'id')->where('user_id', $userId)->where('is_payed', 0)
            ]
        ];
    }

    /**
     * @return HasMany
     */
    public function delivery_plants(): HasMany
    {
        return $this->hasMany(DeliveryPlants::class, 'delivery_id', 'id');
    }

    /**
     * @return HasManyThrough
     */
    public function plants(): HasManyThrough
    {
        return $this->hasManyThrough(Plant::class, DeliveryPlants::class, 'delivery_id',  'id', null,  'product_id')
            ->select(['plant_type_id', 'plants.id', 'plants.created_at'])
            ->withCasts(['created_at' => Timezone::class]);
    }

}
