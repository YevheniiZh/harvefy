<?php

namespace App\Models\V1;


use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Validation\Rule;

class Land extends BaseModel
{
    public const COUNT_PLANTS = 3;

    protected $fillable = [
        'user_id',
        'name',
        'plants',
        'main_image',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'plants' => 'array',
        'is_payed' => 'integer',
        'meat' => 'array',          // for compatibility with new version
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    /**
     * @return array
     */
    public static function getCreateRules(): array
    {
        return [
            'user_id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT],
            'name' => ['required', 'string'],
            'plants' => ['required', 'array', 'min:' . self::COUNT_PLANTS, 'max:' . self::COUNT_PLANTS],
            'plants.*.plant_type_id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:plant_types,id'],
            'plants.*.position' => ['required', 'integer', 'min:1', 'max:' . self::COUNT_PLANTS, 'distinct'],
            'plants.*.autowatering' => ['boolean'],
            'plants.*.data' => ['array']
        ];
    }

    /**
     * @param int $userId
     * @return array
     */
    public static function getUpdateRules(int $userId): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT,
                Rule::exists('lands', 'id')->where('user_id', $userId)],
            'name' => ['string']
        ];
    }

    /**
     * @param int $userId
     * @return array
     */
    public static function getAddPlantRules(int $userId): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT,
                Rule::exists('lands', 'id')->where('user_id', $userId)->where('is_payed', true)],
            'plant_type_id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:plant_types,id'],
            'position' => ['required', 'integer', 'min:1', 'max:' . self::COUNT_PLANTS, 'distinct'],
            'autowatering' => ['boolean'],
        ];
    }

    /**
     * @return array
     */
    public static function getByIdRules(): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT]
        ];
    }

    public function getMainImageAttribute($value)
    {
        return $value ? url($value) : $value;
    }

    /**
     * @return HasMany
     */
    public function plants(): HasMany
    {
        return $this->hasMany(Plant::class, 'land_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function subscription(): HasOne
    {
        return $this->hasOne(Subscription::class, 'land_id', 'id');
    }


    public static function boot(): void
    {
        parent::boot();
        self::creating(function ($model) {
            // for compatibility with new version
            $model->type = \App\Models\Land::TYPE_3_PLANTS;
            $model->meat = [];
        });
    }
}
