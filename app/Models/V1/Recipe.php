<?php

namespace App\Models\V1;


use Illuminate\Database\Eloquent\Relations\HasMany;

class Recipe extends BaseModel
{
    protected $fillable = [
        'image',
        'title',
        'plant_type_ids',
        'recipe'
    ];

    protected $casts = [
        'plant_type_ids' => 'array',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * @return array
     */
    public static function getAllRules(): array
    {
        return [
            'plant_type_id' => ['integer', 'max:' . self::MAX_INT, 'exists:plant_types,id'],
        ];
    }

    /**
     * @return array
     */
    public static function getCreateRules(): array
    {
        return [
            'title' => ['between:2,50', 'string'],
            'file' => ['required', 'mimetypes:image/*', 'file'],
            'plant_type_ids' => ['required', 'array', 'min:1', 'max:' . self::MAX_INT],
            'plant_type_ids.*' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:plant_types,id'],
            'recipe' => ['required', 'string'],
        ];
    }

    /**
     * @return array
     */
    public static function getUpdateRules(): array
    {
        return [
            'id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:recipes,id'],
            'title' => ['between:2,50', 'string'],
            'file' => ['required_without:fileLink', 'mimetypes:image/*', 'file'],
            'fileLink' => ['required_without:file', 'string'],
            'plant_type_ids' => ['required', 'array', 'min:1', 'max:' . self::MAX_INT],
            'plant_type_ids.*' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:plant_types,id'],
            'recipe' => ['required', 'string'],
        ];
    }

    /**
     * @return array
     */
    public static function getDeleteRules(): array
    {
        return [
            'id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:recipes,id']
        ];
    }

    public function getImageAttribute($value)
    {
        return $value ? url($value) : $value;
    }

    /**
     * @return HasMany
     */
    public function plantTypes(): HasMany
    {
        return $this->hasMany(RecipePlantTypes::class, 'recipe_id', 'id');
    }

}
