<?php

namespace App\Models\V1;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Validation\Rule;

class Payment extends BaseModel
{
    public const ITEM_TYPE_LAND = 'land';
    public const ITEM_TYPE_DELIVERY = 'delivery';

    protected $fillable = [
        'user_id',
        'item_id',
        'item_type',
        'amount',
        'currency',
        'type',
        'payment_system',
        'card',
        'card_type',
        'fee',
        'base_amount',
        'base_currency',
        'status',
        'transaction_id',
        'ps_response',
        'ps_callback'
    ];

    protected $casts = [
        'user_id' => 'integer',
        'item_id' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * @param int $userId
     * @return array
     */
    public static function getCreateRules(int $userId): array
    {
        return [
            'user_id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT],
            'land_id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT,
                Rule::exists('lands', 'id')->where(function ($query) use ($userId) {
                    $query->where(['user_id' => $userId, 'is_payed' => 0]);
                }),
            ],
            'debug' => ['boolean']
        ];
    }

    /**
     * @param int $userId
     * @return array
     */
    public static function getCreateDeliveryRules(int $userId): array
    {
        return [
            'user_id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT],
            'delivery_id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT,
                Rule::exists('delivery', 'id')->where(function ($query) use ($userId) {
                    $query->where(['user_id' => $userId, 'is_payed' => 0]);
                }),
            ],
            'debug' => ['boolean']
        ];
    }

    /**
     * @return array
     */
    public static function getLiqpayPaymentPageRules(): array
    {
        return [
            'url' => ['required', 'string'],
            'data' => ['required', 'string'],
            'signature' => ['required', 'string'],
        ];
    }

    /**
     * @return array
     */
    public static function getLiqpayCallbackRules(): array
    {
        return [
            'data' => ['required', 'string'],
            'signature' => ['required', 'string'],
        ];
    }

    public static function boot(): void
    {
        parent::boot();
        self::creating(function ($model) {
            $model->ps_callback = '[]';
        });
    }

    /**
     * @return HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}
