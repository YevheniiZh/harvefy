<?php

namespace App\Models\V1;


class PlantType extends BaseModel
{
    public static $disableAboutCultureMutator = false;

    protected $fillable = [
        'name',
        'image',
        'video',
        'price',
        'short_desc',
        'full_desc',
        'about_culture',
        'days_to_grow',
        'wither_in_days',
        'harvests_per_month',
        'out_of_stock',
        'teaser'
    ];

    protected $casts = [
        'price' => 'double',
        'days_to_grow' => 'integer',
        'harvests_per_month' => 'integer',
        'wither_in_days' => 'integer',
        'out_of_stock' => 'boolean',
        'teaser' => 'boolean',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * @return array
     */
    public static function getCreateRules(): array
    {
        return [
            'name' => ['required', 'between:2,50', 'string'],
            'file' => ['required', 'mimetypes:image/*', 'file'],
            'videoFile' => ['mimetypes:video/*', 'file'],
            'price' => ['sometimes', 'required', 'nullable', 'regex:/^\d+(\.\d{1,2})?$/'],
            'short_desc' => ['required', 'string'],
            'full_desc' => ['required', 'string'],
            'about_culture' => ['required', 'string'],
            'days_to_grow' => ['required', 'between:1,360', 'integer'],
            'wither_in_days' => ['sometimes', 'required', 'nullable', 'between:1,30', 'integer'],
            'out_of_stock' => ['required', 'boolean'],
            'teaser' => ['required', 'boolean'],
        ];
    }

    /**
     * @return array
     */
    public static function getUpdateRules(): array
    {
        return [
            'id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:plant_types,id'],
            'name' => ['required', 'between:2,50', 'string'],
            'file' => ['required_without:fileLink', 'mimetypes:image/*', 'file'],
            'fileLink' => ['required_without:file', 'string'],
            'videoFile' => ['mimetypes:video/*', 'file'],
            'price' => ['sometimes', 'required', 'nullable', 'regex:/^\d+(\.\d{1,2})?$/'],
            'short_desc' => ['required', 'string'],
            'full_desc' => ['required', 'string'],
            'about_culture' => ['required', 'string'],
            'days_to_grow' => ['required', 'between:1,360', 'integer'],
            'wither_in_days' => ['sometimes', 'required', 'nullable', 'between:1,30', 'integer'],
            'out_of_stock' => ['required', 'boolean'],
            'teaser' => ['required', 'boolean'],
        ];
    }

    /**
     * @return array
     */
    public static function getDeleteRules(): array
    {
        return [
            'id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:plant_types,id',
                'unique:plants,plant_type_id',
                'unique:recipe_plant_types,plant_type_id'
            ]
        ];
    }

    public function getImageAttribute($value)
    {
        return $value ? url($value) : $value;
    }

    public function getVideoAttribute($value)
    {
        return $value ? url($value) : $value;
    }

    public function getAboutCultureAttribute($value)
    {
        if (self::$disableAboutCultureMutator) {
            return $value;
        }
        $style = '<link href="' . env('APP_URL') . '/FontManrope500.css" rel="stylesheet"><style type="text/css">body{background-color:#EBEBEB; position: absolute; height: 36px; left: 24px; right: 24px; top: 72px; font-family: Manrope; font-style: normal; font-weight: 500; font-size: 36px; line-height: 100%; /* Black */ color: #000002;}* { font-family: Manrope !important; font-size: 36px !important; }</style>';
        return $style . $value;
    }
}
