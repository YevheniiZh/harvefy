<?php

namespace App\Models\V1;


class Notification extends BaseModel
{
    protected $fillable = [
        'user_id',
        'client_token_id',
        'active',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * @return array
     */
    public static function getCreateRules(): array
    {
        return [
            'user_id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT],
            'client_token_id' => ['required', 'string'],
        ];
    }

    /**
     * @return array
     */
    public static function getSendByAdminRules(): array
    {
        return [
            'toAllUsers' => ['required', 'integer'],
            'email' => ['required_if:toAllUsers,0', 'nullable', 'email', 'max:' . User::EMAIL_MAX_LENGTH, 'exists:users,email'],
            'notificationTitle' => ['required', 'string', 'min:1', 'max:125'],
            'notificationText' => ['required', 'string', 'min:1', 'max:125'],
        ];
    }

    /**
     * @return array
     */
    public static function getSendNotificationRules(): array
    {
        return [
            'client_token_id' => ['string'],
            'title' => ['string'],
            'body' => ['string'],
            'icon' => ['string'],
            'url' => ['string'],
        ];
    }

    /**
     * @return array
     */
    public static function getByIdRules(): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT]
        ];
    }
}
