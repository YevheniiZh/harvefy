<?php

namespace App\Models;


use DateTime;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Validation\Rule;

class Subscription extends BaseModel
{
    public const STATUS_ACTIVE = 1;
    public const STATUS_UNSUBSCRIBE_REQUESTED = 2;
    public const STATUS_UNSUBSCRIBED = 3;
    public const STATUS_UNSUBSCRIBED_DUE_CANT_CHARGE = 4;

    public const STATUSES = [
        self::STATUS_ACTIVE => 'ACTIVE',
        self::STATUS_UNSUBSCRIBE_REQUESTED => 'UNSUBSCRIBE_REQUESTED',
        self::STATUS_UNSUBSCRIBED => 'UNSUBSCRIBED',
        self::STATUS_UNSUBSCRIBED_DUE_CANT_CHARGE => 'UNSUBSCRIBED_DUE_CANT_CHARGE',
    ];

    public const TYPE_3_PLANTS = 1;
    public const TYPE_2_PLANTS = 2;
    //public const TYPE_ANGUS_MEAT = 3;
    //public const TYPE_SIMENTAL_MEAT = 4;

    public const TYPES_CONFIG = [
        self::TYPE_3_PLANTS => Config::CONFIG_SUBSCRIPTION_PRICE,
        self::TYPE_2_PLANTS => Config::CONFIG_SUBSCRIPTION_2_PLANTS_PRICE,
        //self::TYPE_ANGUS_MEAT => Config::CONFIG_SUBSCRIPTION_ANGUS_PRICE,
        //self::TYPE_SIMENTAL_MEAT => Config::CONFIG_SUBSCRIPTION_SIMENTAL_PRICE,
    ];

    public const HANDLED_BY_SCHEDULE_STATUSES = [
        self::STATUS_UNSUBSCRIBE_REQUESTED => [
            self::STATUS_UNSUBSCRIBED,
        ],
        self::STATUS_ACTIVE => [
            self::STATUS_UNSUBSCRIBED_DUE_CANT_CHARGE,
        ]
    ];

    protected $fillable = [
        'user_id',
        'land_id',
        'status',
        'next_payment',
        'canceled',
        'cancel_date',
        'ps_response',
        'payment_id',
        'is_debug',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'land_id' => 'integer',
        'status' => 'integer',
        'next_payment' => 'timestamp',
        'canceled' => 'integer',
        'cancel_date' => 'timestamp',
        'payment_id' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * @return array
     */
    public static function getByIdRules(): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT]
        ];
    }

    /**
     * @param int $userId
     * @return array
     */
    public static function getUnsubscribeRules(int $userId): array
    {
        return [
            'id' => ['required', 'integer', 'min:1', 'max:' . self::MAX_INT,
                Rule::exists('subscriptions', 'id')->where('user_id', $userId)/*->where('status', self::STATUS_ACTIVE)*/
            ]
        ];
    }

    /**
     * @return HasOne
     */
    public function land(): HasOne
    {
        return $this->hasOne(Land::class, 'id', 'land_id');
    }

    /**
     * @return HasOne
     */
    public function payment(): HasOne
    {
        return $this->hasOne(Payment::class, 'id', 'payment_id');
    }

    /**
     * @return HasManyThrough
     */
    public function plant(): HasManyThrough
    {
        return $this->hasManyThrough(Plant::class, Land::class, 'id', 'plant_type_id');
    }

    public static function boot(): void
    {
        parent::boot();
        self::creating(function ($model) {
            $model->ps_response = '[]';
        });
    }

    /**
     * @return HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getRenewAttribute(): string
    {
        $created_at = new DateTime(date('Y-m-d', strtotime($this->created_at)));
        $interval = (new DateTime(date('Y-m-d', strtotime($this->next_payment))))->diff($created_at);
        $days = $interval->format('%m');
        return $this->attributes['renew'] = $days;
    }
}
