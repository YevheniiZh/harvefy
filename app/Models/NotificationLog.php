<?php

namespace App\Models;


class NotificationLog extends BaseModel
{
    protected $table = 'notifications_log';

    protected $fillable = [
        'user_id',
        'plant_id',
        'title',
        'status',
        'response',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'plant_id' => 'integer',
        'status' => 'boolean',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];
}
