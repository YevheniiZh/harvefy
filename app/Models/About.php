<?php

namespace App\Models;


use Illuminate\Validation\Rule;

class About extends BaseModel
{
    public static $disableAboutCultureMutator = false;

    protected $table = 'about';

    protected $fillable = [
        'type_id',
        'image',
        'description'
    ];

    protected $casts = [
        'type_id' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * @return array
     */
    public static function getCreateRules(): array
    {
        return [
            'type_id' => ['required', 'integer', Rule::in(array_keys(LandImage::LAND_TYPES))],
            'file' => ['required', 'mimetypes:image/*', 'file'],
            'description' => ['required', 'string'],
        ];
    }

    /**
     * @return array
     */
    public static function getUpdateRules(): array
    {
        return [
            'id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:about,id'],
            'type_id' => ['required', 'integer', Rule::in(array_keys(LandImage::LAND_TYPES))],
            'file' => ['required_without:fileLink', 'mimetypes:image/*', 'file'],
            'fileLink' => ['required_without:file', 'string'],
            'description' => ['required', 'string'],
        ];
    }

    /**
     * @return array
     */
    public static function getDeleteRules(): array
    {
        return [
            'id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:about,id']
        ];
    }

    public function getImageAttribute($value)
    {
        return $value ? url($value) : $value;
    }

    public function getDescriptionAttribute($value)
    {
        if (self::$disableAboutCultureMutator) {
            return $value;
        }
        $img = '<img src="' . $this->image . '">';
        $style = '<link href="' . env('APP_URL') . '/FontManrope500.css" rel="stylesheet"><style type="text/css">body {background-color:#EBEBEB; margin: 0; font-family: Manrope; font-style: normal; font-weight: 500; font-size: 36px; line-height: 100%; /* Black */ color: #000002;} * { font-family: Manrope !important; } img {width: 100%;} .wrap {position: absolute; height: 36px; margin-left: 24px; margin-right: 24px; /*top: 72px;*/}</style>';
        return $style . $img . '<div class="wrap">' . $value . '</div>';
    }
}
