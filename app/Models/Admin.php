<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'email',
        'phone',
        'promocode',
        'password',
        'is_partner',
        'reset_pass_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return array
     */
    public static function getCreateRules(): array
    {
        return [
            'name' => ['required', 'between:2,50', 'string'],
            'surname' => ['required', 'between:2,50', 'string'],
            'email' => ['required', 'email:rfc,dns', 'unique:admins,email'],
            'phone' => ['string', 'nullable'],
            'promocode' => ['string', 'nullable'],
        ];
    }

    /**
     * @return array
     */
    public static function getChangePasswordRules(): array
    {
        return [
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }

    /**
     * @return array
     */
    public static function getRecoverPasswordRules(): array
    {
        return [
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'token' => ['required', 'string'],
        ];
    }

    /**
     * @return array
     */
    public static function getUpdateRules(): array
    {
        return [
            'id' => ['required', 'integer', 'max:' . BaseModel::MAX_INT],
            'name' => ['required', 'between:2,50', 'string'],
            'surname' => ['required', 'between:2,50', 'string'],
            'phone' => ['string', 'nullable'],
            'promocode' => ['string', 'nullable'],
        ];
    }

    /**
     * @return HasMany
     */
    public function admin_streets(): HasMany
    {
        return $this->hasMany(AdminStreets::class, 'admin_id', 'id');
    }

}
