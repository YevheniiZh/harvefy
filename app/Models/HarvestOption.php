<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;

class HarvestOption extends BaseModel
{
    //protected $hidden = ['city_ids'];

    protected $fillable = [
        'name',
        'image',
        'description',
        'city_ids',
        'meat_type_id',
        'active',
    ];

    protected $casts = [
        'name' => 'string',
        'description' => 'string',
        'city_ids' => 'array',
        'meat_type_id' => 'int',
        'active' => 'int',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * @return array
     */
    public static function getAllRules(): array
    {
        return [
            'meat_type_id' => ['integer', 'max:' . self::MAX_INT, 'exists:meat_types,id'],
        ];
    }

    /**
     * @return array
     */
    public static function getCreateRules(): array
    {
        return [
            'name' => ['required', 'between:2,50', 'string'],
            'file' => ['required', 'mimetypes:image/*', 'file'],
            'description' => ['required', 'string'],
            'city_ids' => ['required', 'array', 'min:1', 'max:' . self::MAX_INT],
            'city_ids.*' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:cities,id'],
            'meat_type_id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:meat_types,id'],
        ];
    }

    /**
     * @return array
     */
    public static function getUpdateRules(): array
    {
        return [
            'id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:harvest_options,id'],
            'name' => ['required', 'between:2,50', 'string'],
            'file' => ['required_without:fileLink', 'mimetypes:image/*', 'file'],
            'description' => ['string'],
            'city_ids' => ['required', 'array', 'min:1', 'max:' . self::MAX_INT],
            'city_ids.*' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:cities,id'],
            'meat_type_id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:meat_types,id'],
        ];
    }

    /**
     * @return array
     */
    public static function getDeleteRules(): array
    {
        return [
            'id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:harvest_options,id',
                'unique:meat,harvest_option_id'
            ]
        ];
    }

    /**
     * @return HasMany
     */
    public function cities(): HasMany
    {
        return $this->hasMany(HarvestOptionCities::class, 'harvest_option_id', 'id');
    }

    public static function boot(): void
    {
        parent::boot();
        self::creating(function ($model) {
            $model->city_ids = array_map('intval', $model->city_ids);
        });
        self::updating(function ($model) {
            $model->city_ids = array_map('intval', $model->city_ids);
        });
    }

    public function getImageAttribute($value)
    {
        return $value ? url($value) : $value;
    }

    public function getCityIdsAttribute($value)
    {
        return array_map('intval', json_decode($value, true));
    }
}
