<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Relations\HasOne;

class AdminStreets extends BaseModel
{
    protected $table = 'admin_streets';

    protected $fillable = [
        'admin_id',
        'street_id',
    ];

    protected $casts = [
        'admin_id' => 'integer',
        'street_id' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * @return HasOne
     */
    public function street(): HasOne
    {
        return $this->hasOne(Street::class, 'id', 'street_id');
    }
}
