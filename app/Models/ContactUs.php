<?php

namespace App\Models;


class ContactUs extends BaseModel
{
    protected $fillable = [
        'user_id',
        'title',
        'details',
        'status'
    ];

    protected $casts = [
        'user_id' => 'integer',
        'status' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * @return array
     */
    public static function getCreateRules(): array
    {
        return [
            'user_id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:users,id'],
            'title' => ['required', 'string', 'min:1', 'max:100'],
            'details' => ['required', 'string', 'min:1', 'max:1000'],
        ];
    }

    /**
     * @return array
     */
    public static function getUpdateRules(): array
    {
        return [
            'id' => ['required', 'integer', 'max:' . self::MAX_INT, 'exists:contact_us,id']
        ];
    }
}
