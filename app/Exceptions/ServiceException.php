<?php


namespace App\Exceptions;

/**
 * Class ServiceException
 */
class ServiceException extends \Exception
{
    private $exceptionData = [];

    /**
     * ServiceException constructor.
     * @param int $code
     * @param string $message
     * @param array $exceptionData
     * @param \Exception|null $previous
     */
    public function __construct(int $code = 0, string $message = '', array $exceptionData = [], \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->setExceptionData($exceptionData);
    }

    /**
     * @param $exceptionData
     */
    public function setExceptionData($exceptionData): void
    {
        $this->exceptionData = $exceptionData;
    }

    /**
     * @return array
     */
    public function getExceptionData(): array
    {
        return $this->exceptionData;
    }
}
