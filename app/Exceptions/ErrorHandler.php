<?php

namespace App\Exceptions;

use App\Helpers\DbHelper;
use App\Services\ResponseService;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\QueryException;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use NunoMaduro\Collision\Adapters\Laravel\ExceptionHandler;
use Symfony\Component\ErrorHandler\Error\FatalError;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class ErrorHandler extends Handler
{

    private $dbHelper;

    public function __construct(DbHelper $dbHelper)
    {

        $this->dbHelper = $dbHelper;
    }


    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Throwable $exception
     * @return void
     */
    public function report(Throwable $exception)
    {

        Log::critical(implode(' ', [
            'message ' . $exception->getMessage(),
            'in file ' . $exception->getFile(),
            'on line ' . $exception->getLine()
        ]));
//        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Throwable $exception
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof AuthorizationException){
            return redirect(route('home'));
        }
        if ($exception instanceof NotFoundHttpException) {
            if (strpos($request->getPathInfo(), 'api') !== false) {
                return ResponseService::notFound('Page not found');
            }
            return redirect()->route('landing');
        }

        if ($exception instanceof MethodNotAllowedHttpException) {
            $message = empty($exception->getMessage()) ? 'Method Not Allowed' : $exception->getMessage();

            return ResponseService::notAllowed($message);
        }

        if ($exception instanceof QueryException) {
            if ($exception->getCode() === DbHelper::FOREIGN_KEY_CODE) {

                return $this->dbHelper->getResponseForeignConflict($exception->getMessage());
            }
            if ($exception->getCode() === DbHelper::DUPLICATE_CODE) {

                return $this->dbHelper->getResponseExistsEntity('Entity', $exception->getMessage());
            }

            return ResponseService::dbError($exception->getMessage());
        }

        if ($exception instanceof ValidationException) {
            return ResponseService::validationError('Error while validation.', $exception->errors());
        }

        if ($exception instanceof FatalError && strpos($exception->getMessage(), 'Allowed memory size') !== false) {
            ob_clean();
            return ResponseService::error( 'The file may not be greater then 200000 kilobytes');
        }

        if ($exception instanceof ServiceException) {
            $data = $exception->getExceptionData()['data'] ?? null;
            $errors = $exception->getExceptionData()['errors'] ?? [];
            return ResponseService::response($data, $errors, (int)$exception->getCode(), $exception->getMessage());
        }

        if ($exception instanceof AuthenticationException) {
            if (strpos($request->getPathInfo(), 'api') !== false) {
                return ResponseService::unauthorized();
            }
            return redirect()->route('landing');
        }

        if ($exception instanceof Exception) {
            $message = implode(' ', [
                'message ' . $exception->getMessage(),
                'in file ' . $exception->getFile(),
                'on line ' . $exception->getLine()
            ]);
            return ResponseService::systemError($message);
        }

        return parent::render($request, $exception);
    }
}
