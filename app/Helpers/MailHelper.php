<?php


namespace App\Helpers;

use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailHelper
{
    /**
     * @param string $toUserEmail
     * @param Mailable $mailClass
     * @return string
     */
    public static function sendEmail(string $toUserEmail, Mailable $mailClass): string
    {
        try {
            Mail::to($toUserEmail)->send($mailClass);
        } catch(\Exception $e) {
            Log::error('Error sendEmail: ' . $e->getMessage());
            Mail::to($toUserEmail)->send($mailClass);
        }
        return 'Email send to ' . $toUserEmail;
    }

    /**
     * @param string $toUserEmail
     * @param string $subject
     * @param string $view
     * @param array $data
     * @return void
     */
    public static function sendEmailThroughSideServer(string $toUserEmail, string $subject, string $view, array $data): void
    {
        $response = Http::post('https://harvefy.tk/api/send_mail', [
            'email' => $toUserEmail,
            'subject' => $subject,
            'view' => $view,
            'data' => $data,
        ]);
        Log::error('sendEmailThroughSideServer: ' . json_encode($response));
    }

}
