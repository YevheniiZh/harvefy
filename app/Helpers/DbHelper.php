<?php


namespace App\Helpers;

use Illuminate\Support\Arr;
use App\Services\ResponseService;
use Illuminate\Http\JsonResponse;

class DbHelper
{
    /**
     *
     * const for code db if ($atribute)=($value) already exists
     */
    public const FOREIGN_KEY_CODE = '23503';
    public const DUPLICATE_CODE = '23505';

    /**
     * formatted Response for Foreign Key Conflict
     *
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function getResponseForeignConflict($message): JsonResponse
    {
        preg_match_all('/(?<=[\'"])\w+(?=[\'"])/', $message, $attribute);
        $entityName = Arr::get($attribute, "0.0");
        $entityUsed = Arr::get($attribute, "0.2");

        return ResponseService::foreignKeyConflict($entityName, $entityUsed);
    }

    /**
     *
     * Formatted Response for Exists Entity
     * @param $entityName
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function getResponseExistsEntity($entityName, $message): JsonResponse
    {
        preg_match('#\((.*?)\)#', $message, $attribute);
        preg_match('#=\((.*?)\)#', $message, $value);
        $attribute = Arr::get($attribute, 1);
        $value = Arr::get($value, 1);

        return ResponseService::existEntity($entityName, [$attribute => $value]);
    }


}
