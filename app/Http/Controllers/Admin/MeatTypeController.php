<?php

namespace App\Http\Controllers\Admin;

use App\Services\MeatTypeService;
use App\Services\ResponseService;
use App\Services\Service;
use Illuminate\Http\Request;
use App\Models\MeatType;
use App\Http\Controllers\Api\Controller;
use Illuminate\Validation\ValidationException;

class MeatTypeController extends Controller
{
    /**
     * @var MeatType
     */
    private $model;

    public function index()
    {
        MeatType::$disableAboutMutator = true;
        $items = MeatType::selectRaw('*')->withCasts([
            'created_at' => 'datetime',
            'updated_at' => 'datetime'
        ])->orderBy('id', 'DESC')->get();

        return view('meat_types', ['items' => $items, 'itemsJs' => Service::indexById($items->toArray())]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function add(Request $request)
    {
        try {
            $request->replace(array_filter($request->all()));
            $request->merge(['out_of_stock' => $request->get('out_of_stock') === 'on']);
            $request->merge(['teaser' => $request->get('teaser') === 'on']);

            $this->validate($request, MeatType::getCreateRules(),
                ['file.required' => 'The image field is required.']);
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }

        if ($file = $request->file('file')) {
            $path = $file->store('image', 'uploads');
            $request->request->add(['image' => $path]);
        }
        if ($videoFile = $request->file('videoFile')) {
            $videoPath = $videoFile->store('video', 'uploads');
            $request->request->add(['video' => $videoPath]);
        }
        $harvestsPerMonth = floor(30 / $request->get('days_to_grow'));
        $request->request->add(['harvests_per_month' => $harvestsPerMonth]);
        $this->model = new MeatType();
        $this->model->fill($request->all());

        if (!$this->model->save()) {
            return ResponseService::validationError('plant', []);
        }
        return ResponseService::ok('Plant type added'); // ['items' => MeatType::all()->sortByDesc('id')]
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function update(Request $request, int $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $request->replace(array_filter($request->all()));
            $request->merge(['out_of_stock' => $request->get('out_of_stock') === 'on']);
            $request->merge(['teaser' => $request->get('teaser') === 'on']);

            $this->validate($request, MeatType::getUpdateRules(),
                ['file.required' => 'The image field is required.']);
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }

        if ($file = $request->file('file')) {
            $path = $file->store('image', 'uploads');
            $request->request->add(['image' => $path]);
        }
        if ($videoFile = $request->file('videoFile')) {
            $videoPath = $videoFile->store('video', 'uploads');
            $request->request->add(['video' => $videoPath]);
        }
        $harvestsPerMonth = floor(30 / $request->get('days_to_grow'));
        $request->request->add(['harvests_per_month' => $harvestsPerMonth]);
        $model = MeatTypeService::update($id, $request->all());
        if (!$model) {
            return ResponseService::validationError('plant', []);
        }
        return ResponseService::ok('Plant type updated');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function delete(Request $request, int $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $this->validate($request, MeatType::getDeleteRules(),
                ['id.unique' => 'Can\'t delete, plant type used in plants']);
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->errors()['id'][0] ?? $e->getMessage());
        }
        MeatTypeService::delete($id);

        return ResponseService::ok('Plant type deleted');
    }
}
