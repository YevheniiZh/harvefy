<?php

namespace App\Http\Controllers\Admin;

use App\Models\ImageType;
use App\Models\MeatType;
use App\Services\ResponseService;
use App\Services\Service;
use Illuminate\Http\Request;
use App\Models\LandImage;
use App\Http\Controllers\Api\Controller;
use Illuminate\Validation\ValidationException;

class LandImageController extends Controller
{
    /**
     * @var LandImage
     */
    private $model;

    public function index()
    {
        $items = LandImage::selectRaw('*')->orderBy('land_type_id', 'ASC')->orderBy('id', 'DESC')->get();

        return view('images', [
            'items' => $items,
            'image_types' => ImageType::all()->pluck('name', 'id'),
            'meat_types' => Service::indexById(MeatType::all()->toArray()),
            //'plant_types' => LandImage::LAND_TYPES
        ]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(int $id)
    {
        $post = LandImage::findOrFail($id);
        return view('admin.viewPlant', ['post' => $post]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function add(Request $request)
    {
        try {
            $this->validate($request, LandImage::getCreateRules(),
                ['required_unless' => 'The file name is required for this type of image']
            );
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }

        if ($file = $request->file('file')) {
            if ($filename = $request->get('file_name')) {
                $path = $file->storeAs('image', $filename . '.' . $file->extension(), 'uploads');
            } else {
                $path = $file->store('image', 'uploads');
            }
            $request->request->add(['image' => $path]);
        }
        $this->model = new LandImage();
        $this->model->fill($request->all());

        if(!$this->model->save()){
            return ResponseService::validationError('plant', []);
        }
        return ResponseService::ok('Land image added'); // ['items' => LandImage::all()->sortByDesc('id')]
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function addType(Request $request)
    {
        try {
            $this->validate($request, ImageType::getCreateRules());
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }

        $this->model = new ImageType();
        $this->model->fill($request->all());

        if(!$this->model->save()){
            return ResponseService::validationError('image type', []);
        }
        return ResponseService::ok('Image type added');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function delete(Request $request, int $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $this->validate($request, LandImage::getDeleteRules(),
                ['id.unique' => 'Can\'t delete, plant type used in plants']);
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->errors()['id'][0] ?? $e->getMessage());
        }
        LandImage::where('id', $id)->delete();

        return ResponseService::ok('Plant type deleted');
    }

}
