<?php

namespace App\Http\Controllers\Admin;

use App\Casts\Timezone;
use App\Helpers\MailHelper;
use App\Http\Controllers\Api\Controller;
use App\Mail\Plain;
use App\Models\Admin;
use App\Services\PartnerService;
use App\Services\ResponseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AdminController extends Controller
{

    public function index()
    {
        $admin = Admin::where('is_partner', false)->withCasts([
            'created_at' => Timezone::class,
        ])->get();
        return view('admins', ['users' => $admin]);
    }

    public function showChangePassword()
    {
        return view('vendor/adminlte/auth/passwords/change');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function changePassword(Request $request): JsonResponse
    {
        try {
            $attributes = $this->validate($request, Admin::getChangePasswordRules());
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }

        $model = PartnerService::get(Auth::id());
        if(!$model) {
            return ResponseService::validationError('Partner not found', []);
        }
        PartnerService::update($model, $attributes);
        $data = new \stdClass();
        $data->subject = 'Смена пароля Harvefy';
        $data->text = 'Ваш пароль изменён';
        MailHelper::sendEmail($model->email, new Plain($data));
        return ResponseService::ok('Password changed');
    }

}
