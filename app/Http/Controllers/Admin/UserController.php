<?php

namespace App\Http\Controllers\Admin;

use App\Casts\Timezone;
use App\Models\Admin;
use App\Models\User;
use App\Services\PartnerService;
use App\Services\Service;

class UserController extends PartnerController
{
    public function users()
    {
        $query = User::selectRaw('*')->with('cityModel')->withCasts([
            'created_at' => Timezone::class,
            'updated_at' => Timezone::class
        ]);
        if ($this->is_partner) {
            $query->where('partner_id', $this->user_id);
        } else {
            $admins = Service::indexById(PartnerService::getAllPartners(true));
        }
        $users = $query->get();
        return view('users', [
            'users' => $users->reverse(),
            'partners' => $admins ?? [],
            'isAdmin' => (int) $this->isAdmin
        ]);
    }

}
