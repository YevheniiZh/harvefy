<?php

namespace App\Http\Controllers\Admin;

use App\Casts\Timezone;
use App\Models\Admin;
use App\Models\Land;
use App\Models\Payment;
use App\Models\Plant;
use App\Services\PartnerService;
use App\Services\Service;
use WayForPay\SDK\Credential\AccountSecretTestCredential;
use WayForPay\SDK\Wizard\TransactionListWizard;

class PaymentController extends PartnerController
{
    /**
     * @var Land
     */
    private $model;

    public function index()
    {
        $query = Payment::selectRaw('*')->with('user')->withCasts([
            'created_at' => Timezone::class,
            'updated_at' => Timezone::class,
            'ps_response' => 'string',
        ])->orderBy('id', 'DESC');
        $query = $this->filterQuery($query);
        $results = $query->get();

        return view('payments', [
            'transactions' => $results,
            'partners' => Service::indexById(PartnerService::getAllPartners(true)),
            'isAdmin' => (int) $this->isAdmin
        ]);
    }

    public function transactions()
    {
        $credential = new AccountSecretTestCredential();
//$credential = new AccountSecretCredential('account', 'secret');

        $response = TransactionListWizard::get($credential)
            ->setDateBegin(new \DateTime('-31 day'))
            ->setDateEnd(new \DateTime())
            ->getRequest()
            ->send();

        $transactions = [];
        foreach ($response->getTransactionList() as $transaction) {
            array_unshift($transactions, [
                'date' => $transaction->getCreatedDate()->format('Y-m-d H:i:s'),
                'type' => $transaction->getType(),
                'status' => $transaction->getStatus(),
                'amount' => $transaction->getAmount() . ' ' . $transaction->getCurrency(),
                'id' => $transaction->getOrderReference(),
            ]);
        }
        return view('transactions', ['transactions' => $transactions]);
    }

    public function show($id)
    {
        $post = Plant::findOrFail($id);
        $comments = Comment::where('post_id', '=', $id)->get();
        return view('admin.viewPlant', ['post' => $post, 'comments' => $comments]);
    }
}
