<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\MailHelper;
use App\Http\Controllers\Api\Controller;
use App\Mail\Plain;
use App\Providers\RouteServiceProvider;
use App\Models\Admin;
use App\Services\PartnerService;
use App\Services\ResponseService;
use App\Services\Service;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use stdClass;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    protected function sendRecoverPassword(Request $request): JsonResponse
    {
        $email = $request->get('email');
        $model = Admin::where('email', $email)->first();
        if (!$model) {
            return ResponseService::validationError('email', ['Email not found']);
        }
        $random = Service::generateRandomString();
        $hash = Hash::make($random);
        $model->reset_pass_token = $hash;
        $model->save();
        $link = route('password_recover', ['token' => $hash]);
        $data = new stdClass();
        $data->subject = 'Сброс пароля Harvefy';
        $data->text = 'Ссылка для сброса пароля: <a href="' . $link . '">' . $link . '</a>';
        MailHelper::sendEmail($email, new Plain($data));
        return ResponseService::ok('Емейл отправлен');
    }

    /**
     * @param Request $request
     * @return Factory|RedirectResponse|Redirector|View
     */
    public function showRecoverPassword(Request $request)
    {
        $model = Admin::where('reset_pass_token', $request->get('token'))->first();
        if (!$model) {
            return redirect(route('home'));
        }
        return view('vendor/adminlte/auth/passwords/recover');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    protected function setNewPassword(Request $request): JsonResponse
    {
        try {
            $attributes = $this->validate($request, Admin::getChangePasswordRules());
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }
        $model = Admin::where('reset_pass_token', $request->get('token'))->first();
        if (!$model) {
            return ResponseService::validationError('user', ['User not found']);
        }
        $attributes['reset_pass_token'] = null;
        PartnerService::update($model, $attributes);
        $data = new stdClass();
        $data->subject = 'Смена пароля Harvefy';
        $data->text = 'Ваш пароль изменён';
        MailHelper::sendEmail($model->email, new Plain($data));
        return ResponseService::ok('Password changed');
    }
}
