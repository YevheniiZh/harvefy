<?php

namespace App\Http\Controllers\Admin;

use App\Casts\Timezone;
use App\Models\PlantType;
use App\Services\RecipePlantTypesService;
use App\Services\RecipeService;
use App\Services\ResponseService;
use App\Services\Service;
use Illuminate\Http\Request;
use App\Models\Recipe;
use App\Http\Controllers\Api\Controller;
use Illuminate\Validation\ValidationException;

class RecipeController extends Controller
{
    /**
     * @var Recipe
     */
    private $model;

    public function index()
    {
        $items = Recipe::selectRaw('*')->withCasts([
            'created_at' => Timezone::class,
            'updated_at' => Timezone::class
        ])->orderBy('id', 'DESC')->get();

        $plantTypes = Service::indexById(PlantType::all(['id', 'name'])->toArray());

        return view('recipes', ['items' => $items, 'itemsJs' => Service::indexById($items->toArray()), 'plant_types' => $plantTypes]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(int $id)
    {
        $post = Recipe::findOrFail($id);
        return view('admin.viewPlant', ['post' => $post]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function add(Request $request)
    {
        try {
            $this->validate($request, Recipe::getCreateRules());
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }

        if ($file = $request->file('file')) {
            $path = $file->store('image', 'uploads');
            $request->request->add(['image' => $path]);
        }
        $this->model = new Recipe();
        $this->model->fill($request->all());

        if(!$this->model->save()){
            return ResponseService::validationError('plant', []);
        }
        foreach ($request->plant_type_ids as $plant_type_id) {
            app(RecipePlantTypesService::class)->create([
                'recipe_id' => $this->model->getAttribute('id'),
                'plant_type_id' => (int) $plant_type_id,
            ]);
        }
        return ResponseService::ok('Recipe added'); // ['items' => Recipe::all()->sortByDesc('id')]
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function update(Request $request, int $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $this->validate($request, Recipe::getUpdateRules());
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }

        if ($file = $request->file('file')) {
            $path = $file->store('image', 'uploads');
            $request->request->add(['image' => $path]);
        }
        $model = RecipeService::update($id, $request->all());
        if(!$model) {
            return ResponseService::validationError('plant', []);
        }
        RecipePlantTypesService::delete($id);
        foreach ($request->plant_type_ids as $plant_type_id) {
            app(RecipePlantTypesService::class)->create([
                'recipe_id' => $id,
                'plant_type_id' => (int) $plant_type_id,
            ]);
        }
        return ResponseService::ok('Recipe added'); // ['items' => Recipe::all()->sortByDesc('id')]
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function delete(Request $request, int $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $this->validate($request, Recipe::getDeleteRules());
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }
        RecipeService::delete($id);
        RecipePlantTypesService::delete($id);

        return ResponseService::ok('Recipe deleted');
    }
}
