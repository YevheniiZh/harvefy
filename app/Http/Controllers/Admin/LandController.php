<?php

namespace App\Http\Controllers\Admin;

use App\Casts\Timezone;
use App\Models\Admin;
use App\Models\Land;
use App\Models\Plant;
use App\Services\LandService;
use App\Services\PartnerService;
use App\Services\Service;

class LandController extends PartnerController
{
    /**
     * @var Land
     */
    private $model;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        $query = Land::selectRaw('*')->with('user')->withCasts([
            'created_at' => Timezone::class,
            'updated_at' => Timezone::class
        ]);
        $query = $this->filterQuery($query);
        $results = $query->orderBy('id', 'DESC')->get()->toArray();

        foreach ($results as $key => $result) {
            $results[$key] = LandService::mapProduct($result);
        }
        return view('lands', [
            'lands' => $results,
            'types' => Land::LAND_TYPES,
            'partners' => Service::indexById(PartnerService::getAllPartners(true)),
            'isAdmin' => (int) $this->isAdmin
        ]);
    }

    public function show($id)
    {
        $post = Plant::findOrFail($id);
        $comments = Comment::where('post_id', '=', $id)->get();
        return view('admin.viewPlant', ['post' => $post, 'comments' => $comments]);
    }
}
