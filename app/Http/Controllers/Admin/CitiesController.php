<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Services\CityService;
use App\Services\ResponseService;
use App\Services\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\Controller;
use Illuminate\Validation\ValidationException;

class CitiesController extends Controller
{
    private $model;

    public function index()
    {
        $items = City::selectRaw('*')->withCasts([
            'created_at' => 'datetime',
            'updated_at' => 'datetime'
        ])->orderBy('id', 'DESC')->get();

        return view('cities', ['items' => $items, 'itemsJs' => Service::indexById($items->toArray())]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function add(Request $request)
    {
        try {
            $request->replace(array_filter($request->all()));
            $request->merge(['plants_available' => $request->get('plants_available') === 'on']);
            $request->merge(['meat_available' => $request->get('meat_available') === 'on']);

            $this->validate($request, City::getCreateRules());
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }

        $this->model = new City();
        $this->model->fill($request->all());
        if (!$this->model->save()) {
            return ResponseService::validationError('city', []);
        }
        return ResponseService::ok('City added');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function update(Request $request, int $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $request->replace(array_filter($request->all()));
            $request->merge(['plants_available' => $request->get('plants_available') === 'on']);
            $request->merge(['meat_available' => $request->get('meat_available') === 'on']);

            $this->validate($request, City::getUpdateRules());
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }

        $model = CityService::update($id, $request->all());
        if (!$model) {
            return ResponseService::validationError('city', []);
        }
        return ResponseService::ok('City updated');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function delete(Request $request, int $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $this->validate($request, City::getDeleteRules(),
                ['id.unique' => 'Can\'t delete, city used by user']);
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->errors()['id'][0] ?? $e->getMessage());
        }
        CityService::delete($id);

        return ResponseService::ok('City deleted');
    }
}
