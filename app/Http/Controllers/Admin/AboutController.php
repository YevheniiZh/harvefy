<?php

namespace App\Http\Controllers\Admin;

use App\Models\LandImage;
use App\Services\AboutService;
use App\Services\ResponseService;
use App\Services\Service;
use Illuminate\Http\Request;
use App\Models\About;
use App\Http\Controllers\Api\Controller;
use Illuminate\Validation\ValidationException;

class AboutController extends Controller
{
    /**
     * @var About
     */
    private $model;

    public function index()
    {
        About::$disableAboutCultureMutator = true;
        $items = About::selectRaw('*')->withCasts([
            'created_at' => 'datetime',
            'updated_at' => 'datetime'
        ])->orderBy('id', 'DESC')->get();

        return view('about', [
            'items' => $items,
            'itemsJs' => Service::indexById($items->toArray()),
            'types' => LandImage::LAND_TYPES
        ]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(int $id)
    {
        $post = About::findOrFail($id);
        $comments = Comment::where('post_id', '=', $id)->get();
        return view('admin.viewPlant', ['post' => $post, 'comments' => $comments]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function add(Request $request)
    {
        try {
            $request->replace(array_filter($request->all()));

            $this->validate($request, About::getCreateRules(),
                ['file.required' => 'The image field is required.']);
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }

        if ($file = $request->file('file')) {
            $path = $file->store('image', 'uploads');
            $request->request->add(['image' => $path]);
        }
        $this->model = new About();
        $this->model->fill($request->all());

        if (!$this->model->save()) {
            return ResponseService::validationError('plant', []);
        }
        return ResponseService::ok('About added'); // ['items' => About::all()->sortByDesc('id')]
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function update(Request $request, int $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $request->replace(array_filter($request->all()));

            $this->validate($request, About::getUpdateRules(),
                ['file.required' => 'The image field is required.']);
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }

        if ($file = $request->file('file')) {
            $path = $file->store('image', 'uploads');
            $request->request->add(['image' => $path]);
        }
        $model = AboutService::update($id, $request->all());
        if (!$model) {
            return ResponseService::validationError('plant', []);
        }
        return ResponseService::ok('About updated');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function delete(Request $request, int $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $this->validate($request, About::getDeleteRules(),
                ['id.unique' => 'Can\'t delete, plant type used in plants']);
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->errors()['id'][0] ?? $e->getMessage());
        }
        AboutService::delete($id);

        return ResponseService::ok('About deleted');
    }
}
