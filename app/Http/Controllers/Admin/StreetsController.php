<?php

namespace App\Http\Controllers\Admin;

use App\Models\Street;
use App\Services\AdminStreetsService;
use App\Services\PartnerService;
use App\Services\StreetService;
use App\Services\ResponseService;
use App\Services\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\Controller;
use Illuminate\Validation\ValidationException;

class StreetsController extends Controller
{
    private $model;

    public function index()
    {
        $items = Street::selectRaw('*')->with('admin')->withCasts([
            'created_at' => 'datetime',
            'updated_at' => 'datetime'
        ])->orderBy('id', 'DESC')->get();

        $admins = Service::indexById(PartnerService::getAllPartners(true));

        return view('streets', [
            'items' => $items,
            'itemsJs' => Service::indexById($items->toArray()),
            'admins' => $admins
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function add(Request $request)
    {
        try {
            $request->replace(array_filter($request->all()));

            $attributes = $this->validate($request, Street::getCreateRules());
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }

        $model = new Street();
        $attributes['city_id'] = 1; // Kyiv
        $model->fill($attributes);
        if (!$model->save()) {
            return ResponseService::validationError('street', []);
        }

        app(AdminStreetsService::class)->create([
            'admin_id' => $attributes['admin_id'],
            'street_id' => $model->getAttribute('id'),
        ]);
        return ResponseService::ok('Street added');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function update(Request $request, int $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $request->replace(array_filter($request->all()));

            $attributes = $this->validate($request, Street::getUpdateRules());
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }

        $model = StreetService::update($id, $request->all());
        if (!$model) {
            return ResponseService::validationError('street', []);
        }
        AdminStreetsService::delete($id);
        app(AdminStreetsService::class)->create([
            'admin_id' => $attributes['admin_id'],
            'street_id' => $model->getAttribute('id'),
        ]);
        return ResponseService::ok('Street updated');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function delete(Request $request, int $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $this->validate($request, Street::getDeleteRules(),
                ['id.unique' => 'Can\'t delete, street used by user']);
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->errors()['id'][0] ?? $e->getMessage());
        }
        StreetService::delete($id);
        AdminStreetsService::delete($id);

        return ResponseService::ok('Street deleted');
    }
}
