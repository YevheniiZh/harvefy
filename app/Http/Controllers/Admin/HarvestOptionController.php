<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Models\HarvestOption;
use App\Models\MeatType;
use App\Services\HarvestOptionCitiesService;
use App\Services\HarvestOptionService;
use App\Services\ResponseService;
use App\Services\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\Controller;
use Illuminate\Validation\ValidationException;

class HarvestOptionController extends Controller
{
    protected $service;

    public function __construct(HarvestOptionService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $items = HarvestOption::orderBy('id', 'DESC')->get();

        $cities = Service::indexById(City::all(['id', 'name'])->toArray());
        $meatTypes = Service::indexById(MeatType::all(['id', 'name'])->toArray());
        return view('harvest_options', [
            'items' => $items,
            'itemsJs' => Service::indexById($items->toArray()),
            'cities' => $cities,
            'meat_types' => $meatTypes,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function add(Request $request)
    {
        try {
            $request->replace(array_filter($request->all()));
            $request->merge(['active' => $request->get('active') === 'on']);

            $this->validate($request, HarvestOption::getCreateRules(),
                ['file.required' => 'The image field is required.']);
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }

        if ($file = $request->file('file')) {
            $path = $file->store('image', 'uploads');
            $request->request->add(['image' => $path]);
        }
        $model = $this->service->create($request->all());
        if (!$model) {
            return ResponseService::validationError('harvest option', []);
        }
        foreach ($request->city_ids as $city_id) {
            app(HarvestOptionCitiesService::class)->create([
                'harvest_option_id' => $model->getAttribute('id'),
                'city_id' => (int) $city_id,
            ]);
        }

        return ResponseService::ok('HarvestOption added');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function update(Request $request, int $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $request->merge(['active' => $request->get('active') === 'on']);
            $this->validate($request, HarvestOption::getUpdateRules(),
                ['file.required' => 'The image field is required.']);
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }

        if ($file = $request->file('file')) {
            $path = $file->store('image', 'uploads');
            $request->request->add(['image' => $path]);
        }
        $model = HarvestOptionService::update($id, $request->all());
        if (!$model) {
            return ResponseService::validationError('harvest option', []);
        }
        HarvestOptionCitiesService::delete($id);
        foreach ($request->city_ids as $city_id) {
            app(HarvestOptionCitiesService::class)->create([
                'harvest_option_id' => $id,
                'city_id' => (int) $city_id,
            ]);
        }
        return ResponseService::ok('HarvestOption updated');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function delete(Request $request, int $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $this->validate($request, HarvestOption::getDeleteRules(),
                ['id.unique' => 'Can\'t delete, harvest option is used']);
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->errors()['id'][0] ?? $e->getMessage());
        }
        HarvestOptionService::delete($id);

        return ResponseService::ok('HarvestOption deleted');
    }
}
