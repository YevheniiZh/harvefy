<?php

namespace App\Http\Controllers\Admin;

use App\Casts\Timezone;
use App\Models\Admin;
use App\Models\Config;
use App\Models\Delivery;
use App\Models\LandImage;
use App\Models\MeatType;
use App\Models\Plant;
use App\Models\PlantType;
use App\Services\DeliveryService;
use App\Services\MeatService;
use App\Services\PartnerService;
use App\Services\PlantService;
use App\Services\ResponseService;
use App\Services\Service;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class DeliveryController extends PartnerController
{
    public const DAYS = [
        1 => ['name' => 'Понеділок'],
        2 => ['name' => 'Вівторок'],
        3 => ['name' => 'Середа'],
        4 => ['name' => 'Четвер'],
        5 => ['name' => 'П\'ятниця'],
        6 => ['name' => 'Субота'],
        7 => ['name' => 'Неділя'],
    ];

    /**
     * @var Delivery
     */
    private $model;

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $query = Delivery::with('plants', 'meat', 'user')->withCasts([
            'delivery_date' => Timezone::class,
            'created_at' => Timezone::class,
            'updated_at' => Timezone::class
        ])->selectRaw('*')->orderBy('id', 'DESC');
        $query = $this->filterQuery($query);
        $results = $query->get()->toArray();

        foreach ($results as $key => $result) {
            $results[$key]['plant_params'] = json_encode($result['plants']);
        }
        $plantTypes = Service::indexById(PlantType::all(['id', 'name'])->toArray());
        $meatTypes = Service::indexById(MeatType::all(['id', 'name'])->toArray());
        $days = Config::getAdditional(Config::DELIVERY_DAYS);
        $allDays = self::DAYS;
        foreach ($days as $key => $day) {
            $dayNum = DeliveryService::getIdByDayName($day);
            $allDays[$dayNum]['active'] = true;
        }

        return view('delivery', [
            'delivery' => $results,
            'selectedItem' => (int) $request->get('id'),
            'plant_types' => $plantTypes,
            'meat_types' => $meatTypes,
            'days' => $allDays,
            'isAdmin' => (int) $this->isAdmin,
            'partners' => Service::indexById(PartnerService::getAllPartners(true))
        ]);
    }

    public function show($id)
    {
        $post = PlantType::findOrFail($id);
        $comments = Comment::where('post_id', '=', $id)->get();
        return view('admin.viewPlant', ['post' => $post, 'comments' => $comments]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function update(Request $request, int $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $this->validate($request, Delivery::getByIdRules());
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->errors()['id'][0] ?? $e->getMessage());
        }

        if ($model = DeliveryService::setDelivered($id)) {
            foreach ($model->delivery_plants as $plant) {
                $service = $plant->product_type_id === LandImage::LAND_TYPE_MEAT ? MeatService::class : PlantService::class;
                $service::updateStatus($plant->product_id, Plant::STATUS_DELIVERED, 'delivered');
            }
            return ResponseService::ok('Delivery updated');
        }
        return ResponseService::error('Delivery not found');
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateConfig(Request $request): ?JsonResponse
    {
        $this->validate($request, Config::getUpdateDaysActiveRules());
        $id = (int) $request->get('id');
        $isActive = (bool) $request->get('isActive');

        $model = Config::where('name', Config::DELIVERY_DAYS)->first();
        $days = $model->additional;
        $daysUpdated = [];
        foreach (self::DAYS as $key => $day) {
            if ($key !== $id && isset($days[$key])) {
                $daysUpdated[$key] = $days[$key];
            } elseif ($key === $id && $isActive) {
                $daysUpdated[$key] = $day['name'];
            }
        }
        $model->additional = $daysUpdated;
        $model->save();

        return ResponseService::ok([]);
    }

}
