<?php

namespace App\Http\Controllers\Admin;

use App\Casts\Timezone;
use App\Models\Notification;
use App\Models\NotificationCustomLog;
use App\Models\NotificationLog;
use App\Services\NotificationService;
use App\Services\ResponseService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\DataTables;

class NotificationsController extends PartnerController
{
    protected $notificationService;

    public function __construct(NotificationService $landService)
    {
        $this->notificationService = $landService;
        parent::__construct();
    }

    public function index(Request $request)
    {
        $results = [];
        $isCustomTab = $this->isAdmin && $request->get('tab') === 'custom';
        if ($isCustomTab) {
            $results = NotificationCustomLog::selectRaw('*')->withCasts([
                'created_at' => Timezone::class,
                'updated_at' => Timezone::class
            ])->orderBy('id', 'DESC')->get();
        }

        $links = [
            'notifications' => url('/admin/notifications_log'),
            'notificationsCustom' => url('/admin/notifications_log?tab=custom'),
        ];
        $view = $isCustomTab ? 'notifications_custom' : 'notifications_log';
        return view($view, ['items' => $results, 'links' => $links, 'isAdmin' => (int) $this->isAdmin]);
    }

    public function getData(Request $request)
    {
        $query = NotificationLog::query()->withCasts([
            'created_at' => Timezone::class,
            'updated_at' => Timezone::class
        ]);
        $query = $this->filterQuery($query);

        return Datatables::of($query)->make(true);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     */
    public function send(Request $request): \Illuminate\Http\JsonResponse
    {
        $toAllUsers = (int) $request->get('toAllUsers');
        $request->request->add(['toAllUsers' => $toAllUsers]);
        $attributes = $this->validate($request, Notification::getSendByAdminRules());

        if ($toAllUsers) {
            $client_token_ids = NotificationService::getAllClientTokenIds();
        } else {
            $user = UserService::getByEmail($attributes['email']);
            if ($user === null) {
                return ResponseService::notExistEntity('user', 'email', $attributes['email']);
            }
            $client_token_ids = NotificationService::getClientTokenIds($user->id);
        }
        if (!$client_token_ids) {
            return ResponseService::error('User has no connected devices');
        }

        $data = [
            'title' => $attributes['notificationTitle'],
            'body' => $attributes['notificationText']
        ];
        foreach ($client_token_ids as $client_token_id) {
            $result = NotificationService::sendNotification($data, $client_token_id);
            NotificationService::logCustomNotification($result, $toAllUsers, $attributes['email'], $data['title'], $data['body']);
        }

        return ResponseService::ok('Notification send to ' . ($toAllUsers ? 'all users' : $attributes['email']));
    }

}
