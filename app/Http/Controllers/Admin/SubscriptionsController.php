<?php

namespace App\Http\Controllers\Admin;

use App\Casts\Timezone;
use App\Models\Admin;
use App\Models\Delivery;
use App\Models\Subscription;
use App\Services\PartnerService;
use App\Services\Service;
use Illuminate\Http\Request;

class SubscriptionsController extends PartnerController
{
    /**
     * @var Delivery
     */
    private $model;

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $query = Subscription::selectRaw('*')->with('user')->withCasts([
            'next_payment' => Timezone::class,
            'cancel_date' => Timezone::class,
            'created_at' => Timezone::class,
            //'updated_at' => Timezone::class
        ])->orderBy('id', 'DESC');
        $query = $this->filterQuery($query);
        $results = $query->get()->append('renew')->toArray();

        return view('subscriptions', [
            'items' => $results,
            'statuses' => Subscription::STATUSES,
            'partners' => Service::indexById(PartnerService::getAllPartners(true)),
            'isAdmin' => (int) $this->isAdmin
        ]);
    }

}
