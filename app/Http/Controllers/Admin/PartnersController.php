<?php

namespace App\Http\Controllers\Admin;

use App\Casts\Timezone;
use App\Helpers\MailHelper;
use App\Http\Controllers\Api\Controller;
use App\Mail\Plain;
use App\Models\Admin;
use App\Services\PartnerService;
use App\Services\Service;
use App\Services\ResponseService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class PartnersController extends Controller
{

    public function index()
    {
        $partners = Admin::where('is_partner', true)->withCasts([
            'created_at' => Timezone::class,
            'updated_at' => Timezone::class,
        ])->get();
        return view('partners', ['partners' => $partners, 'itemsJs' => Service::indexById($partners->toArray())]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function add(Request $request)
    {
        try {
            $attributes = $this->validate($request, Admin::getCreateRules());
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }

        $attributes['password'] = Service::generateRandomString();
        $model = PartnerService::create($attributes);
        if(!$model){
            return ResponseService::validationError('partner', []);
        }
        $data = new \stdClass();
        $data->subject = 'Регистрация партнера Harvefy';
        $data->text = 'Ваш пароль: ' . $attributes['password'];
        MailHelper::sendEmail($model->email, new Plain($data));
        return ResponseService::ok('Partner added');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function update(Request $request, int $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $attributes = $this->validate($request, Admin::getUpdateRules());
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->getMessage(), $e->errors());
        }

        $model = PartnerService::get($id);
        if(!$model) {
            return ResponseService::validationError('Partner not found', []);
        }
        PartnerService::update($model, $attributes);
        return ResponseService::ok('Partner updated');
    }
}
