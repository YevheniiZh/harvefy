<?php

namespace App\Http\Controllers\Admin;

use App\Models\Subscription;
use App\Models\User;

class HomeController extends PartnerController
{
    public function index()
    {
        $query = Subscription::selectRaw('count(distinct user_id) as count')->where('canceled', 0);
        $query = $this->filterQuery($query);
        $subscriptionsCount = $query->first()->count;

        $users = User::selectRaw('*');
        if ($this->is_partner) {
            $users->where('partner_id', $this->user_id);
        }
        $usersCount = $users->get()->count();

        return view('home', ['subscriptionsCount' => $subscriptionsCount, 'usersCount' => $usersCount]);
    }
}
