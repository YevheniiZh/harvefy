<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Api\Controller;
use App\Services\UserService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class PartnerController extends Controller
{
    protected $user_id;
    protected $is_partner;
    protected $isAdmin;
    protected $partnerUsers;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user_id = Auth::id();
            $this->is_partner = Auth::user()->is_partner;
            $this->isAdmin = !$this->is_partner;

            return $next($request);
        });
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function filterQuery(Builder $query): Builder
    {
        if ($this->is_partner) {
            $partnerUsers = $this->partnerUsers ?? UserService::getUserIdsByPartnerId($this->user_id);
            $query->whereIn('user_id', $partnerUsers);
        }
        return $query;
    }
}
