<?php

namespace App\Http\Controllers\Admin;

use App\Casts\Timezone;
use App\Models\ContactUs;
use App\Http\Controllers\Api\Controller;
use App\Services\ContactUsService;
use App\Services\ResponseService;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ContactUsController extends Controller
{
    /**
     * @var ContactUs
     */
    private $model;

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $results = ContactUs::selectRaw('*')->withCasts([
            'created_at' => Timezone::class,
            'updated_at' => Timezone::class
        ])->orderBy('id', 'DESC')->get()->toArray();

        return view('contact_us', [
            'items' => $results,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function markAsHandled(Request $request, int $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $this->validate($request, ContactUs::getUpdateRules());
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->errors()['id'][0] ?? $e->getMessage());
        }
        ContactUsService::markAsHandled($id);

        return ResponseService::ok('Contact us updated');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function delete(Request $request, int $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $this->validate($request, ContactUs::getUpdateRules());
        } catch (ValidationException $e) {
            return ResponseService::validationError($e->errors()['id'][0] ?? $e->getMessage());
        }
        ContactUsService::delete($id);

        return ResponseService::ok('Contact us deleted');
    }
}
