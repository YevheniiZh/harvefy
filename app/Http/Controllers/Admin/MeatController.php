<?php

namespace App\Http\Controllers\Admin;

use App\Casts\Timezone;
use App\Models\Admin;
use App\Models\Meat;
use App\Models\MeatType;
use App\Services\ConfigService;
use App\Services\MeatService;
use App\Services\PartnerService;
use App\Services\ResponseService;
use App\Services\Service;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MeatController extends PartnerController
{

    public function index()
    {
        $items = Meat::whereHas('land', function ($query) {
            return $query->where('is_payed', 1);
        })->orWhereHas('subscription', function ($query) {
            return $query->where('payment_id', '!=', null);
        })->selectRaw('*')->withCasts([
            'last_watering' => Timezone::class,
            'created_at' => Timezone::class,
            'updated_at' => Timezone::class
        ])->orderBy('id', 'DESC')->get();

        /*
         * Сегодня в 9 утра берём заказы со вчера с 16:00 до сегодня 9:00
            В 16:00 сегодня, берём заказы с 9:00 до 16:00
            Convert utc time to kyiv
         */
        if (Carbon::now(ConfigService::LOCAL_TIMEZONE)->hour < 16) {
            $period = '16:00 - 9:00';
            $whereQuery = "CONVERT_TZ(created_at,'+00:00','+02:00')
between date_format(DATE_SUB(CONVERT_TZ(now(),'+00:00','+02:00'), INTERVAL 1 DAY),'%Y-%m-%d 16:00:00') 
and date_format(CONVERT_TZ(now(),'+00:00','+02:00'),'%Y-%m-%d 09:00:00')";
        } else {
            $period = '9:00 - 16:00';
            $whereQuery = "CONVERT_TZ(created_at,'+00:00','+02:00')
between date_format(CONVERT_TZ(now(),'+00:00','+02:00'),'%Y-%m-%d 09:00:00')
and date_format(CONVERT_TZ(now(),'+00:00','+02:00'),'%Y-%m-%d 16:00:00')";
        }
        $lastItems = Meat::whereHas('land', function ($query) {
            return $query->where('is_payed', 1);
        })->whereRaw($whereQuery)
            ->selectRaw('meat_type_id, count(id) as count')
            ->groupBy('meat_type_id')
            ->get();
        $meatTypes = Service::indexById(MeatType::all(['id', 'name'])->toArray());

        return view('meat', [
            'items' => $items,
            'lastItems' => $lastItems,
            'lastItemsPeriod' => $period,
            'statuses' => Meat::STATUSES,
            'deliveryLink' => url('admin/delivery'),
            'meat_types' => $meatTypes,
            'partners' => Service::indexById(PartnerService::getAllPartners(true)),
            'isAdmin' => (int) $this->isAdmin
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getByDate(Request $request): JsonResponse
    {
        $from = $request->get('start');
        $to = $request->get('end');
        $onlyPayed = (bool) $request->get('only_payed');

        $whereQuery = "CONVERT_TZ(created_at,'+00:00','+02:00') between '{$from} 00:00' and '{$to} 23:59'";
        $query = Meat::query();
        $query->when($onlyPayed, function ($q) {
            return $q->whereHas('land', function ($query) {
                return $query->where('is_payed', 1);
            });
        });
        $items = $query->whereRaw($whereQuery)
            ->with('meatType:id,name', 'land:id,is_payed')
            ->selectRaw('id, user_id, land_id, meat_type_id, autowatering, created_at')
            ->withCasts(['created_at' => Timezone::class, 'autowatering' => 'int'])
            ->orderBy('id', 'desc')
            ->get();

        return ResponseService::ok($items);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function pourOn(Request $request, string $id = ''): JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, Meat::getByIdRules());

        MeatService::pourOn($id);

        $item = Meat::where('id', $id)->withCasts([
                'last_watering' => Timezone::class,
                'updated_at' => Timezone::class
            ])->first();
        return ResponseService::ok($item);
    }
}
