<?php

namespace App\Http\Controllers\Api;

use App\Models\Config;
use App\Models\Land;
use App\Models\Payment;
use App\Models\Plant;
use App\Models\Subscription;
use App\Services\DeliveryService;
use App\Services\LandService;
use App\Services\MeatService;
use App\Services\PaymentService;
use App\Services\PlantService;
use App\Services\ResponseService;
use App\Services\SubscriptionService;
use App\Services\SubscriptionStatusLogService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use WayForPay\SDK\Exception\ApiException;

class PaymentController extends Controller
{
    protected $service;
    protected $subscriptionService;
    protected $user_id;

    private const ORDER_LAND_PREFIX = 'order_land_id_';
    private const ORDER_DELIVERY_PREFIX = 'order_delivery_id_';

    public function __construct(PaymentService $service, SubscriptionService $subscriptionService)
    {
        $this->service = $service;
        $this->subscriptionService = $subscriptionService;

        $this->middleware(function ($request, $next) {
            $this->user_id = Auth::id();

            return $next($request);
        });
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, Payment::getByIdRules());

        $model = $this->service->getByIdAndUser($id, $this->user_id);
        if ($model === null) {
            return ResponseService::notExistEntity('payment', 'id', $id);
        }

        return ResponseService::ok($model->toArray());
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request): JsonResponse
    {
        $request->request->add(['user_id' => $this->user_id]);
        $this->validate($request, Payment::getCreateRules($this->user_id));

        $landId = $request->get('land_id');

        try {
            $land = LandService::getById($landId, $this->user_id);

            if ($land->type === Land::TYPE_MEAT) {
                $meat = MeatService::getByLandId($landId);
                $config = Arr::get($meat, 'meat_type.config_price_param');
                $amount = Config::getValue($config);
            } else {
                $amount = Config::getValue($this->subscriptionService->getPriceConfigByType($land->type));
            }
            $orderId = self::ORDER_LAND_PREFIX . $landId . '_' . time();
            $result = $this->service->startSubscribeLiqpay($orderId, $amount,
                "Оплата подписки за огород \"{$land->name}\"");
            $paymentData = $this->service->savePayment(
                $amount, $this->user_id, $landId, $orderId, Payment::ITEM_TYPE_LAND, $result
            );
        } catch (ApiException|\Exception $e) {
            return ResponseService::error($e->getMessage());
        }

        if ($request->get('debug')) {
            $paymentData['land_id'] = $landId;
            $paymentData['status'] = Subscription::STATUS_ACTIVE;
            $paymentData['payment_id'] = $paymentData['id'];
            $paymentData['created_at'] = time();
            $paymentData['is_debug'] = true;
            $subscription = $this->subscriptionService->create($paymentData);
            LandService::setIsPayed($landId);
            return ResponseService::ok(['payment' => $paymentData, 'subscription' => $subscription]);
        }
        return ResponseService::ok(Arr::only($paymentData, 'id'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function createDelivery(Request $request): JsonResponse
    {
        $request->request->add(['user_id' => $this->user_id]);
        $this->validate($request, Payment::getCreateDeliveryRules($this->user_id));

        $deliveryId = $request->get('delivery_id');

        try {
            $orderId = self::ORDER_DELIVERY_PREFIX . $deliveryId . '_' . time();
            $amount = Config::getValue(Config::CONFIG_DELIVERY_PRICE);
            $result = $this->service->startPaymentLiqpay($orderId, $amount, 'Оплата доставки');
            $paymentData = $this->service->savePayment(
                $amount, $this->user_id, $deliveryId, $orderId, Payment::ITEM_TYPE_DELIVERY, $result
            );
        } catch (ApiException|\Exception $e) {
            return ResponseService::error($e->getMessage());
        }

        return ResponseService::ok(Arr::only($paymentData, 'id'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getLiqpayPaymentPage(Request $request)
    {
        $this->validate($request, Payment::getLiqpayPaymentPageRules());

        return view('liqpay_payment', $request->all());
    }

    /**
     * @param Request $request
     * @param string $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getLiqpayPaymentPageById(Request $request, string $id = '')
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, Payment::getLiqpayPaymentPageByIdRules());

        $paymentFormData = $this->service->getById($request->get('id'));
        return view('liqpay_payment', Arr::get($paymentFormData, 'payment_form_data'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function after(Request $request): JsonResponse
    {
        return ResponseService::ok([
            'status' => 'success',
            'message' => 'ok'
        ]);

        try {
            $this->validate($request, Payment::getLiqpayCallbackRules());

            $this->service->getLiqPay()->check_signature($request->get('data'), $request->get('signature'));
            $response = $this->service->decodeResultLiqpay($request->get('data'));

            $model = $this->service->finishPayment($response);

            if (in_array(Arr::get($response, 'status'), PaymentService::LIQPAY_SUCCESS_STATUSES, true)) {
                $itemModel = [];
                if ($model) {
                    if ($model->item_type === Payment::ITEM_TYPE_LAND) {
                        $params = $model->toArray();
                        $params['land_id'] = $params['item_id'];
                        $params['payment_id'] = $model->id;
                        $params['status'] = Subscription::STATUS_ACTIVE;
                        $this->subscriptionService->create($params);
                        $itemModel = LandService::setIsPayed($model->item_id);
                    } else {
                        $itemModel = DeliveryService::setIsPayed($model->item_id);
                        foreach ($itemModel->plant_ids as $plant_id) {
                            PlantService::updateStatus($plant_id, Plant::STATUS_DELIVERY_REQUESTED, 'delivery');
                        }
                        foreach ($itemModel->meat_ids as $meat_id) {
                            MeatService::updateStatus($meat_id, Plant::STATUS_DELIVERY_REQUESTED, 'delivery');
                        }
                    }
                }
                return ResponseService::ok([
                    'status' => 'success',
                    'message' => 'ok',
                    'item_id' => $model->item_id ?? null,
                    'item_data' => $itemModel
                ]);
            } else {
                $error = Arr::get($response, 'err_description');
                //return ResponseService::error($result);
            }
        } catch (\Exception $e) {
            $error = $e->getMessage();
            $this->writeLog('after_payment_error.log', [
                'request' => request()->all(),
                'response' => $response ?? null,
                'error' => $error,
                'model' => ($model ?? null) ? $model->toArray() : null
            ]);
        }

        return ResponseService::ok([
            'status' => 'failed',
            'message' => $error ?: 'Что-то пошло не так',
            'item_id' => $model->item_id ?? null
        ]);
    }

    /**
     * @param int $itemId
     * @return void
     */
    public function setDeliveryIsPayed(int $itemId): void
    {
        if ($itemModel = DeliveryService::setIsPayed($itemId)) {
            foreach ($itemModel->plant_ids as $plant_id) {
                PlantService::updateStatus($plant_id, Plant::STATUS_DELIVERY_REQUESTED, 'delivery');
            }
            foreach ($itemModel->meat_ids as $meat_id) {
                MeatService::updateStatus($meat_id, Plant::STATUS_DELIVERY_REQUESTED, 'delivery');
            }
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function service(Request $request): JsonResponse
    {
        try {
            $this->validate($request, Payment::getLiqpayCallbackRules());

            $this->service->getLiqPay()->check_signature($request->get('data'), $request->get('signature'));
            $response = $this->service->decodeResultLiqpay($request->get('data'));
            $responseStatus = Arr::get($response, 'status');
            $payment = $this->service->storeCallbackResponse($response);

            if (Arr::get($response, 'action') === 'subscribe') {
                $model = $this->service->finishPayment($response);

                if ($model && in_array($responseStatus, PaymentService::LIQPAY_SUCCESS_STATUSES, true)) {
                    $data = 'Subscription start';
                    if ($model->item_type === Payment::ITEM_TYPE_LAND) {
                        $params = $model->toArray();
                        $params['land_id'] = $params['item_id'];
                        $params['payment_id'] = $model->id;
                        $params['status'] = Subscription::STATUS_ACTIVE;
                        $this->subscriptionService->create($params);
                        LandService::setIsPayed($model->item_id);
                    } else {
                        $this->setDeliveryIsPayed($model->item_id);
                    }
                }
            } elseif ($payment && Arr::get($response, 'action') === 'regular') {
                if ($responseStatus === PaymentService::LIQPAY_STATUS_SUCCESS) {
                    $data = 'Subscription continue';
                    SubscriptionService::prolong($payment->id);
                } elseif ($responseStatus === PaymentService::LIQPAY_STATUS_UNSUBSCRIBED) {
                    $data = 'unsubscribed';
                    SubscriptionService::cancelDueCantCharge($payment->id);
                    // @todo
                    //LandService::setIsPayed($subscriptions['land_id'], false);
                } elseif ($responseStatus === PaymentService::LIQPAY_STATUS_FAILURE) {
                    $data = 'unsubscribed';
                    $subscription = SubscriptionService::cancelDueCantCharge($payment->id);
                    if (SubscriptionStatusLogService::getCountAttempts($subscription->id) > 3) {
                        $this->service->unsubscribeLiqpay($payment->transaction_id);
                    }
                }
            } elseif ($payment && Arr::get($response, 'action') === 'pay') {
                $this->service->finishPayment($response);
                if ($responseStatus === PaymentService::LIQPAY_STATUS_SUCCESS) {
                    $this->setDeliveryIsPayed($payment->item_id);
                }
            }
            $result = ResponseService::ok($data ?? '');
        } catch (\Exception $e) {
            $result = ResponseService::error($e->getMessage(), 422,
                ['file' => $e->getFile(), 'line' => $e->getLine()]
            );
        }

        $this->writeLog('service_url.log', [
            'result' => $result,
            'response' => $response ?? null,
            'request' => request()->all()
        ]);

        return $result;
    }


    /**
     * @param string $file
     * @param array $data
     */
    public function writeLog(string $file, array $data): void {
        file_put_contents(
            $_SERVER['DOCUMENT_ROOT'] . '/' . $file,
            PHP_EOL . Carbon::now()->toDateTimeString() . PHP_EOL . json_encode($data) . PHP_EOL,
            FILE_APPEND
        );
    }
}
