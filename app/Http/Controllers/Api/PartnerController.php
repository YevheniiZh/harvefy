<?php

namespace App\Http\Controllers\Api;

use App\Models\BaseModel;
use App\Services\PartnerService;
use App\Services\ResponseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class PartnerController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return ResponseService::ok(PartnerService::getAllPartners(true));
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function show(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, BaseModel::getByIdRules());

        $item = PartnerService::getPartner($id);
        if ($item === null) {
            return ResponseService::notExistEntity('partner', 'id', $id);
        }

        return ResponseService::ok($item);
    }

}
