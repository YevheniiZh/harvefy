<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\V1\Land;
use App\Models\V1\LandImage;
use App\Services\V1\PlantService;
use App\Services\V1\LandService;
use App\Services\V1\PlantTypeService;
use App\Services\V1\ResponseService;
use App\Services\V1\SubscriptionService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LandController extends Controller
{
    protected $landService;
    protected $user_id;

    public function __construct(LandService $landService)
    {
        $this->landService = $landService;
        $this->middleware(function ($request, $next) {
            $this->user_id = Auth::id();

            return $next($request);
        });
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $land = LandService::get($this->user_id);
        if ($land === null) {
            return ResponseService::notExistEntity('land', 'user_id', $this->user_id);
        }
        $results = $land->toArray();

        foreach ($results as $key => $result) {
            $results[$key] = LandService::mapPlants($result);
            $results[$key]['subscription_end_date'] = Arr::get($result, 'subscription.next_payment');
            Arr::forget($results[$key], 'subscription');
        }
        return ResponseService::ok($results);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $request->request->add(['user_id' => $this->user_id]);
        $validateAttributes = $this->validate($request, Land::getCreateRules());

        $plants = [];
        $landImages = LandImage::all();
        $validateAttributes['main_image'] = $landImages->count() ? $landImages->random(1)->first()->getRawOriginal('image') : '';
        $land = $this->landService->create(Arr::except($validateAttributes, 'plants.*.autowatering'));
        foreach ($validateAttributes['plants'] as $key => $plant) {
            $landPlant = [
                'land_id' => $land->id,
                'land_position' => $plant['position'],
                'plant_type_id' => $plant['plant_type_id'],
                'autowatering' => $plant['autowatering'],
                'user_id' => $this->user_id
            ];
            $plantId = app(PlantService::class)->create($landPlant)->id;
            PlantService::pourOn($plantId);
            $plant['id'] = $plantId;
            $plants[] = $plant;
        }
        $result = $this->landService->update($land->id, ['plants' => $plants]);

        return ResponseService::created($result);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function show(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, Land::getByIdRules());

        $land = LandService::getById($id, $this->user_id);
        if ($land === null) {
            return ResponseService::notExistEntity('land', 'id', $id);
        }
        $result = LandService::mapPlants($land->toArray());
        $result['subscription_end_date'] = Arr::get($result, 'subscription.next_payment');
        Arr::forget($result, 'subscription');

        return ResponseService::ok($result);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $validateAttributes = $this->validate($request, Land::getUpdateRules($this->user_id));

        $model = $this->landService->update($id, $validateAttributes);

        return ResponseService::ok($model);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function addPlant(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $validateAttributes = $this->validate($request, Land::getAddPlantRules($this->user_id));

        if ($plant = PlantService::getActivePlantByPostision($id, $validateAttributes['position'])) {
            return ResponseService::error('Can\'t add new plant to land');
        }
        $plantType = PlantTypeService::get($validateAttributes['plant_type_id']);
        $subscription = SubscriptionService::getByLandId($id);
        if ($subscription && $subscription->canceled && Carbon::createFromTimestamp($subscription->next_payment) < Carbon::now()->addDays($plantType->days_to_grow)) {
            return ResponseService::error('Can\'t add this plant to land');
        }
        $landPlant = [
            'land_id' => $id,
            'land_position' => $validateAttributes['position'],
            'plant_type_id' => $validateAttributes['plant_type_id'],
            'autowatering' => Arr::get($validateAttributes, 'autowatering', false),
            'user_id' => $this->user_id
        ];
        $plantId = app(PlantService::class)->create($landPlant)->id;
        PlantService::pourOn($plantId);
        $plant = [
            'id' => $plantId,
            'position' => $validateAttributes['position'],
            'plant_type_id' => $validateAttributes['plant_type_id']
        ];
        $model = $this->landService->addPlant($id, $plant);

        return ResponseService::ok($model);
    }

}
