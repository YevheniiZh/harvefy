<?php

namespace App\Http\Controllers\Api\V1;

use App\Services\V1\ResponseService;
use App\Services\V1\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\V1\User;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{

    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */
    public function profile(): JsonResponse
    {
        return ResponseService::ok(UserService::get(Auth::id()));
    }

    // @todo remove
    /**
     * Get all User.
     *
     * @return JsonResponse
     */
    public function allUsers(): JsonResponse
    {
         return ResponseService::ok(User::all());
    }

    /**
     * Get one user.
     *
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function singleUser(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, User::getByIdRules());

        $user = UserService::get($id);
        if ($user === null) {
            return ResponseService::notExistEntity('user', 'id', $id);
        }

        return ResponseService::ok($user);
    }

    /**
     * @return JsonResponse
     */
    public function delete(): ?JsonResponse
    {
        $userId = Auth::id();
        $user = Auth::user();
        UserService::delete($userId);

        return ResponseService::ok($user);
    }

}
