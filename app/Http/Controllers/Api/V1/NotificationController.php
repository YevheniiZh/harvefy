<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\V1\Land;
use App\Models\V1\Notification;
use App\Services\V1\NotificationService;
use App\Services\V1\ResponseService;
use App\Services\V1\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class NotificationController extends Controller
{
    protected $notificationService;
    protected $user_id;

    public function __construct(NotificationService $landService)
    {
        $this->notificationService = $landService;
        $this->middleware(function ($request, $next) {
            $this->user_id = Auth::id();

            return $next($request);
        });
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $items = NotificationService::get($this->user_id);
        if ($items === null) {
            return ResponseService::notExistEntity('notification', 'user_id', $this->user_id);
        }
        return ResponseService::ok($items);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $request->request->add(['user_id' => $this->user_id]);
        $validateAttributes = $this->validate($request, Notification::getCreateRules());
        $item = $this->notificationService->findByToken(
            $validateAttributes['user_id'],
            $validateAttributes['client_token_id']
        );
        if ($item) {
            return ResponseService::ok($item);
        }
        $result = $this->notificationService->create($validateAttributes);

        return ResponseService::created($result);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    /*public function sendNotification(Request $request): JsonResponse
    {
        $validateAttributes = $this->validate($request, Notification::getSendNotificationRules());

        $YOUR_TOKEN_ID = Arr::get($validateAttributes, 'client_token_id');
        $response = NotificationService::sendNotification($validateAttributes, $YOUR_TOKEN_ID);

        return ResponseService::ok($response);
    }*/

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function show(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, Notification::getByIdRules());

        $items = NotificationService::getById($id, $this->user_id);
        if ($items === null) {
            return ResponseService::notExistEntity('notification', 'id', $id);
        }

        return ResponseService::ok($items);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $validateAttributes = $this->validate($request, Land::getUpdateRules($this->user_id));

        $model = $this->notificationService->update($id, $validateAttributes);

        return ResponseService::ok($model);
    }

    /**
     * @return JsonResponse
     */
    public function delete(): ?JsonResponse
    {
        $userId = Auth::id();
        $user = Auth::user();
        UserService::delete($userId);

        return ResponseService::ok($user);
    }

}
