<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\V1\ContactUs;
use App\Services\V1\ContactUsService;
use App\Services\V1\ResponseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class ContactUsController extends Controller
{
    protected $service;
    protected $user_id;

    public function __construct(ContactUsService $service)
    {
        $this->service = $service;
        $this->middleware(function ($request, $next) {
            $this->user_id = Auth::id();

            return $next($request);
        });
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return ResponseService::ok(ContactUsService::get($this->user_id));
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function show(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, ContactUs::getByIdRules());

        $item = ContactUsService::getById($id, $this->user_id);
        if ($item === null) {
            return ResponseService::notExistEntity('contact us', 'id', $id);
        }

        return ResponseService::ok($item);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $request->request->add(['user_id' => $this->user_id]);
        $validateAttributes = $this->validate($request, ContactUs::getCreateRules());

        $result = $this->service->create($validateAttributes);

        return ResponseService::created($result);
    }

}
