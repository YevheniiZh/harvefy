<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\V1\Config;
use App\Models\V1\Subscription;
use App\Services\V1\LandService;
use App\Services\V1\PaymentService;
use App\Services\V1\ResponseService;
use App\Services\V1\SubscriptionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class SubscriptionController extends Controller
{
    protected $subscriptionService;
    protected $paymentService;
    protected $user_id;

    public function __construct(SubscriptionService $subscriptionService, PaymentService $paymentService)
    {
        $this->subscriptionService = $subscriptionService;
        $this->paymentService = $paymentService;
        $this->middleware(function ($request, $next) {
            $this->user_id = Auth::id();

            return $next($request);
        });
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $land = SubscriptionService::get($this->user_id);
        if ($land === null) {
            return ResponseService::notExistEntity('subscription', 'user_id', $this->user_id);
        }
        $results = $land->toArray();
        foreach ($results as $key => $result) {
            $results[$key]['land'] = LandService::mapPlants($result['land']);
        }
        return ResponseService::ok($results);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function show(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, Subscription::getByIdRules());

        $model = SubscriptionService::getById($id, $this->user_id);
        if ($model === null) {
            return ResponseService::notExistEntity('subscription', 'id', $id);
        }
        $result = $model->toArray();
        $result['land'] = LandService::mapPlants($result['land']);
        $result['sub_price'] = Config::getValue(Config::CONFIG_SUBSCRIPTION_PRICE);

        return ResponseService::ok($result);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function unsubscribe(Request $request, string $id = ''): ?JsonResponse
    {
        $userId = Auth::id();
        $request->request->add(['id' => $id]);
        $this->validate($request, Subscription::getUnsubscribeRules($userId));

        if ($model = SubscriptionService::unsubscribe($id)) {
            if ($transaction_id = Arr::get($model, 'payment.transaction_id')) {
                $result = $this->paymentService->unsubscribeLiqpay($transaction_id);
                SubscriptionService::updatePsResponse($id, $result);
            }
        }

        return ResponseService::ok($model);
    }

}
