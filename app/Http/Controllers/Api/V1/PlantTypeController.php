<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\V1\PlantType;
use App\Services\V1\PlantTypeService;
use App\Services\V1\ResponseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class PlantTypeController extends Controller
{
    protected $landService;
    protected $user_id;

    public function __construct(PlantTypeService $landService)
    {
        $this->landService = $landService;
        $this->middleware(function ($request, $next) {
            $this->user_id = Auth::id();

            return $next($request);
        });
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return ResponseService::ok(PlantType::all());
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function show(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, PlantType::getByIdRules());

        $item = PlantTypeService::get($id);
        if ($item === null) {
            return ResponseService::notExistEntity('plant', 'id', $id);
        }

        return ResponseService::ok($item);
    }

}
