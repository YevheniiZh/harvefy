<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\V1\Config;
use App\Models\V1\Payment;
use App\Models\V1\Plant;
use App\Models\V1\Subscription;
use App\Services\V1\DeliveryService;
use App\Services\V1\LandService;
use App\Services\V1\PaymentService;
use App\Services\V1\PlantService;
use App\Services\V1\ResponseService;
use App\Services\V1\SubscriptionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use WayForPay\SDK\Exception\ApiException;

class PaymentController extends Controller
{
    protected $service;
    protected $subscriptionService;
    protected $user_id;

    private const ORDER_LAND_PREFIX = 'order_land_id_';
    private const ORDER_DELIVERY_PREFIX = 'order_delivery_id_';

    public function __construct(PaymentService $service, SubscriptionService $subscriptionService)
    {
        $this->service = $service;
        $this->subscriptionService = $subscriptionService;

        $this->middleware(function ($request, $next) {
            $this->user_id = Auth::id();

            return $next($request);
        });
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request): JsonResponse
    {
        $request->request->add(['user_id' => $this->user_id]);
        $this->validate($request, Payment::getCreateRules($this->user_id));

        $landId = $request->get('land_id');
        $amount = Config::getValue(Config::CONFIG_SUBSCRIPTION_PRICE);

        try {
            $land = LandService::getById($landId, $this->user_id);
            $orderId = self::ORDER_LAND_PREFIX . $landId . '_' . time();
            $result = $this->service->startSubscribeLiqpay($orderId, $amount,
                "Оплата подписки за огород \"{$land->name}\"");
            $paymentData = $this->service->savePayment(
                $amount, $this->user_id, $landId, $orderId, Payment::ITEM_TYPE_LAND
            );
        } catch (ApiException|\Exception $e) {
            return ResponseService::error($e->getMessage());
        }

        if ($request->get('debug')) {
            $paymentData['land_id'] = $landId;
            $paymentData['status'] = Subscription::STATUS_ACTIVE;
            $paymentData['payment_id'] = $paymentData['id'];
            $paymentData['created_at'] = time();
            $paymentData['is_debug'] = true;
            $subscription = $this->subscriptionService->create($paymentData);
            LandService::setIsPayed($landId);
            return ResponseService::ok(['payment' => $paymentData, 'subscription' => $subscription]);
        }
        return ResponseService::ok($result);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function createDelivery(Request $request): JsonResponse
    {
        $request->request->add(['user_id' => $this->user_id]);
        $this->validate($request, Payment::getCreateDeliveryRules($this->user_id));

        $deliveryId = $request->get('delivery_id');

        try {
            $orderId = self::ORDER_DELIVERY_PREFIX . $deliveryId . '_' . time();
            $amount = Config::getValue(Config::CONFIG_DELIVERY_PRICE);
            $result = $this->service->startPaymentLiqpay($orderId, $amount, 'Оплата доставки');
            $this->service->savePayment(
                $amount, $this->user_id, $deliveryId, $orderId, Payment::ITEM_TYPE_DELIVERY
            );
        } catch (ApiException|\Exception $e) {
            return ResponseService::error($e->getMessage());
        }

        return ResponseService::ok($result);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getLiqpayPaymentPage(Request $request)
    {
        $this->validate($request, Payment::getLiqpayPaymentPageRules());

        $this->service->getLiqPay()->check_signature($request->get('data'), $request->get('signature'));
        $response = $this->service->decodeResultLiqpay($request->get('data'));

        if (Arr::get($response, 'action') === 'pay') {
            return view('liqpay_payment', $request->all());
        }
        return view('update_app', $request->all());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function after(Request $request): JsonResponse
    {
        try {
            $this->validate($request, Payment::getLiqpayCallbackRules());

            $this->service->getLiqPay()->check_signature($request->get('data'), $request->get('signature'));
            $response = $this->service->decodeResultLiqpay($request->get('data'));

            $model = $this->service->finishPayment($response);

            if (in_array(Arr::get($response, 'status'), PaymentService::LIQPAY_SUCCESS_STATUSES, true)) {
                $itemModel = [];
                if ($model) {
                    if ($model->item_type === Payment::ITEM_TYPE_LAND) {
                        $params = $model->toArray();
                        $params['land_id'] = $params['item_id'];
                        $params['payment_id'] = $model->id;
                        $params['status'] = Subscription::STATUS_ACTIVE;
                        $this->subscriptionService->create($params);
                        $itemModel = LandService::setIsPayed($model->item_id);
                    } else {
                        $itemModel = DeliveryService::setIsPayed($model->item_id);
                        foreach ($itemModel->plant_ids as $plant_id) {
                            PlantService::updateStatus($plant_id, Plant::STATUS_DELIVERY_REQUESTED, 'delivery');
                        }
                    }
                }
                return ResponseService::ok([
                    'status' => 'success',
                    'message' => 'ok',
                    'item_id' => $model->item_id ?? null,
                    'item_data' => $itemModel
                ]);
            } else {
                $error = Arr::get($response, 'err_description');
                //return ResponseService::error($result);
            }
        } catch (\Exception $e) {
            $error = $e->getMessage();
            $this->writeLog('after_payment_error.log', [
                'request' => request()->all(),
                'response' => $response ?? null,
                'error' => $error,
                'model' => ($model ?? null) ? $model->toArray() : null
            ]);
        }

        return ResponseService::ok([
            'status' => 'failed',
            'message' => $error ?: 'Что-то пошло не так',
            'item_id' => $model->item_id ?? null
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function service(Request $request): JsonResponse
    {
        try {
            $this->validate($request, Payment::getLiqpayCallbackRules());

            $this->service->getLiqPay()->check_signature($request->get('data'), $request->get('signature'));
            $response = $this->service->decodeResultLiqpay($request->get('data'));

            $payment = $this->service->storeCallbackResponse($response);

            if ($payment && Arr::get($response, 'action') === 'regular') {
                if (Arr::get($response, 'status') === PaymentService::LIQPAY_STATUS_SUCCESS) {
                    $data = 'Subscription continue';
                    SubscriptionService::prolong($payment->id);
                } elseif (Arr::get($response, 'status') === PaymentService::LIQPAY_STATUS_UNSUBSCRIBED) {
                    $data = 'unsubscribed';
                    SubscriptionService::cancelDueCantCharge($payment->id);
                    // @todo
                    //LandService::setIsPayed($subscriptions['land_id'], false);
                } elseif (Arr::get($response, 'status') === PaymentService::LIQPAY_STATUS_FAILURE) {
                    $data = 'unsubscribed';
                    SubscriptionService::cancelDueCantCharge($payment->id);
                }
            } elseif ($payment && Arr::get($response, 'action') === 'pay') {
                if (Arr::get($response, 'status') === PaymentService::LIQPAY_STATUS_SUCCESS) {
                    $itemModel = DeliveryService::setIsPayed($payment->item_id);
                    foreach ($itemModel->plant_ids as $plant_id) {
                        PlantService::updateStatus($plant_id, Plant::STATUS_DELIVERY_REQUESTED, 'delivery');
                    }
                }
            }
            $result = ResponseService::ok($data ?? '');
        } catch (\Exception $e) {
            $result = ResponseService::error($e->getMessage());
        }

        $this->writeLog('service_url.log', [
            'request' => request()->all(),
            'response' => $response ?? null,
            'result' => $result
        ]);

        return $result;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getConfig(Request $request): ?JsonResponse
    {
        return ResponseService::ok(Config::all(['name', 'value', 'additional'])->toArray());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateConfig(Request $request): ?JsonResponse
    {
        $validateAttributes = $this->validate($request, Config::getUpdateRules());

        $model = Config::where('name', $request->get('name'))->first();
        $model->fill($validateAttributes);
        $model->save();
        $model->refresh();

        return ResponseService::ok(Arr::only($model->toArray(), ['name', 'value', 'additional']));
    }

    /**
     * @param string $file
     * @param array $data
     */
    public function writeLog(string $file, array $data): void {
        file_put_contents(
            $_SERVER['DOCUMENT_ROOT'] . '/' . $file,
            PHP_EOL . Carbon::now()->toDateTimeString() . PHP_EOL . json_encode($data) . PHP_EOL,
            FILE_APPEND
        );
    }
}
