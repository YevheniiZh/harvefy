<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\LandImage;
use App\Models\V1\Config;
use App\Models\V1\Delivery;
use App\Services\V1\DeliveryPlantsService;
use App\Services\V1\DeliveryService;
use App\Services\V1\ResponseService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class DeliveryController extends Controller
{
    protected $deliveryService;
    protected $user_id;

    public function __construct(DeliveryService $deliveryService)
    {
        $this->deliveryService = $deliveryService;
        $this->middleware(function ($request, $next) {
            $this->user_id = Auth::id();

            return $next($request);
        });
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $land = DeliveryService::get($this->user_id);
        $results = $land ? $land->toArray() : [];

        return ResponseService::ok($results);
    }

    /**
     * @return JsonResponse
     */
    public function days(): JsonResponse
    {
        return ResponseService::ok(\App\Services\DeliveryService::getAvailableDays());

        /*$days = Config::getAdditional(Config::DELIVERY_DAYS);
        $today = (int) Carbon::today()->format('N');
        $availableDays = [$today + 2];
        if (Carbon::now()->hour < 12) {
            $availableDays[] = $today + 1;
        }
        return ResponseService::ok(collect($days)->only($availableDays)->values());*/
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $request->request->add(['user_id' => $this->user_id]);
        $validateAttributes = $this->validate($request, Delivery::getCreateRules($this->user_id),
            ['unique' => 'Delivery already exists']
        );
        if ($this->deliveryService->checkForPayedPlants($validateAttributes['plant_ids'])) {
            return ResponseService::error('plant_ids already payed');
        }

        $result = $this->deliveryService->create($validateAttributes);
        foreach ($validateAttributes['plant_ids'] as $plant_id) {
            app(DeliveryPlantsService::class)->create([
                'delivery_id' => $result->getAttribute('id'),
                'product_id' => $plant_id,
                'product_type_id' => LandImage::LAND_TYPE_MICROGREEN,
            ]);
        }

        return ResponseService::created($result);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function show(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, Delivery::getByIdRules());

        $model = DeliveryService::getById($id, $this->user_id);
        if ($model === null) {
            return ResponseService::notExistEntity('delivery', 'id', $id);
        }
        $result = $model->toArray();
        $result['delivery_price'] = Config::getValue(Config::CONFIG_DELIVERY_PRICE);

        return ResponseService::ok($result);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function delete(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, Delivery::getDeleteByIdRules($this->user_id));

        DeliveryService::delete($id, $this->user_id);
        DeliveryPlantsService::delete($id);

        return ResponseService::ok([]);
    }

}
