<?php

namespace App\Http\Controllers\Api;

use App\Models\Recipe;
use App\Services\RecipeService;
use App\Services\ResponseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class RecipeController extends Controller
{

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function index(Request $request): JsonResponse
    {
        $this->validate($request, Recipe::getAllRules());

        if ($request->plant_type_id) {
            $result = RecipeService::getByPlantTypeId($request->plant_type_id);
        } else {
            $result = Recipe::all();
        }
        return ResponseService::ok($result);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function show(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, Recipe::getByIdRules());

        $item = RecipeService::get($id);
        if ($item === null) {
            return ResponseService::notExistEntity('recipe', 'id', $id);
        }

        return ResponseService::ok($item);
    }

}
