<?php

namespace App\Http\Controllers\Api;

use App\Models\MeatType;
use App\Services\MeatTypeService;
use App\Services\ResponseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class MeatTypeController extends Controller
{
    protected $landService;
    protected $user_id;

    public function __construct(MeatTypeService $landService)
    {
        $this->landService = $landService;
        $this->middleware(function ($request, $next) {
            $this->user_id = Auth::id();

            return $next($request);
        });
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return ResponseService::ok(MeatType::all()->append('price')->makeHidden('config_price_param'));
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function show(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, MeatType::getByIdRules());

        $item = MeatTypeService::get($id);
        if ($item === null) {
            return ResponseService::notExistEntity('plant', 'id', $id);
        }

        return ResponseService::ok($item);
    }

}
