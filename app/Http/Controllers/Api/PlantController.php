<?php

namespace App\Http\Controllers\Api;

use App\Models\Plant;
use App\Services\PlantService;
use App\Services\ResponseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class PlantController extends Controller
{
    protected $plantService;
    protected $user_id;

    public function __construct(PlantService $plantService)
    {
        $this->plantService = $plantService;
        $this->middleware(function ($request, $next) {
            $this->user_id = Auth::id();

            return $next($request);
        });
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function index(Request $request): JsonResponse
    {
        $statusIds = json_decode($request->get('status'), true);

        if ($statusIds === null) {
            return ResponseService::validationError('Error while validation.', [
                'status' => [
                    'Status ids must be valid JSON string',
                ],
            ]);
        }

        $request->request->add(['status' => $statusIds]);
        $this->validate($request, Plant::getAllRules());

        $plant = PlantService::getByUserId($this->user_id, $statusIds);
        if ($plant === null) {
            return ResponseService::notExistEntity('land', 'user_id', $this->user_id);
        }
        $results = [];

        foreach ($plant as $key => $item) {
            $grow = PlantService::getHoursInterval($item->created_at);

            $result = $item->toArray();
            $result['harvest_in'] = $this->plantService->harvestIn($item, $grow['days']);
            $result['grown_on'] = $this->plantService->grownOnPercent($item, $grow['hours']) . '%';
            $results[$key] = $result;
        }

        return ResponseService::ok($results);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function show(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, Plant::getByIdRules());

        $item = PlantService::get($id);
        if ($item === null) {
            return ResponseService::notExistEntity('plant', 'id', $id);
        }
        $grow = PlantService::getHoursInterval($item->created_at);
        $item['harvest_in'] = $this->plantService->harvestIn($item, $grow['days']);
        $item['grown_on'] = $this->plantService->grownOnPercent($item, $grow['hours']) . '%';

        return ResponseService::ok($item);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $validateAttributes = $this->validate($request, Plant::getUpdateRules());

        $model = PlantService::update($id, $validateAttributes);

        return ResponseService::ok($model);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function harvest(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, Plant::getHarvestRules($this->user_id));

        $model = PlantService::updateStatus($id, Plant::STATUS_IN_BASKET, 'harvest');

        return ResponseService::ok($model);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function pourOn(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, Plant::getPourOnRules($this->user_id));

        $item = PlantService::pourOn($id);

        return ResponseService::ok($item);
    }

}
