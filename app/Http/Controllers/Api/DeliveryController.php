<?php

namespace App\Http\Controllers\Api;

use App\Models\Config;
use App\Models\Delivery;
use App\Models\LandImage;
use App\Services\DeliveryPlantsService;
use App\Services\DeliveryService;
use App\Services\ResponseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class DeliveryController extends Controller
{
    protected $deliveryService;
    protected $user_id;

    public function __construct(DeliveryService $deliveryService)
    {
        $this->deliveryService = $deliveryService;
        $this->middleware(function ($request, $next) {
            $this->user_id = Auth::id();

            return $next($request);
        });
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $land = DeliveryService::get($this->user_id);
        $results = $land ? $land->toArray() : [];

        return ResponseService::ok($results);
    }

    /**
     * @return JsonResponse
     */
    public function days(): JsonResponse
    {
        return ResponseService::ok(DeliveryService::getAvailableDays());
    }

    /**
     * @return JsonResponse
     */
    public function last(): JsonResponse
    {
        $result = [];
        if ($last = DeliveryService::getLast($this->user_id)) {
            $result = $last->only(['address', 'phone', 'name']);
        }
        return ResponseService::ok($result);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $request->request->add(['user_id' => $this->user_id]);
        $validateAttributes = $this->validate($request,
            Delivery::getCreateRules($this->user_id),
            ['unique' => 'Delivery already exists']
        );
        $plantIds = Arr::get($validateAttributes, 'plant_ids', []);
        $meatIds = Arr::get($validateAttributes, 'meat_ids', []);
        if ($this->deliveryService->checkForPayedPlants($plantIds)) {
            return ResponseService::error('plant_ids already payed');
        }
        if ($this->deliveryService->checkForPayedMeat($meatIds)) {
            return ResponseService::error('meat_ids already payed');
        }

        $result = $this->deliveryService->create($validateAttributes);
        foreach ($plantIds as $plantId) {
            app(DeliveryPlantsService::class)->create([
                'delivery_id' => $result->getAttribute('id'),
                'product_id' => $plantId,
                'product_type_id' => LandImage::LAND_TYPE_MICROGREEN,
            ]);
        }
        foreach ($meatIds as $meatId) {
            app(DeliveryPlantsService::class)->create([
                'delivery_id' => $result->getAttribute('id'),
                'product_id' => $meatId,
                'product_type_id' => LandImage::LAND_TYPE_MEAT,
            ]);
        }

        return ResponseService::created($result);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function show(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, Delivery::getByIdRules());

        $model = DeliveryService::getById($id, $this->user_id);
        if ($model === null) {
            return ResponseService::notExistEntity('delivery', 'id', $id);
        }
        $result = $model->toArray();
        $result['delivery_price'] = Config::getValue(Config::CONFIG_DELIVERY_PRICE);

        return ResponseService::ok($result);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function delete(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, Delivery::getDeleteByIdRules($this->user_id));

        DeliveryService::delete($id, $this->user_id);
        DeliveryPlantsService::delete($id);

        return ResponseService::ok([]);
    }

}
