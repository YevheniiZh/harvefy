<?php

namespace App\Http\Controllers\Api;

use App\Models\Land;
use App\Models\LandImage;
use App\Services\MeatService;
use App\Services\MeatTypeService;
use App\Services\PlantService;
use App\Services\LandService;
use App\Services\PlantTypeService;
use App\Services\ResponseService;
use App\Services\SubscriptionService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class LandController extends Controller
{
    protected $landService;
    protected $user_id;

    public function __construct(LandService $landService)
    {
        $this->landService = $landService;
        $this->middleware(function ($request, $next) {
            $this->user_id = Auth::id();

            return $next($request);
        });
    }

    /**
     * @return JsonResponse
     */
    public function landTypes(): JsonResponse
    {
        $result = [];

        $landTypes = LandService::getAvailableLandTypes();
        foreach ($landTypes as $key => $status) {
            $result[] = ['id' => $key, 'name' => $status];
        }
        return ResponseService::ok($result);
    }

    /**
     * @return JsonResponse
     * @throws \Exception
     */
    public function index(): JsonResponse
    {
        $land = LandService::get($this->user_id);
        if ($land === null) {
            return ResponseService::notExistEntity('land', 'user_id', $this->user_id);
        }
        $results = $land->toArray();

        foreach ($results as $key => $result) {
            $results[$key] = LandService::mapProduct($result);
            $results[$key]['subscription_end_date'] = Arr::get($result, 'subscription.next_payment');
            Arr::forget($results[$key], 'subscription');
        }
        return ResponseService::ok($results);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $request->request->add(['user_id' => $this->user_id]);
        $type = (int) $request->get('type') ?: Land::TYPE_3_PLANTS;
        $validateAttributes = $this->validate($request, Land::getCreateRules($type));

        $products = [];
        $mainImage = '';
        $landTypeId = $type === Land::TYPE_MEAT ? LandImage::LAND_TYPE_MEAT : LandImage::LAND_TYPE_MICROGREEN;
        $productTypeId = $type === Land::TYPE_MEAT ? Arr::get($validateAttributes, 'meat.0.meat_type_id') : null;
        if ($landImages = LandImage::getAllByType($landTypeId, $productTypeId)) {
            $mainImage = $landImages->count() ? $landImages->random(1)->first()->getRawOriginal('image') : '';
        }
        $validateAttributes['main_image'] = $mainImage;
        $land = $this->landService->create(Arr::except($validateAttributes, 'plants.*.autowatering'));

        if ($type === Land::TYPE_MEAT) {
            foreach ($validateAttributes['meat'] as $key => $meat) {
                $landPlant = [
                    'land_id' => $land->id,
                    'meat_type_id' => $meat['meat_type_id'],
                    'autowatering' => $meat['autowatering'],
                    'user_id' => $this->user_id
                ];
                $meatId = app(MeatService::class)->create($landPlant)->id;
                MeatService::pourOn($meatId);
                $meat['id'] = $meatId;
                $products[] = $meat;
            }
            $result = $this->landService->update($land->id, ['meat' => $products]);
        } else {
            foreach ($validateAttributes['plants'] as $key => $plant) {
                $landPlant = [
                    'land_id' => $land->id,
                    'land_position' => $plant['position'],
                    'plant_type_id' => $plant['plant_type_id'],
                    'autowatering' => $plant['autowatering'],
                    'user_id' => $this->user_id
                ];
                $plantId = app(PlantService::class)->create($landPlant)->id;
                PlantService::pourOn($plantId);
                $plant['id'] = $plantId;
                $products[] = $plant;
            }
            $result = $this->landService->update($land->id, ['plants' => $products]);
        }

        return ResponseService::created($result);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     * @throws \Exception
     */
    public function show(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, Land::getByIdRules());

        $land = LandService::getById($id, $this->user_id);
        if ($land === null) {
            return ResponseService::notExistEntity('land', 'id', $id);
        }
        $result = LandService::mapProduct($land->toArray());
        $result['subscription_end_date'] = Arr::get($result, 'subscription.next_payment');
        Arr::forget($result, 'subscription');

        return ResponseService::ok($result);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $validateAttributes = $this->validate($request, Land::getUpdateRules($this->user_id));

        $model = $this->landService->update($id, $validateAttributes);

        return ResponseService::ok($model);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function addPlant(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);

        $model = $this->landService::getById($id, $this->user_id);
        if ($model === null) {
            return ResponseService::notExistEntity('land', 'id', $id);
        }
        $validateAttributes = $this->validate($request, Land::getAddPlantRules($this->user_id, $model->type));

        if ($plant = PlantService::getActivePlantByPostision($id, $validateAttributes['position'])) {
            return ResponseService::error('Can\'t add new plant to land');
        }
        $plantType = PlantTypeService::get($validateAttributes['plant_type_id']);
        $subscription = SubscriptionService::getByLandId($id);
        if ($subscription && $subscription->canceled && Carbon::createFromTimestamp($subscription->next_payment) < Carbon::now()->addDays($plantType->days_to_grow)) {
            return ResponseService::error('Can\'t add this plant to land');
        }
        $plant = [
            'land_id' => $id,
            'land_position' => $validateAttributes['position'],
            'plant_type_id' => $validateAttributes['plant_type_id'],
            'autowatering' => Arr::get($validateAttributes, 'autowatering', false),
            'user_id' => $this->user_id
        ];
        $plantId = app(PlantService::class)->create($plant)->id;
        PlantService::pourOn($plantId);
        $landPlant = [
            'id' => $plantId,
            'position' => $validateAttributes['position'],
            'plant_type_id' => $validateAttributes['plant_type_id']
        ];
        $model = $this->landService->addPlant($id, $landPlant);

        return ResponseService::ok($model);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function addMeat(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $validateAttributes = $this->validate($request, Land::getAddMeatRules($this->user_id));

        if (MeatService::getActiveMeat($id)) {
            return ResponseService::error('Can\'t add new meat to land');
        }
        if (!$meatTypeId = MeatService::getLandMeatTypeId($id)) {
            Log::error('Error getLandMeatTypeId: ' . $id);
            return ResponseService::error('Can\'t add new meat to land');
        }
        $meatType = MeatTypeService::get($meatTypeId);
        $subscription = SubscriptionService::getByLandId($id);
        if ($subscription && $subscription->canceled && Carbon::createFromTimestamp($subscription->next_payment) < Carbon::now()->addDays($meatType->days_to_grow)) {
            return ResponseService::error('Can\'t add this meat to land');
        }

        $meat = [
            'land_id' => $id,
            'meat_type_id' => $meatTypeId,
            'autowatering' => Arr::get($validateAttributes, 'autowatering', false),
            'user_id' => $this->user_id
        ];
        $meatId = app(MeatService::class)->create($meat)->id;
        MeatService::pourOn($meatId);

        $model = $this->landService->addMeat($id, [
            'id' => $meatId,
            'meat_type_id' => $meatTypeId
        ]);

        return ResponseService::ok($model);
    }

}
