<?php

namespace App\Http\Controllers\Api;

use App\Models\Plant;
use App\Services\ResponseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class PlantStatusController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $result = [];
        foreach (Plant::STATUSES as $key => $status) {
            $result[] = ['id' => $key, 'name' => $status];
        }
        return ResponseService::ok($result);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     */
    public function show(Request $request, string $id = ''): ?JsonResponse
    {
        if (!Arr::get(Plant::STATUSES, $id)) {
            return ResponseService::notExistEntity('status', 'id', $id);
        }

        return ResponseService::ok(['id' => $id, 'name' => Plant::STATUSES[$id]]);
    }

}
