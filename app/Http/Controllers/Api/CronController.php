<?php

namespace App\Http\Controllers\Api;

use App\Services\CronService;
use Illuminate\Http\Request;

class CronController extends Controller
{
    public function execute(Request $request) {

        if (!$request->get('pass') || $request->get('pass') !== env('CRON_PASS_CODE')) {
            file_put_contents(
                base_path().'/error.log',
                PHP_EOL. date('d-m-Y H:i:s') .' '. request()->url() .PHP_EOL. 'wrong cron pass: ' . $request->get('pass') .PHP_EOL,
                FILE_APPEND
            );
            return redirect()->route('landing');
        }
        CronService::executeTasksForThisTime();

        return response('Complete', 200);
    }

}
