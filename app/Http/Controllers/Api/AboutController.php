<?php

namespace App\Http\Controllers\Api;

use App\Models\About;
use App\Services\AboutService;
use App\Services\ResponseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AboutController extends Controller
{
    protected $landService;
    protected $user_id;

    public function __construct(AboutService $landService)
    {
        $this->landService = $landService;
        $this->middleware(function ($request, $next) {
            $this->user_id = Auth::id();

            return $next($request);
        });
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return ResponseService::ok(About::all());
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function show(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, About::getByIdRules());

        $item = AboutService::get($id);
        if ($item === null) {
            return ResponseService::notExistEntity('plant', 'id', $id);
        }

        return ResponseService::ok($item);
    }

}
