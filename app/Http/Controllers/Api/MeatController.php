<?php

namespace App\Http\Controllers\Api;

use App\Models\Meat;
use App\Services\HarvestOptionService;
use App\Services\MeatService;
use App\Services\ResponseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class MeatController extends Controller
{
    protected $meatService;
    protected $user_id;

    public function __construct(MeatService $meatService)
    {
        $this->meatService = $meatService;
        $this->middleware(function ($request, $next) {
            $this->user_id = Auth::id();

            return $next($request);
        });
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function index(Request $request): JsonResponse
    {
        $statusIds = json_decode($request->get('status'), true);

        if ($statusIds === null) {
            return ResponseService::validationError('Error while validation.', [
                'status' => [
                    'Status ids must be valid JSON string',
                ],
            ]);
        }

        $request->request->add(['status' => $statusIds]);
        $this->validate($request, Meat::getAllRules());

        $meat = MeatService::getByUserId($this->user_id, $statusIds);
        if ($meat === null) {
            return ResponseService::notExistEntity('land', 'user_id', $this->user_id);
        }
        $results = [];

        foreach ($meat as $key => $item) {
            $grow = MeatService::getHoursInterval($item->created_at);

            $result = $item->toArray();
            $result['harvest_in'] = $this->meatService->harvestIn($item, $grow['days']);
            $result['grown_on'] = $this->meatService->grownOnPercent($item, $grow['hours']) . '%';
            $results[$key] = $result;
        }

        return ResponseService::ok($results);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function show(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, Meat::getByIdRules());

        $item = MeatService::get($id);
        if ($item === null) {
            return ResponseService::notExistEntity('meat', 'id', $id);
        }
        $grow = MeatService::getHoursInterval($item->created_at);
        $item['harvest_in'] = $this->meatService->harvestIn($item, $grow['days']);
        $item['grown_on'] = $this->meatService->grownOnPercent($item, $grow['hours']) . '%';

        return ResponseService::ok($item);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $validateAttributes = $this->validate($request, Meat::getUpdateRules());

        $model = MeatService::update($id, $validateAttributes);

        return ResponseService::ok($model);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function harvest(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);

        $this->validate($request, Meat::getHarvestRules($this->user_id));
        $item = MeatService::get($id);
        $result = HarvestOptionService::getByMeatTypeIdCityId($item->meat_type_id, Auth::user()->city_id);
        $harvestOptionIds = array_column($result->toArray(), 'id');
        $validateAttributes = $this->validate($request, Meat::getHarvestOptionRules($harvestOptionIds));

        $model = MeatService::harvestMeat($id, $validateAttributes['harvest_option_id']);

        return ResponseService::ok($model);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function pourOn(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, Meat::getPourOnRules($this->user_id));

        $item = MeatService::pourOn($id);

        return ResponseService::ok($item);
    }

}
