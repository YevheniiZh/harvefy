<?php

namespace App\Http\Controllers\Api;

use App\Models\Config;
use App\Services\ResponseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ConfigController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getConfig(Request $request): ?JsonResponse
    {
        return ResponseService::ok(Config::all(['name', 'value', 'additional'])->toArray());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getCurrentApiVersion(Request $request): ?JsonResponse
    {
        return ResponseService::ok(['version' => env('CURRENT_API_VERSION')]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateConfig(Request $request): ?JsonResponse
    {
        $validateAttributes = $this->validate($request, Config::getUpdateRules());

        $model = Config::where('name', $request->get('name'))->first();
        $model->fill($validateAttributes);
        $model->save();
        $model->refresh();

        return ResponseService::ok(Arr::only($model->toArray(), ['name', 'value', 'additional']));
    }
}
