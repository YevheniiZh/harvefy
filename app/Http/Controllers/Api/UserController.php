<?php

namespace App\Http\Controllers\Api;

use App\Models\Street;
use App\Services\CityService;
use App\Services\ResponseService;
use App\Services\StreetService;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    protected $cityService;

    public function __construct(CityService $cityService)
    {
        $this->cityService = $cityService;
    }

    /**
     * @return JsonResponse
     */
    public function getCities(): JsonResponse
    {
        return ResponseService::ok($this->cityService::getAvailable());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function getStreets(Request $request): JsonResponse
    {
        $attributes = $this->validate($request, Street::getFindRules());
        if ($street = Arr::get($attributes, 'street')) {
            $result = StreetService::getAllBySybstring($street);
        } else {
            $result = StreetService::getAll();
        }
        return ResponseService::ok($result);
    }

    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */
    public function profile(): JsonResponse
    {
        return ResponseService::ok(Auth::guard('api')->user());
    }

    // @todo remove
    /**
     * Get all User.
     *
     * @return JsonResponse
     */
    public function allUsers(): JsonResponse
    {
         return ResponseService::ok(User::all());
    }

    /**
     * Get one user.
     *
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function singleUser(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, User::getByIdRules());

        $user = UserService::get($id);
        if ($user === null) {
            return ResponseService::notExistEntity('user', 'id', $id);
        }

        return ResponseService::ok($user);
    }

    /**
     * @return JsonResponse
     */
    public function delete(): ?JsonResponse
    {
        $userId = Auth::id();
        $user = Auth::user();
        UserService::delete($userId);

        return ResponseService::ok($user);
    }

}
