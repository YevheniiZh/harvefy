<?php

namespace App\Http\Controllers\Api;

use App\Helpers\MailHelper;
use App\Mail\RecoveryPassword;
use App\Models\User;
use App\Services\ResponseService;
use App\Services\StreetService;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Store a new user.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function register(Request $request): ?JsonResponse
    {
        $request->request->set('email', strtolower($request->get('email')));
        $validateAttributes = $this->validate($request, User::getCreateRules());

        if ($request->get('address') && $street = StreetService::getInSybstring($request->get('address'))) {
            $validateAttributes['partner_id'] = $street->admin->id ?? null;
        }

        return ResponseService::created($this->userService->create($validateAttributes));
    }


    /**
     * Get a JWT via given credentials.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function login(Request $request): JsonResponse
    {
        $this->validate($request, User::getAuthenticationRules());

        $credentials = $request->only(['email', 'password']);
        if (!$token = Auth::guard('api')->attempt($credentials)) {
            return ResponseService::validationError('Authentication failed', ['email' => ['Email or password is incorrect']]);
        }

        return ResponseService::respondWithToken($token);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function forgot(Request $request): JsonResponse
    {
        $validateAttributes = $this->validate($request, User::getForgotPasswordRules());

        $newPassword = UserService::generateRandomString(10);
        if ($user = UserService::updatePassword($validateAttributes['email'], $newPassword)) {
            //$userToLogout = User::find($user->id);
            //Auth::setUser($userToLogout);
            //Auth::logout();

            $data = new \stdClass();
            $data->newPassword = $newPassword;
            //MailHelper::sendEmailThroughSideServer($validateAttributes['email'], 'Відновлення паролю Harvefy', 'mails.recovery', (array) $data);

            $send = true;
            while($send) {
                try {
                    Mail::to($validateAttributes['email'])->send(new RecoveryPassword($data));
                    $send = false;
                } catch (\Exception $e) {
                    Log::error('Error sendEmail: ' . $e->getMessage());
                    sleep(1);
                }
            }
        }

        return ResponseService::ok([]);
    }

    /**
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        $userId = Auth::id();
        Auth::logout();

        return ResponseService::ok(['user_id' => $userId]);
    }

    /**
     * @return JsonResponse
     */
    public function tokenRefresh(): JsonResponse
    {
        $token = auth()->refresh();

        return ResponseService::respondWithToken($token);
    }

    /**
     * @return JsonResponse
     */
    public function tokenInfo(): JsonResponse
    {
        $payload = auth()->payload();

        return ResponseService::ok([
            'user_id' => Auth::id(),
            'exp' => $payload('exp'),
        ]);
    }

}
