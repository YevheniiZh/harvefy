<?php

namespace App\Http\Controllers\Api;

use App\Models\HarvestOption;
use App\Services\HarvestOptionService;
use App\Services\ResponseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class HarvestOptionController extends Controller
{

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function index(Request $request): JsonResponse
    {
        $this->validate($request, HarvestOption::getAllRules());
        if ($request->meat_type_id) {
            $result = HarvestOptionService::getByMeatTypeId($request->meat_type_id);
        } else {
            $result = HarvestOptionService::getAll();
        }
        if ($result) {
            $userCityId = Auth::user()->city_id;
            foreach ($result as $key => $item) {
                $optionCityIds = array_column($item['cities'], 'city_id');
                $result[$key]['available'] = $item['active'] ? in_array($userCityId, $optionCityIds, true): false;
                unset($result[$key]['cities']);
            }
        }
        return ResponseService::ok($result);
    }

}
