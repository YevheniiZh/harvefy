<?php

namespace App\Http\Controllers\Api;

use App\Models\Config;
use App\Models\Subscription;
use App\Services\LandService;
use App\Services\PaymentService;
use App\Services\ResponseService;
use App\Services\SubscriptionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class SubscriptionController extends Controller
{
    protected $subscriptionService;
    protected $paymentService;
    protected $user_id;

    public function __construct(SubscriptionService $subscriptionService, PaymentService $paymentService)
    {
        $this->subscriptionService = $subscriptionService;
        $this->paymentService = $paymentService;
        $this->middleware(function ($request, $next) {
            $this->user_id = Auth::id();

            return $next($request);
        });
    }

    /**
     * @return JsonResponse
     * @throws \Exception
     */
    public function index(): JsonResponse
    {
        $subscriptions = SubscriptionService::get($this->user_id);
        if ($subscriptions === null) {
            return ResponseService::notExistEntity('subscription', 'user_id', $this->user_id);
        }
        $results = $subscriptions->toArray();
        foreach ($results as $key => $result) {
            $results[$key]['land'] = LandService::mapProduct($result['land']);
        }
        return ResponseService::ok($results);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     * @throws \Exception
     */
    public function show(Request $request, string $id = ''): ?JsonResponse
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, Subscription::getByIdRules());

        $model = SubscriptionService::getById($id, $this->user_id);
        if ($model === null) {
            return ResponseService::notExistEntity('subscription', 'id', $id);
        }
        $result = $model->toArray();
        $result['land'] = LandService::mapProduct($result['land']);
        $result['sub_price'] = Arr::get($model, 'payment.amount');

        return ResponseService::ok($result);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function unsubscribe(Request $request, string $id = ''): ?JsonResponse
    {
        $userId = Auth::id();
        $request->request->add(['id' => $id]);
        $this->validate($request, Subscription::getUnsubscribeRules($userId));

        $model = SubscriptionService::getById($id, $userId);
        if ((int) $model->status !== Subscription::STATUS_ACTIVE) {
            return ResponseService::ok("Ви вже відписалися :disappointed:");
        }

        if ($model = SubscriptionService::unsubscribe($id)) {
            if ($transaction_id = Arr::get($model, 'payment.transaction_id')) {
                $result = $this->paymentService->unsubscribeLiqpay($transaction_id);
                SubscriptionService::updatePsResponse($id, $result);
            }
        }

        return ResponseService::ok($model);
    }

}
