<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Logger
{
    public function handle($request, Closure $next)
    {
        Log::info('_REQUEST, url: ' . request()->getPathInfo() .
            ', token: ' . request()->header('authorization') . ', userId: ' . Auth::id()
        );

        return $next($request);
    }
}
