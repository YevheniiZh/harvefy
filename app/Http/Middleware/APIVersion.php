<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

/**
 * Class APIVersion
 * @package App\Http\Middleware
 */
class APIVersion
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @param $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard)
    {
        // @todo remove if not used
        config(['app.api.version' => $guard]);
        return $next($request);
    }
}