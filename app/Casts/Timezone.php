<?php

namespace App\Casts;

use App\Services\ConfigService;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Support\Carbon;

class Timezone implements CastsAttributes
{
    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $key
     * @param mixed $value
     * @param array $attributes
     * @return Carbon|mixed
     * @throws \Exception
     */
    public function get($model, $key, $value, $attributes)
    {
        if (!$value) {
            return $value;
        }
        return (new Carbon($value))->setTimezone(ConfigService::LOCAL_TIMEZONE)->toDateTimeString();
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $key
     * @param mixed $value
     * @param array $attributes
     * @return array|mixed|string
     */
    public function set($model, $key, $value, $attributes)
    {
        return $value;
    }
}