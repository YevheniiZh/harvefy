<?php

namespace App\Console\Commands;

use App\Services\CronService;
use App\Services\MeatService;
use App\Services\PlantService;
use Illuminate\Console\Command;

class SendNewPlants extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendNewPlants';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * @throws \Throwable
     */
    public function handle(): void
    {
        $result['plant'] = PlantService::sendNewPlants();
        $result['meat'] = MeatService::sendNewMeat();

        CronService::writeLog('sendNewPlants.log', $result);
        echo 'sendNewPlants Handled' . PHP_EOL;
    }
}
