<?php

namespace App\Console\Commands;

use App\Services\CronService;
use App\Services\MeatService;
use App\Services\PlantService;
use Illuminate\Console\Command;

class RunAutowatering extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runAutowatering';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * @throws \Exception
     */
    public function handle(): void
    {
        $result['plant'] = PlantService::runAutowatering();
        $result['meat'] = MeatService::runAutowatering();

        CronService::writeLog('autowatering.log', $result);
        echo 'runAutowatering Handled' . PHP_EOL;
    }
}
