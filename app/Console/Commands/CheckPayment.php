<?php

namespace App\Console\Commands;

use App\Services\PaymentService;
use Illuminate\Console\Command;

class CheckPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check_payment {order_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $paymentService;

    /**
     * Create a new command instance.
     *
     * @param PaymentService $paymentService
     */
    public function __construct(PaymentService $paymentService)
    {
        parent::__construct();
        $this->paymentService = $paymentService;
    }

    public function handle(): void
    {
        $orderId = $this->argument('order_id');

        $result = $this->paymentService->getStatusLiqpay($orderId);
        echo 'Result: ' . json_encode($result) . "\r\n";
    }
}
