<?php

namespace App\Console\Commands;

use App\Services\CronService;
use App\Services\MeatService;
use App\Services\PlantService;
use Illuminate\Console\Command;

class HandleStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'handleStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * @throws \Throwable
     */
    public function handle(): void
    {
        $result['plant'] = PlantService::checkStatusForUpdate();
        $result['meat'] = MeatService::checkStatusForUpdate();

        CronService::writeLog('handleStatus.log', $result);
        echo 'handleStatus Handled' . PHP_EOL;
    }
}
