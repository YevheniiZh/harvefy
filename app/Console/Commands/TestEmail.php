<?php

namespace App\Console\Commands;

use App\Mail\DemoEmail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class TestEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test_email {email?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $email = $this->argument('email') ?: 'receiver@mailinator.com';

        $objDemo = new \stdClass();
        $objDemo->sender = 'SenderUserName';
        $objDemo->receiver = 'ReceiverUserName';

        $result = Mail::to($email)->send(new DemoEmail($objDemo));
        var_dump($result);

        echo 'Result: ' . json_encode($result) . "\r\n";
    }
}
