<?php

namespace App\Console\Commands;

use App\Services\CronService;
use App\Services\MeatService;
use App\Services\PlantService;
use Illuminate\Console\Command;

class SendHasGrownToday extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendHasGrownToday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * @throws \Throwable
     */
    public function handle(): void
    {
        $result['plant'] = PlantService::sendHasGrownToday();
        $result['meat'] = MeatService::sendHasGrownToday();

        CronService::writeLog('sendHasGrownToday.log', $result);
        echo 'sendHasGrownToday Handled' . PHP_EOL;
    }
}
