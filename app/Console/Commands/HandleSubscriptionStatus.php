<?php

namespace App\Console\Commands;

use App\Services\CronService;
use App\Services\SubscriptionService;
use Illuminate\Console\Command;

class HandleSubscriptionStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'handleSubscriptionStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * @throws \Exception
     */
    public function handle(): void
    {
        $result = SubscriptionService::checkStatusForUpdate();

        CronService::writeLog('handleSubscriptionStatus.log', $result);
        echo 'handleSubscriptionStatus Handled' . PHP_EOL;
    }
}
