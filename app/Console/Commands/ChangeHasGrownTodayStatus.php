<?php

namespace App\Console\Commands;

use App\Services\CronService;
use App\Services\MeatService;
use App\Services\PlantService;
use Illuminate\Console\Command;

class ChangeHasGrownTodayStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'changeHasGrownTodayStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * @throws \Throwable
     */
    public function handle(): void
    {
        $result['plant'] = PlantService::changeStatusHasGrownToday();
        $result['meat'] = MeatService::changeStatusHasGrownToday();

        CronService::writeLog('changeHasGrownTodayStatus.log', $result);
        echo 'changeHasGrownTodayStatus Handled' . PHP_EOL;
    }
}
