<?php

namespace App\Console;

use App\Services\ConfigService;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Carbon;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('handleStatus')->everyTenMinutes()->appendOutputTo('handleStatus.log');
        $schedule->command('handleSubscriptionStatus')->everyTenMinutes()->appendOutputTo('handleSubscriptionStatus.log');
        $schedule->command('runAutowatering')->hourly()->appendOutputTo('autowatering.log');
        // get hour by kyiv timezone in UTC
        $hour9 = Carbon::createFromFormat('H', 9, ConfigService::LOCAL_TIMEZONE)->tz('UTC')->hour;
        $hour16 = Carbon::createFromFormat('H', 16, ConfigService::LOCAL_TIMEZONE)->tz('UTC')->hour;
        $schedule->command('sendNewPlants')->twiceDaily($hour9, $hour16)->appendOutputTo('sendNewPlants.log');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
