<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extendImplicit('password_without_spaces', static function($attr, $value){
            if(!isset($value)) {
                return true;
            }
            return trim($value) === $value;
        },
            'Password cannot begin or end with a space');
    }
}
